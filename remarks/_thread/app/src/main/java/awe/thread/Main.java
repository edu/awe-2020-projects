//---------------------
//Main
//(c) H.Buchmann FHNW 2020
//---------------------
package awe.thread;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;

public class Main extends Activity 
                  implements Thread.Listener
{
  private TextView tick;
  private TextView interrupt;
  private int      count=0;
  private Thread   th=null;
  
  protected void onCreate(Bundle savedInstanceState) 
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    tick=(TextView)findViewById(R.id.tick); 
    interrupt=(TextView)findViewById(R.id.interrupt);
  }

  public void onInterrupt(Thread.Listener.Cause cause)
  {
   interrupt.setText(""+cause);
   th=null;
  }
  
  public void onTick()
  {
   tick.setText(""+count);
   ++count;
  }

  public void start(View v)
  {
   if (th==null) th=new Thread(this);
  }
  
  public void interrupt(View v)
  {
   if (th==null) return;
   th.interrupt();
  }
}
