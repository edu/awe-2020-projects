//----------------------
//Thread.java
//(c) H.Buchmann FHNW 2020
// using enums
//       Handler
//       Looper
//----------------------
package awe.thread;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

class Thread implements Runnable,Handler.Callback
{
 static interface Listener
 {
  enum Cause {Except,Interrupted};
  void onInterrupt(Cause cause);
  void onTick();
 }
 
 private final Listener li;
 private final Handler  handler;
 private final java.lang.Thread th;
 Thread(Listener li)
 {
  this.li=li;
  handler=new Handler(Looper.myLooper(),this);//Handler.createAsync(Looper.myLooper(),this);
  System.err.println("Thread="+java.lang.Thread.currentThread());
  th=new java.lang.Thread(this);
  th.start();
 }

 void interrupt()
 {
  System.err.println("Thread.interrupt="+java.lang.Thread.currentThread());
  th.interrupt();
 }

//------------------------------------ the thread
 public void run()
 {
  try
  {
  while(true)
  {
   System.err.println("Thread.run="+java.lang.Thread.currentThread());
   if (java.lang.Thread.interrupted())
      {
       handler.sendEmptyMessage(2); //interrupted 
      }
   handler.sendEmptyMessage(0); //tick
   java.lang.Thread.sleep(1000);
  }
  }
  catch(InterruptedException ex)
  {
   handler.sendEmptyMessage(1); //interrupt   
  }
  catch(Exception ex)
  {
   throw new RuntimeException(ex);
  }
 }

 public boolean handleMessage(Message msg)
 {
  switch(msg.what)
  {
   case 0:
    li.onTick();
   break;
   case 1:
    li.onInterrupt(Listener.Cause.Except);
   break;
   case 2:
    li.onInterrupt(Listener.Cause.Interrupted);
   break;
  }
  return true;
 } 
 
}
