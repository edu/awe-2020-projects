//---------------------------
//Background.java
//(c) H.Buchmann FHNW 2020
//---------------------------
package awe.asyncTaskListener;

class Background extends android.os.AsyncTask<Void,Void,Void>
{
 static interface Listener
 {
  void onDone();
 }
 
 final private Listener li;
 
 Background(Listener li)
 {
  this.li=li;
  execute();
 }
 
 public Void doInBackground(Void... args)
 {
  try
  {
   Thread.sleep(1000);
  }
  catch(Exception ex)
  {
   throw new RuntimeException(ex);
  }
  return null;
 }
 
 public void onPostExecute(Void arg)
 {
  li.onDone(); //in UI Thread
 }
}

