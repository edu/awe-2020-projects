//----------------------
//Result.java
//(c) H.Buchmann FHNW 2020
//----------------------
package awe.asyncTaskListener;

import android.content.Context;
import android.util.AttributeSet;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Color;

public class Result extends android.view.View
{
 public Result(Context context,AttributeSet attr)
 {
  super(context,attr);
  paint=new Paint();
  paint.setTextSize(64);
  paint.setColor(Color.WHITE);
 }
 
 private String text="start";
 final Paint paint;
  
 public void onDraw(Canvas canvas)
 {
  canvas.drawText(text,20,64,paint);
 }
 
 void show(String text)
 {
  this.text=text;
  invalidate(); 
 }
}
