//---------------------
//Main
//(c) H.Buchmann FHNW 2020
// AsyncTask is deprecated
//---------------------
package awe.asyncTaskListener;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class Main extends Activity 
                  implements Background.Listener
{
  private Result result;
  private int count=0;
  protected void onCreate(Bundle savedInstanceState) 
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    result=(Result)findViewById(R.id.result);
  }
  
  public void start(View v)
  {
   new Background(this);
  }
  
  public void onDone()
  {
   System.err.println("done "+count);
   result.show(""+count);
   ++count;
  }
}
