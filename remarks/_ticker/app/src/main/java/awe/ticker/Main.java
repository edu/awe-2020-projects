//---------------------
//Main
//(c) H.Buchmann FHNW 2020
//---------------------
package awe.ticker;

import android.app.Activity;
import android.os.Bundle;

public class Main extends Activity
                  implements Ticker.Listener 
{
  private Result result;
  private int count=0;
  
  protected void onCreate(Bundle savedInstanceState) 
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    result=(Result)findViewById(R.id.result);
    new Ticker(this);
  }
  
  public void onTick()
  {
   System.err.println("onTick="+count);
   result.show(""+count);
   ++count;
  }
}
