//-----------------------
//Ticker.java
//(c) H.Buchmann FHNW 2020
//-----------------------
package awe.ticker;
import android.os.Handler;
import android.os.Message;
 
class Ticker implements Handler.Callback
{
 private Handler handler;
 final private Listener li;
 
 static interface Listener
 {
  void onTick();
 }
 
 public boolean handleMessage(Message msg)
 {
  li.onTick();
  handler.sendEmptyMessageDelayed(0,1000);
  return true;
 }
 
 Ticker(Listener li)
 {
  this.li=li;
  handler=new Handler(android.os.Looper.getMainLooper(),this);
  handler.sendEmptyMessageDelayed(0,1000);
 }
  
}
