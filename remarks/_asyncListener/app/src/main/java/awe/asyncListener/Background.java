//---------------------------
//Background.java
//(c) H.Buchmann FHNW 2020
//---------------------------
package awe.asyncListener;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

class Background implements Runnable,Handler.Callback
 
{
 static interface Listener
 {
  void onDone();
 }
 
 final private Listener li;
 final private Handler handler;
 
 Background(Listener li)
 {
  this.li=li;
  handler=new Handler(Looper.getMainLooper(),this);
  System.err.println("Background="+Thread.currentThread());
  new Thread(this).start();
 }
 
 
 public void run() //background thread
 {
  System.err.println("Background.run="+Thread.currentThread());
  try 
  {
   Thread.sleep(1000);
  }
  catch(Exception ex)
  {
   throw new RuntimeException(ex);
  }
  handler.sendEmptyMessage(0);
 }

  public boolean handleMessage(Message msg)
  {
   li.onDone();  //UI-thread
   return true;
  }
}

