//---------------------
//Main
//(c) H.Buchmann FHNW 2020
//---------------------
package awe.layout;

import android.app.Activity;
import android.os.Bundle;


public class Main extends Activity 
{
  protected void onCreate(Bundle savedInstanceState) 
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
  }
}
