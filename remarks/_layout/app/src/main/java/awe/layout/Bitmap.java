//---------------------
//Bitmap.java
//(c) H.Buchmann FHNW 2020
//---------------------
package awe.layout;
import android.content.Context;
import android.util.AttributeSet;
import android.graphics.Canvas;
import android.graphics.BitmapFactory;
 
public class Bitmap extends android.view.View
{
 private final android.graphics.Bitmap figure;
 
 public Bitmap(Context context,AttributeSet attrs)
 {
  super(context,attrs);
  figure=BitmapFactory.decodeResource(context.getResources(),R.drawable.figure);
 }
 
 public void onDraw(Canvas canvas)
 {
  System.err.println("onDraw");
  canvas.drawBitmap(figure,0,0,null);
 }
 
}
