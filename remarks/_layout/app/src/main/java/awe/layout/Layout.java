//-----------------
//Layout.java
//(c) H.Buchmann FHNW 2020
//-----------------
package awe.layout;
import android.content.Context;
import android.util.AttributeSet;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Color;

import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.graphics.RectF;

public class Layout extends android.view.View
{
 final private Paint line1=new Paint();
 final private Paint line2=new Paint();
 final private Paint alpha=new Paint();
 final private Bitmap figure;
 final private RectF region=new RectF();
 
 private int x0,y0,x1,y1;
  
 public Layout(Context context,AttributeSet attrs)
 {
  super(context,attrs);
  line1.setStrokeWidth(5);
  line1.setColor(Color.YELLOW);
  line2.setStrokeWidth(5);
  line2.setColor(Color.GREEN);
  alpha.setAlpha(127);
  figure=BitmapFactory.decodeResource(context.getResources(),R.drawable.figure);

 }
 
 public void onLayout(boolean changed,
                      int x0,int y0,
		      int x1,int y1)
 {
  System.err.println("onLayout changed="+changed+" x0="+x0+" y0="+y0+
                                                 " x1="+x1+" y1="+y1);
  this.x0=x0;
  this.y0=y0;
  this.x1=x1;
  this.y1=y1;
  region.left=0;
  region.top=0;
  region.right=x1-x0;
  region.bottom=y1-y0;
 }

 public void onDraw(Canvas canvas)
 {
  System.err.println("onDraw");
  canvas.drawLine(x0,y0,x1,y1,line1);
  canvas.drawLine(x1,y0,x0,y1,line1);
  
  canvas.drawLine(0,0,x1-x0,y1-y0,line2);
  canvas.drawLine(x1-x0,0,0,y1-y0,line2);
  
  canvas.drawBitmap(figure,null,region,alpha);
 }
}
