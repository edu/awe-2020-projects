//---------------------
//Main
//(c) H.Buchmann FHNW 2020
//---------------------
package awe.power;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import java.util.Timer;
import java.util.TimerTask;
import android.os.SystemClock;

public class Main extends Activity 
{

  class OSTicker extends TimerTask
  {
   public void run()
   {
    System.err.println("os-tick");
   }
  }
  
  class PollTicker implements Runnable
  {
   PollTicker()
   {
    new Thread(this).start();
   }
   
   public void run()
   {
    System.err.println("os-tick");
    long current=SystemClock.currentThreadTimeMillis();
    long next   =current+1000;
    while(true)
    {
     current=SystemClock.currentThreadTimeMillis();
     if (next==current)
        {
	 System.err.println("poll-tick");
	 next=current+1000;
	}
    }
   }
  }
  
  protected void onCreate(Bundle savedInstanceState) 
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
  }
  
  public void osTick(View v)
  {
   new Timer().schedule(new OSTicker(),0,1000);
  }
  
  public void pollTick(View v)
  {
   new PollTicker();
  }
}
