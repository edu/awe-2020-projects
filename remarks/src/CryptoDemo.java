//---------------------
//CryptoDemo.java
//(c) H.Buchmann FHNW 2020
//using
//  java CryptoDemo enc key < path_to_file > path_to_chiffre
//                  dec key < pat_to_chiffre 
//  key= string
//---------------------
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.CipherOutputStream;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.SecretKeyFactory;
import java.security.spec.KeySpec;
import java.security.Key;

class CryptoDemo
{
 private final Cipher cipher=Cipher.getInstance("AES/ECB/PKCS5Padding");
 private final Key key;
 private final byte Salt[]={1,3,4,5};
 
 private CryptoDemo(String key) throws Exception
 {
  SecretKeyFactory fact=SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
  this.key=new SecretKeySpec(
           fact.generateSecret(new PBEKeySpec(key.toCharArray(),Salt,8,256)).getEncoded(),"AES"
	                    );
 }
 
 void enc() throws Exception
 {
  cipher.init(Cipher.ENCRYPT_MODE,key);
  CipherOutputStream  out=new CipherOutputStream(System.out,cipher);
  while(true)
  {
   int b=System.in.read();
   if (b<0) break;
   out.write(b);
  }
  out.close();
 }
 
 void dec() throws Exception
 {
  cipher.init(Cipher.DECRYPT_MODE,key);
  CipherInputStream  in=new CipherInputStream(System.in,cipher);
  while(true)
  {
   int b=in.read();
   if (b<0) break;
   System.out.write(b);
  }
  System.out.close();
  
 }
 
 private static void usage()
 {
  System.err.println("usage: java "+CryptoDemo.class.getName()+" enc|dec key\n"+
                     "   data from/to stdin/stdout");
  System.exit(1);
 }
 
 public static void main(String args[]) throws Exception
 {
  if (args.length!=2) usage();
  switch(args[0])
  {
   case "enc":
    new CryptoDemo(args[1]).enc();
   return;
   
   case "dec":
    new CryptoDemo(args[1]).dec();
   return;
  }
  usage();
 }
}
