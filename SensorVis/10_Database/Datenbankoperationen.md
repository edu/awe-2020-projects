# Operations

## Inserting

```sql
INSERT INTO envdata(time, location, temperature, humidity)
  VALUES (NOW(), 'office', 70.0, 50.0);
```


### Differenzen erkennen
>Beispiele mit anderen Namen!!

```sql
SELECT time, humidity FROM (
  SELECT time,
    humidity,
    humidity - LAG(humidity) OVER (ORDER BY time) AS diff
  FROM conditions) ht
WHERE diff IS NULL OR diff != 0;
```

### Buckets
```sql
SELECT time_bucket('1 minutes', time) AS five_min, avg(humidity)
  FROM conditions
  GROUP BY five_min
  ORDER BY five_min DESC LIMIT 12;
```

### First, Last

```sql
SELECT time_bucket('1 minutes', time) one_min, location, last(humidity, time)
  FROM conditions
  GROUP BY one_min, location
  ORDER BY one_min DESC LIMIT 12;
```

### Histogram
```sql
SELECT location, COUNT(*),
    histogram(humidity, 50.0, 85.0, 7)
   FROM conditions
   WHERE time > NOW() - interval '1 hours'
   GROUP BY location;
```

### Gap filling

```sql
SELECT
  time_bucket_gapfill('1 min'::interval, time, now() - '1 hours'::interval, now()) as onemin,
  location,
  locf(avg(humidity)) AS humidity
FROM conditions
WHERE
  time > now() - '1 hours'::interval
  
GROUP BY onemin, location;
```

```sql
SELECT
  time_bucket_gapfill('0.5 min'::interval, time, now() - '3 hours'::interval, now()) as onemin,
  locf(avg(humidity)) AS humidity,
  locf(avg(temperature)) AS temperature
FROM envdata
WHERE
  time > now() - '3 hours'::interval
  
GROUP BY onemin;
```

```sql
SELECT
  time_bucket_gapfill('0.5 min'::interval, time, '2020-04-15 14:08:25.650971+01', '2020-04-15 14:46:28.614523+01') as onemin,
  locf(avg(humidity)) AS humidity,
  locf(avg(temperature)) AS temperature
FROM envdata
WHERE (time >'2020-04-15 14:08:25.650971+01') and (time <'2020-04-15 14:46:28.614523+01' )
  
  
GROUP BY onemin;




SELECT
  time_bucket_gapfill('0.5 min'::interval, time) as onemin,
  locf(avg(humidity)) AS humidity,
  locf(avg(temperature)) AS temperature
FROM envdata
where (time >'2020-04-15 14:08:25.650971+01') and (time <'2020-04-15 14:46:28.614523+01' )
  
  
GROUP BY onemin;
```

```sql
SELECT
  time_bucket_gapfill('1 min'::interval, time, now() - '3 hours'::interval, now()) as onemin,
  locf(avg(humidity)) AS humidity
FROM conditions
WHERE
  time > now() - '3 hours'::interval
  
GROUP BY onemin;
```

### Join

```sql

select location.longitude, location.latitude,location.location_name, node.node_id from node inner join location on node.location_id = location.location_id;

```
```sql
select 
node.node_id as id, 
node.last_update,
count(envdata.*) as quantity,
location_name as loc 
from envdata
inner join node on envdata.node_id = node.node_id 
inner join location on location.location_id = node.location_id
group by node.node_id, location.location_id

```
