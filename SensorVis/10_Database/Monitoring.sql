

CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;
CREATE TYPE "state" AS ENUM (
  'STATE_OK',
  'STATE_ERR_CONN',
  'STATE_ERR_STOR',
  'STATE_ERR_SENS',
  'STATE_ERR_UNKN'
);

CREATE TYPE "sensortype" AS ENUM (
  'ENV_SENS',
  'VIB_SENS',
  'FLOW_SENS',
  'PRESS_SENS'
);

CREATE TABLE "node" (
  "node_id" integer primary key not null,
  "type" sensortype,
  "state" state,
  "location_id" integer,
  "last_update" timestamptz DEFAULT (now()),
  "description" text,
  "rssi" double precision

  
);

CREATE TABLE "location" (
  "location_id" integer primary key not null,
  "address" text,
  "longitude" double precision,
  "latitude" double precision,
  "location_name" text,
  "floor" integer,
  "room" text
);

CREATE TABLE "nodeconfig" (
  "node_id" integer not null,
  "configdata" text
);


CREATE TABLE "envdata" (
  "node_id" integer,
  "temperature" double precision,
  "humidity" double precision,
  "moisture" double precision,
  "light" double precision,
  "gasconcentration" double precision,
  "time" timestamptz NOT NULL DEFAULT (now())
);

SELECT create_hypertable('envdata', 'time');

ALTER TABLE "node" ADD FOREIGN KEY ("location_id") REFERENCES "location" ("location_id");

ALTER TABLE "envdata" ADD FOREIGN KEY ("node_id") REFERENCES "node" ("node_id");

ALTER TABLE "nodeconfig" ADD FOREIGN KEY ("node_id") REFERENCES "node" ("node_id");

CREATE USER internaluser WITH PASSWORD 'aweProjekt';
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO internaluser;
