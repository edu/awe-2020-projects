import sys

#set up psycopg2 environment
import psycopg2

#driving_distance module
#note the lack of trailing semi-colon in the query string, as per the Postgres documentation
query = """
    select *
    from envdata
"""

#make connection between python and postgresql
conn = psycopg2.connect("dbname = 'awe1' user = 'internaluser' host = '192.168.178.183' password = 'aweProjekt'")
cur = conn.cursor()

outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)

with open('resultsfile.csv', 'w') as f:
    cur.copy_expert(outputquery, f)

conn.close()