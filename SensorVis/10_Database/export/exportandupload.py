#!/usr/bin/python3 
from ftplib import FTP
import psycopg2
import fileinput
import os
import sys

conn = psycopg2.connect("dbname = 'awe' user = 'internaluser' host = '192.168.178.183' password = 'aweProjekt'")
cur = conn.cursor()


ftp = FTP()
ftp.set_debuglevel(2)
ftp.connect('timeo.bplaced.net', 21) 
tempfilename = "envdata.csv"


def listfiles():
    files = []

    try:
        files = ftp.nlst()
    except:
        raise

    for f in files:
        print(f)
    return files



def sendfile(filename):
    
    ftp.login('timeo','FTPAccess')
    fp = open(filename, 'rb')
    # upload file
    ftp.storbinary('STOR %s' % os.path.basename(filename), fp, 1024)
    fp.close()

def getfile(filename):
    ftp.login('timeo','FTPAccess')
    ftp.retrbinary("RETR " + filename, open("n_"+filename, 'wb').write)
    ftp.quit()

def export():
    try:
        query = """
        select *
        from envdata
        """
        outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
        with open(tempfilename, 'w') as f:
            cur.copy_expert(outputquery, f)
        conn.close()
        print("worked!")
        return 1
        
    except:
        return 0

if(export()):
    sendfile(tempfilename)