# Synchronisieren der Datenbanken

## Exportandupload.py

- Exportiert die Table envdata in ein csv-File und ladet das File auf den PHP Server
  
Wird mit crontab gescheudeled:

```bash
crontab -e

# m h  dom mon dow   command                                                                                             
30 * * * * root /home/pi/dbfiles/expandupload.py 

```
somit werden die Daten alle 60 Minuten hochgeladen

## download2db.py

>Zuerst Table `temp` erstellen:
```sql
CREATE TABLE "temp" (
  "node_id" integer,
  "temperature" double precision,
  "humidity" double precision,
  "moisture" double precision,
  "light" double precision,
  "gasconcentration" double precision,
  "time" timestamptz NOT NULL DEFAULT (now())
);
```

- Das CSV File wird heruntergeladen als `temp.csv` gespeichert.
- Der Inhalt des Files wird in die Tabelle `temp` geschrieben.
- Alle Einträge aus der Tabelle `temp`, welche noch nicht in der Tabelle `envdata` sind, werden nachgetragen.
- Das File `temp.csv` wird gelöscht

