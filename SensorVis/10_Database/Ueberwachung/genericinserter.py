import psycopg2
import datetime

host = "192.168.178.183"
#host="localhost"



conn = psycopg2.connect('host='+host+' user=internaluser password=aweProjekt dbname=awe')
cur = conn.cursor()
"""
time_ = datetime.datetime(2020,4,16,1,1,1)
err_content = "This is a test error"
err_type = "error"

print(time_)

query = "INSERT INTO err VALUES(%s,%s,%s)"
args = (err_content,err_type,time_, )
cur.execute(query,args)
conn.commit()
"""

colnames = ["node_id","temperature","humidity","moisture","light","gasconcentration"]
query = "SELECT * FROM envdata order by time desc limit 10"
cur.execute(query)
errs = (cur.fetchall())
lrec = []
for item in errs:
    #time_when = (item[6]+datetime.timedelta(hours=1)).strftime("%d.%m.%Y %H:%M:%S")
    time_when = (item[6]+datetime.timedelta(hours=1)).strftime("%H:%M:%S")
    entry ={"time":time_when,"node-id":item[0]}


    values = []
    index = 1
    while(index<6):
        if(item[index]):
            values.append(item[index])
            entry.update({colnames[index]:round(item[index],2)})
        index+=1
    
    lrec.append(entry)
print(lrec)
print("----")
for item in lrec:
    print(item)
