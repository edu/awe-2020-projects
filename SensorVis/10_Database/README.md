# Database

- Postgresql Database mit TimeScaleDB Erweiterung

## Setup / Installation

1. Postgresql "normal" installieren
```bash
sudo apt install postgresql libpq-dev postgresql-client postgresql-client-common -y 
```

2. server-dev installieren. Wird gebraucht, weil die Header dabei sind.
```bash
sudo apt install postgresql-server-dev-11 
```

3. Timescale aus Source installieren
```bash
git clone https://github.com/timescale/timescaledb.git
cd timescaledb
git checkout <release_tag>  # e.g., git checkout 1.6.1

# Bootstrap the build system (CMake install)
./bootstrap
# By Error Program 'pg_isolation_regress' not found,...
./bootstrap -DREGRESS_CHECKS=OFF

# To build the extension
cd build && make

# To install
make install
```

4. Anpassungen in postgresql.conf (z.B.: /etc/postgresql/11/main)
```bash
shared_preload_libraries = 'timescaledb'
```

5. Datenbank erstellen
```bash
sudo su postgres
psql 
create database awe;

\c awe;
```


## Database Design

Database Name : awe

### Enums:

```sql
enum state{
  STATE_OK
  STATE_ERR_CONN
  STATE_ERR_STOR
  STATE_ERR_SENS
  STATE_ERR_UNKN
}

enum sensortype{
  ENV_SENS
  VIB_SENS
  FLOW_SENS
  PRESS_SENS
}
```

### Tables

#### node
Name|type
-|-
node_id|int
type|sensortype (enum)
state|state (enum)
location_id|int
last_update|timestamp
description|text
rssi|double precision

#### location
Name|type
-|-
location_id|int
address|text
longitude|long
latitude|long
location_name|text
floor|int
room|text

Geo: 47.372938, 8.043970

#### envdata

Humidity, Moisture, Temperature, Light Density

&rarr; hypertable

Name|type
-|-
node_id|int
temperature|double precision
humidity|double precision
moisture|double precision
light|double precision
gasconcentration|double precision
time|timestamp

### User

username = internaluser
pw = aweProjekt



## Locations eintragen

```sql
INSERT INTO LOCATION VALUES(1,'Panoramaweg 2, 5035 Unterentfelden',47.372938,8.043970,'Timeo Home', 0, 'Living Room' );
INSERT INTO LOCATION VALUES(11,'Panoramaweg 2, 5035 Unterentfelden',47.372938,8.043970,'Timeo Home', 1, 'Entree' );
INSERT INTO LOCATION VALUES(12,'Panoramaweg 2, 5035 Unterentfelden',47.372938,8.043970,'Timeo Home', 0, 'Outdoor' );
INSERT INTO LOCATION VALUES(2,'Dorfstrasse 19, 5225 Bözberg',47.497396, 8.158408,'Barbara Home', 0, 'Living Room' );

```

## Node erfassen
```sql
INSERT INTO node VALUES(10, 'ENV_SENS','STATE_OK',1,DEFAULT, 'ESP8266, welches Zimmerpflanze und Umgebung überwacht');
INSERT INTO node VALUES(12, 'ENV_SENS','STATE_OK',11,DEFAULT, 'ESP8266 witty cloud mit LDR');
INSERT INTO node VALUES(15, 'ENV_SENS','STATE_OK',11,DEFAULT, 'Nucleo32, welches die Light Density und die Gaskonzentration mittels MQ-02 überwacht');

INSERT INTO node VALUES(9, 'ENV_SENS','STATE_OK',2,DEFAULT, 'dummy Sensor');
```

## Sensorwerte eintragen

```sql
INSERT INTO envdata values(10, 23.4, 54, 78.5, 20, DEFAULT);
```

## Error- Table
```sql
CREATE TABLE err (error_content text, err_type text, time timestamptz default(now()));
```

## Rules

>Hypertables do not support rules
```sql
CREATE RULE log_activity AS ON INSERT TO envdata DO ALSO UPDATE  node set last_update = DEFAULT where node_id = NEW.node_id;
```



## Copy Table


alles
```sql

insert into envdata select * from temp ;
```

duplikate löschen
```sql
select count(*) from temp right join envdata on temp.time = envdata.time;

select * from temp left join envdata on temp.time = envdata.time
where envdata.time is null;

select temp.node_id, temp.temperature, temp.humidity, temp.moisture, temp.light, temp.gasconcentration, temp.time from temp left join envdata on temp.time = envdata.time
where envdata.time is null;




insert into envdata select temp.node_id, temp.temperature, temp.humidity, temp.moisture, temp.light, temp.gasconcentration, temp.time from temp left join envdata on temp.time = envdata.time
where envdata.time is null;


insert into envdata select temp.* from temp left join envdata on temp.time = envdata.time
where envdata.time is null;

```

## Rule on Temptable
>geht nicht
```sql
CREATE RULE sync_db AS ON INSERT TO temp DO ALSO insert into envdata select temp.* from temp left join envdata on temp.time = envdata.time
where envdata.time is null;
```



