#!/usr/bin/python3

import paho.mqtt.client as mqtt
import json
import psycopg2

host = "192.168.178.183"
#host="localhost"

mqtt_keys = ["id","temp","humi","moist","lightdensity","gasconcentration"]

conn = psycopg2.connect('host='+host+' user=internaluser password=aweProjekt dbname=awe')
cur = conn.cursor()




def insertEnvData(pl_json):
    id = pl_json["id"]
    insertvalues=[]
    insertvalues.append(id)   
    valstr = "%s"
    i = 1
    while i<len(mqtt_keys):
        try:
            tmpval = pl_json[mqtt_keys[i]]
            insertvalues.append(tmpval)
            valstr+=",%s"
        except:
            valstr+=",DEFAULT"
        i+=1
    #print(valstr)
    postgres_insert_query = """
    INSERT INTO envdata 
    VALUES ("""
    postgres_insert_query+=valstr+")"
    record_to_insert = tuple(insertvalues)
    cur.execute(postgres_insert_query,record_to_insert)
    conn.commit()
    try:
        rssi = pl_json["rssi"]
        postgres_insert_query="""update node set last_update=DEFAULT, state='STATE_OK', rssi = %s where node_id = %s;"""
        record_to_insert=(rssi,id)
        cur.execute(postgres_insert_query,record_to_insert)
        conn.commit()
    except:
        postgres_insert_query="""update node set last_update=DEFAULT, state = 'STATE_OK' where node_id = %s;"""
        record_to_insert=(id,)
        cur.execute(postgres_insert_query,record_to_insert)
        conn.commit()

def selectType(msg):
    if(msg.topic=="monitoring/data/env"):
        json_payload = None
        try:
            json_payload = json.loads(msg.payload)
            print(json_payload)
            if(json_payload["id"]):
                insertEnvData(json_payload)
        except:
            print("parsing failed")
        

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("monitoring/#")


def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload.decode('utf-8')))
    selectType(msg)
    #handleData(str(msg.payload.decode('utf-8')))


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(host, 1883, 60)

client.loop_forever()
