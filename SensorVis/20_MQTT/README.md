# MQTT

## Verwendete Software

Mosquitto Broker auf Raspberry PI

[PubSubClient](https://github.com/knolleary/pubsubclient) auf Mikrocontrollern

## Datenstrukturen

### Daten von Node in die Datenbank

Topic
```
monitoring/data/<Datatype>
```
Payload
>Korrektes Json Format!

Inhalt:
```
ID (Integer)
status String (gemäss ENUM STATUS)
daten (Integer)
```

Beispielpaket:

Topic:
```
monitoring/data/env
```
Payload:
```json
{
    "id":10,
    "state":"STATE_OK",
    "temperature":21.6,
    "humidity":45,
    "moisture":72
}
```

