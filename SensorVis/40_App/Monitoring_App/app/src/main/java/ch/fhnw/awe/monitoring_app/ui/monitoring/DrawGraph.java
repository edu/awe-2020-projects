package ch.fhnw.awe.monitoring_app.ui.monitoring;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DrawGraph {
    private GraphView gv;
    private static Context mContext;
    private LineGraphSeries <DataPoint> temp;
    private LineGraphSeries <DataPoint> humi;
    private LineGraphSeries <DataPoint> gas;
    private LineGraphSeries <DataPoint> moist;

    private boolean data_available = false;

    DrawGraph(GraphView gv){
        this.gv=gv;
    }

    public void setGraph(DataPoint[] points, int maxinfs, int len){


        LineGraphSeries <DataPoint> series = new LineGraphSeries<>(points);
            /*
            LineGraphSeries <DataPoint> series = new LineGraphSeries<>(new DataPoint[]{
                    new DataPoint(0,1),
                    new DataPoint(1,5),
                    new DataPoint(5,3),
                    new DataPoint(20,4),
            });
            */
        gv.removeAllSeries();
       // gv.getViewport().setYAxisBoundsManual(true);
        //gv.getViewport().setMaxY(maxinfs);
        //gv.getGridLabelRenderer().setHorizontalAxisTitle("Days since startdate");
        //gv.getViewport().setScalable(true);
        //gv.getViewport().setScrollable(true);
        gv.getViewport().setMinimalViewport(0,len,0,maxinfs);
        //gv.getViewport().setMinX(100);


        //gv.getViewport().setMinX(len-1);
        //gv.getViewport().setMaxX(len+1);

        gv.addSeries(series);
        System.err.println("---^^^-----");

    }

    public void updateGraph(JSONObject data){
        try{

            boolean use_hhmm = false;
            String patterndaymonth = "dd.MM";
            SimpleDateFormat daymonthformat = new SimpleDateFormat(patterndaymonth);
            String patternhhmm = "HH:mm";
            SimpleDateFormat hhmmformat = new SimpleDateFormat(patternhhmm);

            JSONArray time = data.getJSONArray("time");
            JSONArray temperature = data.getJSONArray("temperature");
            JSONArray gas = data.getJSONArray("gasconcentration");
            JSONArray humi = data.getJSONArray("humidity");
            JSONArray moist = data.getJSONArray("moisture");


            int len = time.length();
            int min_time = time.getInt(0);
            int max_time = time.getInt(time.length()-1);
            int time_diff = (max_time-min_time)/3600;
            Date min_date = new java.util.Date(min_time*1000L);
            Date max_date = new java.util.Date(max_time*1000L);
            if(time_diff<48)use_hhmm=true;
            System.err.print("timediff in h:\t");
            System.err.println(time_diff);
            System.err.println(Integer.toString(time.length()));
            DataPoint[] p_temp = new DataPoint[len];
            DataPoint[] p_humi = new DataPoint[len];
            DataPoint[] p_gas = new DataPoint[len];
            DataPoint[] p_moist = new DataPoint[len];

           // int maxValue = 0;
            double temp_v=0;
            double humi_v = 0;
            double gas_v = 0;
            double moist_v = 0;
            for(int i = 0;i<time.length();++i){
                int time_ = time.getInt(i);
                Date date = new java.util.Date(time_*1000L);
                try{
                    /*
                System.err.print("moist:\t");
                System.err.print(moist.getDouble(i));
                System.err.print("\tgas:\t");
                System.err.println(gas.getDouble(i));

*/
                    temp_v = temperature.getDouble(i);
                    humi_v= humi.getDouble(i);
                    gas_v = gas.getDouble(i);
                    moist_v = moist.getDouble(i);
                }catch (Exception e) {
                }
                p_temp[i]=new DataPoint(date, temp_v);

                p_humi[i]=new DataPoint(date,humi_v);

                p_gas[i] = new DataPoint(date,gas_v);

                p_moist[i] = new DataPoint(date,moist_v);
            }
        this.temp = new LineGraphSeries<>(p_temp);
        this.humi = new LineGraphSeries<>(p_humi);
        this.gas = new LineGraphSeries<>(p_gas);
        this.moist=new LineGraphSeries<>(p_moist);

        gv.removeAllSeries();

        //gv.getViewport().setYAxisBoundsManual(true);
        //gv.getViewport().setMaxY(maxinfs);
        //gv.getGridLabelRenderer().setHorizontalAxisTitle("Days since startdate");
        //gv.getViewport().setScalable(true);
        //gv.getViewport().setScrollable(true);
        //gv.getViewport().setMinimalViewport(null,null,0,maxValue);
        //gv.getViewport().setMaxX(100);
            //   gv.getViewport().setMinX(min_time);

            this.humi.setColor(Color.GREEN);
            this.humi.setTitle("humidity");
            this.temp.setColor(Color.RED);
            this.temp.setTitle("temperature");

            this.moist.setTitle("moisture");
            this.moist.setColor(Color.BLACK);

            this.gas.setColor(Color.YELLOW);
            this.gas.setTitle("Air Pollution");


            gv.addSeries(this.temp);
            gv.addSeries(this.humi);
            gv.addSeries(this.moist);
            gv.addSeries(this.gas);
            gv.getLegendRenderer().setVisible(true);
            gv.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
            gv.getLegendRenderer().setBackgroundColor(Color.argb(0,0,0,0));

        if(use_hhmm){
            gv.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(mContext, hhmmformat));
        }else {
            gv.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(mContext, daymonthformat));
        }
           gv.getGridLabelRenderer().setNumHorizontalLabels(6); // only 4 because of the space
            gv.getViewport().setScalable(true);

// set manual x bounds to have nice steps

            gv.getViewport().setMinX(this.temp.getLowestValueX());
            gv.getViewport().setMaxX(this.temp.getHighestValueX());
           // gv.getViewport().setMaxY(maxValue);
           //gv.getViewport().setXAxisBoundsManual(true);
           // gv.getGridLabelRenderer().setHumanRounding(false);


        data_available=true;


        }catch (Exception ex) {
            System.err.println("ERROR WHILE DRAWING GRAPH");
            System.err.println(ex);
        }

    }

    public void setVisibleSeries(String series, boolean visible, Activity a){

        if(!data_available){
            Toast.makeText( a, "No Data Here! check connection!",
                    Toast.LENGTH_LONG).show();

        }else {
            if (series == "temp") {
                if (!visible) {
                    if ((gv.getSeries().contains(this.temp))) {
                        gv.removeSeries(this.temp);
                    }

                } else {
                    if (!(gv.getSeries().contains(this.temp))) {
                        gv.addSeries(this.temp);
                    }
                }
            }
            if (series == "humi") {
                if (!visible) {
                    if ((gv.getSeries().contains(this.humi))) {
                        gv.removeSeries(this.humi);
                    }

                } else {
                    if (!(gv.getSeries().contains(this.humi))) {
                        gv.addSeries(this.humi);
                    }
                }
            }
            if (series == "gas") {
                if (!visible) {
                    if ((gv.getSeries().contains(this.gas))) {
                        gv.removeSeries(this.gas);
                    }

                } else {
                    if (!(gv.getSeries().contains(this.gas))) {
                        gv.addSeries(this.gas);
                    }
                }
            }
            if (series == "moist") {
                if (!visible) {
                    if ((gv.getSeries().contains(this.moist))) {
                        gv.removeSeries(this.moist);
                    }

                } else {
                    if (!(gv.getSeries().contains(this.moist))) {
                        gv.addSeries(this.moist);
                    }
                }
            }
        }
    }


}
