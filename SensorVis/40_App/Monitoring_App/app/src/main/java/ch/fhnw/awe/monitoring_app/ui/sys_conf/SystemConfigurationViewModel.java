package ch.fhnw.awe.monitoring_app.ui.sys_conf;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SystemConfigurationViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SystemConfigurationViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Set Server IP and Port");
    }

    public LiveData<String> getText() {
        return mText;
    }
}