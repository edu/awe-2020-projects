package ch.fhnw.awe.monitoring_app;

import android.app.Application;


public class SystemClass{

    private static SystemClass mInstance= null;
    public String ip = "192.168.178.130";
    public String port = "5500";
    public boolean connection_ok = false;
    public int responsetime;
    public long min_time = 1586956105;
    public boolean use_https = false;

    protected SystemClass(){}

    public static synchronized SystemClass getInstance() {
        if(null == mInstance){
            mInstance = new SystemClass();
        }
        return mInstance;
    }
}

/*
public class SystemClass extends Application {

    private String ip;
    private int port;

    public String getServerIP(){
        return ip;
    }

    public int getServerPort(){
        return port;
    }

    public void setServerIP(String ip){
        this.ip = ip;
    }

    public void setServerPort(int port){
        this.port = port;
    }

}
*/