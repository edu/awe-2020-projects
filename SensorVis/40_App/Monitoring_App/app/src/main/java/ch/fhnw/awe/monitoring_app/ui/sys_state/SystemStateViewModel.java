package ch.fhnw.awe.monitoring_app.ui.sys_state;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SystemStateViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SystemStateViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("State");
    }

    public LiveData<String> getText() {
        return mText;
    }
}