package ch.fhnw.awe.monitoring_app.ui.sys_conf;

import android.app.Application;
import android.graphics.Color;
import android.os.AsyncTask;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;

import org.json.JSONObject;

import ch.fhnw.awe.monitoring_app.SystemClass;

public class ConnectionTester extends AsyncTask<String,Void,String>  {
    private String ip;
    private String port;
    private TextView resulttext;

    String server_response;
    boolean succeeded = false;

    int http_code;
    long startingmillis;

    ConnectionTester(String ip, String port,TextView resulttext){
        this.ip = ip;
        this.port = port;
        this.resulttext = resulttext;
    }





    @Override
    protected String doInBackground(String... strings){
        startingmillis = System.currentTimeMillis();

        try {
            URL url;
            HttpURLConnection urlconn=null;

            if(SystemClass.getInstance().use_https){
                url = new URL("https://"+ip+":"+port+"/test");
            }else{
                url = new URL("http://"+ip+":"+port+"/test");
            }


            System.err.println("Testing URL:"+url);
            urlconn = (HttpURLConnection)url.openConnection();
            urlconn.setRequestMethod("GET");
            http_code = urlconn.getResponseCode();

            if(http_code==HttpURLConnection.HTTP_OK){
                succeeded=true;
                InputStream in = urlconn.getInputStream();

                BufferedReader reader = null;
                StringBuffer response = new StringBuffer();
                try {
                    reader = new BufferedReader(new InputStreamReader(in));
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                server_response= response.toString();
                return server_response;
            }
            return null;







    }catch(Exception ex)
        {
            System.err.println("CONN ERR! cause:");
            System.err.print(ex);
            return server_response;
        }

    }

    @Override
    protected void onPostExecute(String s) {
        long endmillis = System.currentTimeMillis();
        int timediff = (int)(endmillis-startingmillis);
        SystemClass.getInstance().responsetime = timediff;
        if(succeeded){
            System.err.println(server_response);
            try {
                JSONObject obj = new JSONObject(server_response);
                boolean state = obj.getBoolean("connection");
                int min_time = obj.getInt("min_time");
                if(state){
                    System.err.println("CONNECTION OK");
                    SystemClass.getInstance().ip = this.ip;
                    SystemClass.getInstance().port= this.port;
                    SystemClass.getInstance().connection_ok = true;
                    SystemClass.getInstance().min_time=min_time;
                    resulttext.setText("Connection OK!\nIP and Port are saved.");
                    resulttext.setTextColor(Color.rgb(0,200,0));
                }else{
                    resulttext.setText("Connecting failed!\nInvalid Server response!");
                    resulttext.setTextColor(Color.RED);
                }
            }catch (Exception e){
                resulttext.setText("Connecting failed!\nInvalid Server response!");
                resulttext.setTextColor(Color.RED);

            }

        }else {
            resulttext.setText("Connecting failed!\nHTTP response code was "+Integer.toString(http_code));
            resulttext.setTextColor(Color.RED);

            System.err.println("WRONG IP OR PORT!");
        }

        super.onPostExecute(s);
    }
}

