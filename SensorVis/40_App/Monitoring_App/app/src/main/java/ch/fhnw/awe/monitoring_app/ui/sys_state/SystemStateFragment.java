package ch.fhnw.awe.monitoring_app.ui.sys_state;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import ch.fhnw.awe.monitoring_app.R;
import ch.fhnw.awe.monitoring_app.ui.monitoring.Client;

public class SystemStateFragment extends Fragment {

    private SystemStateViewModel systemStateViewModel;
    private TextView errors, warnings;
    private StateClient stateclient;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        systemStateViewModel =
                ViewModelProviders.of(this).get(SystemStateViewModel.class);
        View root = inflater.inflate(R.layout.fragment_sys_state, container, false);
        errors = (TextView) root.findViewById(R.id.text_sys_state_errors);
        warnings = (TextView) root.findViewById(R.id.text_sys_state_warnings);
        stateclient=new StateClient(errors,warnings);
        stateclient.execute("go");

        return root;
    }
}