package ch.fhnw.awe.monitoring_app.ui.connection;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import ch.fhnw.awe.monitoring_app.R;
import ch.fhnw.awe.monitoring_app.SystemClass;

public class ConnectionFragment extends Fragment {

    private ConnectionViewModel sendViewModel;
    private TextView show_ip;
    private TextView show_port;
    private TextView show_time;
    private TextView show_connok;
    private TextView show_encription;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        sendViewModel =
                ViewModelProviders.of(this).get(ConnectionViewModel.class);
        View root = inflater.inflate(R.layout.fragment_connection, container, false);
        final TextView textView = root.findViewById(R.id.text_send);
        sendViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        show_ip = root.findViewById(R.id.show_ip);
        show_port=root.findViewById(R.id.show_port);
        show_time=root.findViewById(R.id.show_time);
        show_connok=root.findViewById(R.id.show_conntested);
        show_encription = root.findViewById(R.id.is_ssl);

        if(SystemClass.getInstance().use_https){
            show_encription.setText("SSL");
        }


        if(SystemClass.getInstance().connection_ok){
            show_connok.setText("Connection is OK.");
            show_connok.setTextColor(Color.rgb(0,200,0));
        }else{
            show_connok.setText("Connection is not tested yet.\n" +
                    "Default Parameters:");
            show_connok.setTextColor(Color.rgb(200,0,0));
        }
        show_ip.setText(SystemClass.getInstance().ip);
        show_port.setText(SystemClass.getInstance().port);

        show_time.setText(Integer.toString(SystemClass.getInstance().responsetime)+" ms");

        return root;
    }
}