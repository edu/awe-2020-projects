package ch.fhnw.awe.monitoring_app.ui.monitoring;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.io.IOException;
import java.net.URL;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.Date;

import android.os.AsyncTask;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONArray;
import org.json.JSONObject;

import org.json.*;

import ch.fhnw.awe.monitoring_app.SystemClass;


public class Client extends AsyncTask<String,Void,String>{

    private String port;
    private Object answer;
    private  String   ip = "192.168.2.96";
    //private  String   ip = "192.168.178.130";
    private DrawGraph dg;
    private String body_s;
    private long startingmillis;
    private long endingmillis;



    String server_response;

    Client(DrawGraph dg, String post_json_string){
        this.dg = dg;
        this.ip = SystemClass.getInstance().ip;
        this.port = SystemClass.getInstance().port;
        if(post_json_string=="default") {
            System.err.println("def");
            JSONObject data = new JSONObject();
            try {
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                long timestamp_s = timestamp.getTime() / 1000 - (24 * 60 * 60)+3600;
                long timestamp_e = timestamp.getTime() / 1000;
                data.put("start", timestamp_s);
                data.put("end", timestamp_e);
            } catch (Exception e) {

            }

            this.body_s = data.toString();
        }
        else{
            this.body_s=post_json_string;
        }
    }




    @Override
    protected String doInBackground(String... strings){
        startingmillis = System.currentTimeMillis();

        try {
            URL url;
            HttpURLConnection urlconn=null;


           if(SystemClass.getInstance().use_https){
               url = new URL("https://"+ip+":"+port+"/getdata");
           }else{
               url = new URL("http://"+ip+":"+port+"/getdata");
           }

            System.err.println("URL is:"+url);
            urlconn = (HttpURLConnection)url.openConnection();
            urlconn.setRequestMethod("POST");
            urlconn.setDoInput(true);
            urlconn.setDoOutput(true);
            urlconn.setUseCaches(false);
            urlconn.setRequestProperty("Content-Type","application/json");
            urlconn.setRequestProperty("Content-Length",String.valueOf(body_s.length()));
            OutputStreamWriter writer = new OutputStreamWriter(urlconn.getOutputStream());
            writer.write(body_s);
            writer.flush();

            int responseCode = urlconn.getResponseCode();

            System.err.println("HTTP Code is:"+Integer.toString(responseCode));
            if(responseCode == HttpURLConnection.HTTP_OK) {
                InputStream in = urlconn.getInputStream();

                BufferedReader reader = null;
                StringBuffer response = new StringBuffer();
                try {
                    reader = new BufferedReader(new InputStreamReader(in));
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                server_response= response.toString();
                endingmillis = System.currentTimeMillis();
                int size = server_response.length();
                System.err.println("response has size of "+Integer.toString(size));

            }
            //Socket sock = new Socket(ip, PORT);
            //ObjectOutputStream dst = new ObjectOutputStream(sock.getOutputStream());
            //dst.write("/co/"+start+"/"+end);
            return null;

        }catch(Exception ex)
        {
            System.err.println("CONN ERR! cause:");
            System.err.print(ex);
            return "Connection Error.";
        }
    }
    @Override
    protected void onPostExecute(String s) {
        try {
            JSONObject obj = new JSONObject(server_response);
            //dg = new DrawGraph(gv);
            this.dg.updateGraph(obj);
            SystemClass.getInstance().connection_ok=true;
            SystemClass.getInstance().responsetime=(int)(endingmillis-startingmillis);


            super.onPostExecute(s);
           // System.err.println("Response" + server_response);
        }catch (Exception ex){
            //tv.setText("could not parse Json!");
            System.err.println(ex);
        }


    }

}

