package ch.fhnw.awe.monitoring_app.ui.sys_state;

import android.graphics.Color;
import android.os.AsyncTask;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import ch.fhnw.awe.monitoring_app.SystemClass;

public class StateClient extends AsyncTask<String,Void,String> {

    private TextView err;
    private TextView warn;
    private long startingmillis;
    private long endingmillis;
    private String server_response;
    private String ip;
    private String port;
    private int http_code;
    private boolean succeeded=false;

    StateClient(TextView err, TextView warn){
        this.err=err;
        this.warn=warn;
        this.ip= SystemClass.getInstance().ip;
        this.port=SystemClass.getInstance().port;
    }



    @Override
    protected String doInBackground(String... strings) {
        startingmillis = System.currentTimeMillis();

        try {
            URL url;
            HttpURLConnection urlconn=null;


            if(SystemClass.getInstance().use_https){
                url = new URL("https://"+ip+":"+port+"/geterr");
            }else{
                url = new URL("http://"+ip+":"+port+"/geterr");
            }

            System.err.println("URL:"+url);
            urlconn = (HttpURLConnection)url.openConnection();
            urlconn.setRequestMethod("GET");
            http_code = urlconn.getResponseCode();

            if(http_code==HttpURLConnection.HTTP_OK){
                succeeded=true;
                InputStream in = urlconn.getInputStream();

                BufferedReader reader = null;
                StringBuffer response = new StringBuffer();
                try {
                    reader = new BufferedReader(new InputStreamReader(in));
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                server_response= response.toString();
                return server_response;
            }
            return null;







        }catch(Exception ex)
        {
            System.err.println("CONN ERR! cause:");
            System.err.print(ex);
            return server_response;
        }

    }





    @Override
    protected void onPostExecute(String s) {
        endingmillis=System.currentTimeMillis();
        if(succeeded){
            try {
                JSONObject obj = new JSONObject(server_response);
                JSONArray errs = obj.getJSONArray("errors");
                JSONArray warnings = obj.getJSONArray("warnings");
                int errlen = errs.length();
                int warnlen = warnings.length();

                if(errlen>4)errlen=4;
                if(warnlen>4)warnlen=4;
                String error_str = "";
                String warn_str = "";
                for(int i = 0;i<errlen;++i){
                    if(i!=0)error_str+="\n\n";
                    JSONObject entry = errs.getJSONObject(i);
                    error_str+=entry.getString("time")+": ";
                    error_str+=entry.getString("descr");
                }
                err.setText(error_str);

                for(int i = 0;i<warnlen;++i){
                    if(i!=0)warn_str+="\n\n";
                    JSONObject entry = warnings.getJSONObject(i);
                    warn_str+=entry.getString("time")+": ";
                    warn_str+=entry.getString("descr");
                }
                warn.setText(warn_str);



                SystemClass.getInstance().connection_ok=true;
                SystemClass.getInstance().responsetime=(int)(endingmillis-startingmillis);
            }catch (Exception e){
                err.setText("NO CONNECTION TO SERVER!");


            }

        }
        System.err.println(server_response);


        super.onPostExecute(s);
    }
}
