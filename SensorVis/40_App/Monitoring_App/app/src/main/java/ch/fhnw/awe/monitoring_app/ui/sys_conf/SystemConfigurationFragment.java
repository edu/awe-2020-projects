package ch.fhnw.awe.monitoring_app.ui.sys_conf;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import ch.fhnw.awe.monitoring_app.R;
import ch.fhnw.awe.monitoring_app.SystemClass;
import android.widget.Toast;

public class SystemConfigurationFragment extends Fragment {

    private SystemConfigurationViewModel systemConfigurationViewModel;
    private EditText et_ip;
    private EditText et_port;
    private Button conn_test_button;
    private ConnectionTester ct;
    private TextView tv_connres;
    private CheckBox encr;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        systemConfigurationViewModel =
                ViewModelProviders.of(this).get(SystemConfigurationViewModel.class);
        View root = inflater.inflate(R.layout.fragment_sys_conf, container, false);
        final TextView textView = root.findViewById(R.id.text_tools);
        systemConfigurationViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        conn_test_button=root.findViewById(R.id.conntestbtn);
        et_ip = root.findViewById(R.id.et_ip);
        et_port=root.findViewById(R.id.et_port);
        tv_connres = root.findViewById(R.id.conntestresult);
        encr = root.findViewById(R.id.encription_btn);

        et_ip.setText(SystemClass.getInstance().ip);
        et_port.setText(SystemClass.getInstance().port);
        encr.setChecked(SystemClass.getInstance().use_https);

        encr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    SystemClass.getInstance().use_https=true;
                }else{
                    SystemClass.getInstance().use_https=false;
                }
            }
        });

        conn_test_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Testing Connection...",
                        Toast.LENGTH_LONG).show();
                tv_connres.setText("Testing connection to Server...");

                String ip = (String) et_ip.getText().toString();
                String port = (String) et_port.getText().toString();
                ct = new ConnectionTester(ip,port,tv_connres);
                ct.execute("go");


            }
        });


        return root;
    }
}