package ch.fhnw.awe.monitoring_app.ui.home;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.AnimatedStateListDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;

import ch.fhnw.awe.monitoring_app.R;
import ch.fhnw.awe.monitoring_app.SystemClass;
import ch.fhnw.awe.monitoring_app.ui.monitoring.MonitoringFragment;

public class HomeFragment extends Fragment {

    private Button btnmonitoring, btnpackets, btnserver, btnstate, btnconnect;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             final ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        btnmonitoring = (Button) root.findViewById(R.id.btn_monitoring);
        btnconnect=       (Button) root.findViewById(R.id.btn_check_connection);
        btnpackets= (Button) root.findViewById(R.id.btn_rt_msg);
        btnstate=(Button) root.findViewById(R.id.btn_state);
        btnserver = (Button) root.findViewById(R.id.btn_conn_info);

        if(!SystemClass.getInstance().connection_ok){
            btnmonitoring.setTextColor(Color.rgb(200,200,200));
            btnserver.setTextColor(Color.rgb(200,200,200));
            btnpackets.setTextColor(Color.rgb(200,200,200));
            btnstate.setTextColor(Color.rgb(200,200,200));
            btnconnect.setBackgroundColor(Color.rgb(200,0,0));

            btnconnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);


                    navController.navigate(R.id.nav_sys_conf);
                }
            });


        }else {

            btnconnect.setBackgroundColor(Color.rgb(0,150,0));
            btnconnect.setText("connected to server");


            btnmonitoring.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                    navController.navigate(R.id.nav_monitoring);
                }
            });

            btnpackets.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                    navController.navigate(R.id.nav_config);
                }
            });

            btnserver.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                    navController.navigate(R.id.nav_connection);
                }
            });

            btnstate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                    navController.navigate(R.id.nav_sys_state);
                }
            });

        }

        return root;
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navController.navigate(R.id.nav_home);

    }




}