package ch.fhnw.awe.monitoring_app.ui.home;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridLayout;
import android.widget.ImageView;

public class homeGridViewItem extends GridLayout {


    public homeGridViewItem(Context context) {
        super(context);
    }

    public homeGridViewItem(Context context, AttributeSet attrs) {
        super(context,attrs);
    }
    public homeGridViewItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(heightMeasureSpec < widthMeasureSpec) {
            super.onMeasure(heightMeasureSpec, heightMeasureSpec);
        } else if(widthMeasureSpec < heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        }

    }
}
