package ch.fhnw.awe.monitoring_app.ui.monitoring;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ch.fhnw.awe.monitoring_app.R;
import ch.fhnw.awe.monitoring_app.SystemClass;

public class MonitoringFragment extends Fragment {

    private MonitoringViewModel monitoringViewModel;
    private Spinner spinnerSensorNode;
    private GraphView gv;
    private TextView startDate, endDate;
    private CheckBox humi, temp, humiPlant, airPoll;
    private Button btnStartDate, btnEndDate;
    private Button btnuseAttitude;
    private DatePickerDialog mDatePickerS, mDatePickerE;
    private Calendar mcurrentDate,mcurrentDateS, mcurrentDateE;
    private int yearS, monthS, dayS, yearE, monthE, dayE;
    private Client client;
    private String dates[]={"5.5.20","5.4.20"};
    private DrawGraph dg;
    private LinearLayout layout;
    private long selectedstartdate=0, selectedenddate=0;


    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        monitoringViewModel =
                ViewModelProviders.of(this).get(MonitoringViewModel.class);

        View root = inflater.inflate(R.layout.fragment_monitoring, container, false);

        gv = (GraphView) root.findViewById(R.id.graph);
        startDate = (TextView) root.findViewById(R.id.text_startDate);
        endDate = (TextView) root.findViewById(R.id.text_endDate);
        humi = (CheckBox) root.findViewById(R.id.checkbox_humidity);
        temp = (CheckBox) root.findViewById(R.id.checkbox_temperature);
        humiPlant = (CheckBox) root.findViewById(R.id.checkbox_humidityPlant);
        airPoll = (CheckBox) root.findViewById(R.id.checkbox_airPollution);
        btnStartDate = (Button) root.findViewById(R.id.btn_cal_startDate);
        btnEndDate = (Button) root.findViewById(R.id.btn_cal_endDate);
        btnuseAttitude = (Button) root.findViewById(R.id.btn_useAttitude);
        layout = (LinearLayout)root.findViewById(R.id.linear_layout_moni);
        getDate();



        if(getActivity().getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){
            layout.setWeightSum(65);
        }else{
            layout.setWeightSum(100);
        }



        this.dg = new DrawGraph(gv);
        //client = new Client("192.168.2.96",dg,gv,"default");
        client = new Client(dg,"default");
        client.execute(dates);
        final Activity a = getActivity();
        humi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if(isChecked){

                    dg.setVisibleSeries("humi",true,a);
                }else{
                    dg.setVisibleSeries("humi",false,a);
                }

            }
        });
        temp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){

                    dg.setVisibleSeries("temp",true,a);
                }else{
                    dg.setVisibleSeries("temp",false,a);
                }

            }
        });
        humiPlant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){

                    dg.setVisibleSeries("moist",true,a);
                }else{
                    dg.setVisibleSeries("moist",false,a);
                }

            }
        });
        airPoll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){

                    dg.setVisibleSeries("gas",true,a);
                }else{
                    dg.setVisibleSeries("gas",false,a);
                }

            }
        });



        btnuseAttitude.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAttitude();


            }
        });

        btnStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatePickerS=new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedYear, int selectedMonth, int selectedDay) {
                        startDate.setText(selectedDay + "." + (selectedMonth+1)  + "." + selectedYear);
                        yearS =selectedYear;
                        monthS = selectedMonth;
                        dayS = selectedDay;

                    }
                },yearS, monthS, dayS);
                mDatePickerS.setTitle("Select Startdate");


                Calendar cal = Calendar.getInstance();

                    mDatePickerS.getDatePicker().setMaxDate(cal.getTimeInMillis());




                mDatePickerS.getDatePicker().setMinDate(SystemClass.getInstance().min_time*1000L);

                mDatePickerS.show();

            }
        });

        btnEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatePickerE=new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedYear, int selectedMonth, int selectedDay) {
                        endDate.setText(selectedDay + "." + (selectedMonth+1)  + "." + selectedYear);
                        yearE =selectedYear;
                        monthE = selectedMonth;
                        dayE = selectedDay;

                    }
                },yearE, monthE, dayE);
                selectedenddate=componentTimeToTimestamp(yearE,monthE,dayE,0,0);
                mDatePickerE.setTitle("Select Enddate");
                Calendar cal = Calendar.getInstance();


                mDatePickerE.getDatePicker().setMinDate(SystemClass.getInstance().min_time*1000L);
                mDatePickerE.getDatePicker().setMaxDate(cal.getTimeInMillis());

                mDatePickerE.show();

            }
        });




        return root;
    }

    public void getDate(){
        mcurrentDate=Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        startDate.setText(day + "." + (month+1)  + "." + year);
        endDate.setText(day + "." + (month+1)  + "." + year);
        yearS = year;
        yearE = year;
        monthS = month;
        monthE = month;
        dayS = day;
        dayE = day;
    }







    public void setAttitude(){
        //client = new Client("192.168.2.96",gv);
        int starting = componentTimeToTimestamp(yearS,monthS,dayS,0,0);
        System.err.println(starting);
        int ending = componentTimeToTimestamp(yearE,monthE,dayE,0,0);
        if(starting>ending){
            int temp = ending;
            ending=starting;
            starting=temp;
        }
        String json_timeinfo = "default";
        if(starting<ending) {

            JSONObject timeinfo = new JSONObject();
            try {
                timeinfo.put("start", starting);
                timeinfo.put("end", ending);
                json_timeinfo = timeinfo.toString();
                System.err.println(json_timeinfo);
            } catch (Exception e) {

            }
        }

        client = new Client(dg,json_timeinfo);
        client.execute(dates);
        temp.setChecked(true);
        humi.setChecked(true);
        airPoll.setChecked(true);
        humiPlant.setChecked(true);
    }

    int componentTimeToTimestamp(int year, int month, int day, int hour, int minute) {

        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        c.set(Calendar.HOUR, hour);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return (int) (c.getTimeInMillis() / 1000L);
    }

    private RelativeLayout.LayoutParams paramsNotFullscreen; //if you're using RelativeLatout

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) //To fullscreen
        {
               layout.setWeightSum(65);
        }
        else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
        {
            layout.setWeightSum(100);

        }
        super.onConfigurationChanged(newConfig);
    }

}