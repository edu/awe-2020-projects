package ch.fhnw.awe.monitoring_app.ui.config;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import ch.fhnw.awe.monitoring_app.R;

public class ConfigFragment extends Fragment {

    private ConfigViewModel configViewModel;
    private View root;

    private TextView tv;

    private Button refresh_btn;


    private LastDataClient lc;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configViewModel = ViewModelProviders.of(this).get(ConfigViewModel.class);
        root = inflater.inflate(R.layout.fragment_config, container, false);


        tv = root.findViewById(R.id.lastMsg);
        refresh_btn = root.findViewById(R.id.refresh_btn);
        /*
        //Intent intent = getIntent();
        TextView textView = root.findViewById(R.id.text_config_json);
        //textView.setText(message);
        btn = (Button) root.findViewById(R.id.btn_config_check);
        btnnew=(Button) root.findViewById(R.id.btn_config_create_new);
        dropdown = (Spinner)root.findViewById(R.id.spinner_config);
        checkbtn = (Button)root.findViewById(R.id.btn_config_check);
        idet = (EditText)root.findViewById(R.id.txtedit_config_set_id);

        alert = new AlertDialog.Builder(this.getContext());
*/
        lc = new LastDataClient(tv);
        lc.execute("go");

        refresh_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lc = new LastDataClient(tv);
                lc.execute("go");
            }
        });

/*
        btnnew.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                TextView tv = (TextView) root.findViewById(R.id.text_config_json);
                tv.setText("creating new config File");
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {

                Toast.makeText(getContext(),"Loading Templates",Toast.LENGTH_SHORT).show();
                dropdown.setVisibility(View.VISIBLE);
                String[] items = new String[]{"generic","BKE", "ZGV", "ES", "VIB","THM"};
                adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, items);
                dropdown.setAdapter(adapter);

            }
        });
        checkbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Toast.makeText(getContext(), "checking ID " + idet.getText(), Toast.LENGTH_SHORT).show();

          //      input(0);


            }


        });
*/
        return root;

    }
    /**
    public void input(final int stage){
        final EditText input = new EditText(this.getContext());
        input.setText(defVals[stage]);

        new AlertDialog.Builder(this.getContext())
                .setIcon(android.R.drawable.ic_menu_upload)
                .setTitle(titlesrc[stage])
                .setMessage("")
                .setView(input)

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getContext(), "okok.."+Integer.toString(stage), Toast.LENGTH_SHORT).show();
                        //dropdown.setVisibility(View.INVISIBLE);
                        //finish();
                        if(stage<titlesrc.length-1){
                            input(stage+1);
                        }

                    }

                })
                .setNegativeButton("Cancel", null )
                .show();
    }


/**
    public void onBackPressed() {
        new AlertDialog.Builder(this.getContext())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing Activity")
                .setMessage("Are you sure you want to close this activity?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }**/

}