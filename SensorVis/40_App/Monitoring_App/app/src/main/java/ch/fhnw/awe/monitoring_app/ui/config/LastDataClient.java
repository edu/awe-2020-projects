package ch.fhnw.awe.monitoring_app.ui.config;

import android.graphics.Color;
import android.os.AsyncTask;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import ch.fhnw.awe.monitoring_app.SystemClass;

public class LastDataClient extends AsyncTask<String,Void,String> {
    private String ip;
    private String port;
    private TextView resulttext;

    String server_response;
    boolean succeeded = false;

    int http_code;
    long startingmillis;

    LastDataClient(TextView tv){
        this.resulttext = tv;
        this.ip=SystemClass.getInstance().ip;
        this.port=SystemClass.getInstance().port;
    }


    @Override
    protected String doInBackground(String... strings){
        startingmillis = System.currentTimeMillis();

        try {
            URL url;
            HttpURLConnection urlconn=null;

            if(SystemClass.getInstance().use_https){
                url = new URL("https://"+ip+":"+port+"/getlast");
            }else{
                url = new URL("http://"+ip+":"+port+"/getlast");
            }


            System.err.println("Testing URL:"+url);
            urlconn = (HttpURLConnection)url.openConnection();
            urlconn.setRequestMethod("GET");
            http_code = urlconn.getResponseCode();

            if(http_code==HttpURLConnection.HTTP_OK){
                succeeded=true;
                InputStream in = urlconn.getInputStream();

                BufferedReader reader = null;
                StringBuffer response = new StringBuffer();
                try {
                    reader = new BufferedReader(new InputStreamReader(in));
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                server_response= response.toString();
                return server_response;
            }
            return null;




        }catch(Exception ex)
        {
            System.err.println("CONN ERR! cause:");
            System.err.print(ex);
            return server_response;
        }

    }

    @Override
    protected void onPostExecute(String s) {
        if(succeeded){
        long endmillis = System.currentTimeMillis();
        int timediff = (int)(endmillis-startingmillis);
        SystemClass.getInstance().responsetime = timediff;

            System.err.println(server_response);
            try {
                JSONArray last = new JSONArray(server_response);
                int len = last.length();
                String last_rec = "";
                for(int i = 0;i<len;++i){
                    if(i!=0)last_rec+="\n\n";
                    JSONObject entry = last.getJSONObject(i);
                    last_rec+= entry.getString("time");
                    last_rec+= " Node-";
                    last_rec+= Integer.toString(entry.getInt("node_id"));
                    last_rec+="\n";
                    entry.remove("time");
                    entry.remove("node_id");
                    last_rec+=entry.toString();
                }
                resulttext.setText(last_rec);


            }catch (Exception e){
                resulttext.setText("Connecting failed!\nInvalid Server response!");
                resulttext.setTextColor(Color.RED);

            }

        }else {
            resulttext.setText("Connecting failed!\nHTTP response code was "+Integer.toString(http_code));
            resulttext.setTextColor(Color.RED);


        }

        super.onPostExecute(s);
    }
}
