# Node

## Nodes im System

ID|Ort|Typ|Werte|Beschreibung
-|-|-|-|-
10|Timeo, Wohnzimmer|Umgebungsdaten|Relative Luftfeuchtigkeit (%), Humusfeuchtigkeit Zimmerpflanze (%), Temperatur (°C)|Sensor Node aus ESP8266
15|Timeo, Eingangsbereich|Umgebungsdaten|Light Density (%), Gaskonzentration brennbarer Gase (%)|Nucleo32, kommunikation über Ethernet
9|Timeo, Aussen|Umgebungsdaten|Light Density|ESP8266 Witty Cloud mit eingebautem LDR
1|Barbara, Wohnraum|Umgebungsdaten|Relative Feuchtigkeit (%), Temperatur (°C), Humusfeuchtigkeit Zimmerpflanze (%), Gaskonzentration brennbarer Gase (%)| ESP32 TTGO mit Display|


## Boards

ESP8266 NodeMCU-V1

![8266](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Nodemcu_amica_bot_02.png/1920px-Nodemcu_amica_bot_02.png)

Witty Cloud

![wiC](https://www.diy-shop.ch/187-large_default/witty-cloud-esp8266-smart-wifi-modul.jpg)

nucleo 32 l432kc

![nucleo](https://media.rs-online.com/t_large/F1438574-01.jpg)

Ethernet Interface

![w5500](https://icmasteronline.com/wp-content/uploads/2019/12/w5500.jpg)

ESP32 TTGO T-Display

![ttgo](https://imgaz.staticbg.com/thumb/large/oaupload/banggood/images/16/CA/808b45ee-f288-4048-a4a9-b21a5d1c7e13.jpg)

## Sensorik

DHT11

![DHT11](https://www.robotshop.com/media/catalog/product/cache/image/1350x/9df78eab33525d08d6e5fb8d27136e95/d/h/dht11-temperature-humidity-sensor-module.jpg)

DHT22

![DHT22](https://images-na.ssl-images-amazon.com/images/I/516FZ5D8vpL._SX342_.jpg)

Kapazitiver Moisture Sensor

![moist](https://images-na.ssl-images-amazon.com/images/I/61LzLgJxV%2BL._SL1000_.jpg)

MQ-Sensoren

![MQ-135](https://www.roboter-bausatz.de/media/image/49/19/05/Hot-Sale-The-Best-Price-MQ135-MQ-135-Air-Quality-Sensor-Hazardous-Gas-Detection-Module-For-2_600x600.jpg)


LDR

![LDR](https://arduinobuy.com/wp-content/uploads/2019/12/LDR.png)