
#include <DHT.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define MOIST A0
#define DHT_PIN 5




const char* SSID = "WullisPlaceVI";
const char* PSK = "mein_sicheres_passwort";
const char* MQTT_BROKER = "192.168.178.183";
int delayTime = 30000;
long now = 0;
float temp = 0;
float humi = 0;
float moist = 0;

DHT dht(DHT_PIN, DHT11);
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;
int LDR = A0;
int btn = 4;

bool st = false;
void setup() {
	pinMode(MOIST, INPUT);
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, LOW);

	dht.begin();
	Serial.begin(115200);
	setup_wifi();
	client.setServer(MQTT_BROKER, 1883);

	
	digitalWrite(LED_BUILTIN, HIGH);
}

void setup_wifi() {
	delay(10);
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(SSID);

	WiFi.begin(SSID, PSK);

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());
	
}

void reconnect() {
	digitalWrite(LED_BUILTIN, LOW);
	while (!client.connected()) {
		Serial.print("Reconnecting...");
		if (!client.connect("PlantMonitoringNode")) {
			Serial.print("failed, rc=");
			Serial.print(client.state());
			Serial.println(" retrying in 5 seconds");
			delay(5000);
		}
	}
	digitalWrite(LED_BUILTIN, HIGH);
	
}

void getData() {
	temp= dht.readTemperature();
	humi=dht.readHumidity();
	moist = 100-((analogRead(MOIST)*100)/1023);

}
void loop() {

	if (!client.connected()) {
		reconnect();
	}
	client.loop();

	if (millis() > now + delayTime) {
		getData();
		snprintf(msg, 50, "Alive since %ld milliseconds", millis());
		Serial.print("Publish message: ");
		Serial.println(msg);
		String payload = "{\"id\":10,\"temp\":";
		payload += String(temp);
		payload += ",\"humi\":";
		payload += String(humi);
		payload += ",\"moist\":";
		payload += String(moist);
		payload += "}";
		Serial.println(payload);
		byte value = analogRead(LDR);
		client.publish("monitoring/data/env", (char*)payload.c_str());

		now = millis();
	}
	
}
