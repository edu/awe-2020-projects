#include "mbed.h"
#include "WIZnetInterface.h"
#include "MQTTPacket.h"
#include "Adafruit_SSD1306.h"

//#define USE_DHCP
char payload_msg[100];

#ifndef USE_DHCP
const char *IP_Addr = "192.168.178.35";
const char *IP_Subnet = "255.255.255.0";
const char *IP_Gateway = "192.168.178.1"; // put your own values here
#endif

float lightDensity=0;
float co2=0;

char ldbuf[8];
char gasbuf[8];
AnalogIn   lightd(A0);
AnalogIn   gas(A1);

SPI spi(SPI_MOSI, SPI_MISO, SPI_SCK);    // MOSI, MISO, SCLK
WIZnetInterface eth(&spi, SPI_CS, PA_8); // spi, CS, RESET

class I2CPreInit : public I2C
{
public:
    I2CPreInit(PinName sda, PinName scl) : I2C(sda, scl)
    {
        frequency(400000);
        start();
    };
};
I2CPreInit gI2C(PB_7,PB_6);
Adafruit_SSD1306_I2c gOled2(gI2C,PA_7);

int publish(char * message,int msglen)
{
    MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
    int rc = 0;
    char buf[200];
    int buflen = sizeof(buf);
    TCPSocketConnection mysock;
    MQTTString topicString = MQTTString_initializer;

    //unsigned char payload[] = "{[m alive!]}";
    
    //int payloadlen = 12; //strlen(payload);
    int len = 0;
    unsigned char m_Test[20];
    strcpy((char *)m_Test, "Hello World");

    mysock.connect("192.168.178.183", 1883);

    data.clientID.cstring = "mbed test client";
    data.keepAliveInterval = 20;
    data.cleansession = 1;
    data.MQTTVersion = 4;

    unsigned char * payload = (unsigned char*) message;
    int payloadlen = strlen(message);

    unsigned char *bufc = (unsigned char *)buf;
    len = MQTTSerialize_connect(bufc, buflen, &data);

    topicString.cstring = "monitoring/data/env";
    len += MQTTSerialize_publish(bufc + len, buflen - len, 0, 0, 0, 0, topicString, payload, payloadlen);

    len += MQTTSerialize_disconnect(bufc + len, buflen - len);
    //printf("Len:%d\nPayload:%s\nPayload %s\n", len, payload, payload);
    rc = 0;
    while (rc < len)
    {
        int rc1 = mysock.send(buf, len);
        if (rc1 == -1)
        {
            printf("Send failed\n");
            break;
        }
        else
            rc += rc1;
    }
    if (rc == len){
        printf("Send succeeded\n");
    ThisThread::sleep_for(200);
    return 1;
    }else return 0;
    
}

int connect(){
uint8_t mac[6];
    mac[0] = 0x90;
    mac[1] = 0xa2;
    mac[2] = 0xda;
    mac[3] = 0x0f;
    mac[4] = 0x0e;
    mac[5] = 0x61; // 90:a2:da:0f:0e:63

    #ifdef USE_DHCP
    int ret = eth.init(mac);
    #else
    int ret = eth.init(mac, IP_Addr, IP_Subnet, IP_Gateway);
    #endif
    printf("return value of init is %d\n", ret);
    printf("MAC=%s \n", eth.getMACAddress());

    ret = eth.connect();
    printf("IP: %s, MASK: %s, GW: %s \n",
           eth.getIPAddress(),
           eth.getNetworkMask(),
           eth.getGateway());

    printf("connect err code is %d \n", ret);
    return ret;
}

void readValues(){
    payload_msg[0] ='\0';
    lightDensity = lightd.read()*100;
    co2 = gas.read()*100;
    int ret = snprintf(ldbuf, 8, "%f",lightDensity);
    ret = snprintf(gasbuf, 8, "%f",co2);
    char *prefix = "{\"id\":15,\"lightdensity\":";
    
    strcat(payload_msg, prefix);
    printf("payload is now: %s\nsize is %d\n",payload_msg,sizeof(payload_msg));
    strcat(payload_msg, ldbuf);
    char * seperator = ",\"gasconcentration\":";
    strcat(payload_msg, seperator);
    strcat(payload_msg,gasbuf);
    char * ending = "}";
    strcat(payload_msg, ending);
    printf("payload is now: %s\nsize is %d\n",payload_msg,sizeof(payload_msg));

    

}

// main() runs in its own thread in the OS
int main()
{
    bool connected = false;
    if(connect()>=0){
        connected=true;
    }

     gOled2.clearDisplay();
     gOled2.setTextCursor(0, 0);
    gOled2.printf("%ux%u OLED Display\r\n", gOled2.width(), gOled2.height());
    gOled2.display();
     printf("Setup..\n");
    
    while (true) {
        printf("hello\n");
        if(!connected){
            if(connect()>=0)
            connected = true;
        }
        gOled2.clearDisplay();
        gOled2.setTextCursor(0, 0);
        //gOled2.printf("%ux%u OLED Display\r\n", gOled2.width(), gOled2.height());
        gOled2.printf("IP is %s\n", eth.getIPAddress());

       
        printf("IP=%s\n",eth.getIPAddress());
        readValues();
        printf("a1 = %f , a2 = %f\n",lightDensity,co2);
        gOled2.printf("LDR = %f\nCo2 = %f",lightDensity,co2);

         gOled2.display();
         publish(payload_msg,strlen(payload_msg));
        ThisThread::sleep_for(30000);

    }
}

