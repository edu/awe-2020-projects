#include <TFT_eSPI.h>
#include <SPI.h>
#include "WiFi.h"
#include <Wire.h>
#include <Button2.h>
#include "esp_adc_cal.h"
#include "bmp.h"
#include <PubSubClient.h>
#include <DHTesp.h>

#ifndef TFT_DISPOFF
#define TFT_DISPOFF 0x28
#endif

#ifndef TFT_SLPIN
#define TFT_SLPIN   0x10
#endif

#define TFT_MOSI            19
#define TFT_SCLK            18
#define TFT_CS              5
#define TFT_DC              16
#define TFT_RST             23

#define TFT_BL          4  // Display backlight control pin
#define ADC_EN          14
#define ADC_PIN         34
#define BUTTON_1        35
#define BUTTON_2        0

#define ADC_PIN_SOIL    33
#define ADC_PIN_LUFT    32

TFT_eSPI tft = TFT_eSPI(135, 240); // Invoke custom library
Button2 btn1(BUTTON_1);
Button2 btn2(BUTTON_2);
WiFiClient espClient;
PubSubClient client(espClient);
DHTesp dht;

char buff[512];
char ssid[] = "ASUS_AP";
char pass[] = "244466666";
int vref = 1100;
int btnCick = false;
int status = WL_IDLE_STATUS;
const char* mqtt_server = "192.168.2.96";
long lastMsg = 0;
char msg[50];
int value = 0;
int dhtPin = 2;


//! Long time delay, it is recommended to use shallow sleep, which can effectively reduce the current consumption
void espDelay(int ms)
{   
    esp_sleep_enable_timer_wakeup(ms * 1000);
    esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH,ESP_PD_OPTION_ON);
    esp_light_sleep_start();
}


void button_init()
{
    btn1.setPressedHandler([](Button2 & b) {
        Serial.println("Detect Voltage..");
        btnCick = true;
    });

    btn2.setPressedHandler([](Button2 & b) {
        btnCick = false;
        Serial.println("btn press wifi scan");
        wifi_scan();
    });
}

void button_loop()
{
    btn1.loop();
    btn2.loop();
}

void wifi_scan()
{
    tft.setTextColor(TFT_GREEN, TFT_BLACK);
    tft.fillScreen(TFT_BLACK);
    tft.setTextDatum(MC_DATUM);
    tft.setTextSize(1);

    tft.drawString("Scan Network", tft.width() / 2, tft.height() / 2);

    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    delay(100);

    int16_t n = WiFi.scanNetworks();
    tft.fillScreen(TFT_BLACK);
    if (n == 0) {
        tft.drawString("no networks found", tft.width() / 2, tft.height() / 2);
    } else {
        tft.setTextDatum(TL_DATUM);
        tft.setCursor(0, 0);
        Serial.printf("Found %d net\n", n);
        for (int i = 0; i < n; ++i) {
            sprintf(buff,
                    "[%d]:%s(%d)",
                    i + 1,
                    WiFi.SSID(i).c_str(),
                    WiFi.RSSI(i));
            tft.println(buff);
        }
    }

    wifi_connection();
}

void wifi_connection(){
    WiFi.enableSTA(true);
    WiFi.begin(ssid, pass);

    while (WiFi.status() != WL_CONNECTED){
      Serial.print("Attempting to connect to WPA SSID: ");
      Serial.println(ssid);
      status = WiFi.begin(ssid, pass);
      delay(3000);
      Serial.println(status);
    }

    IPAddress ip = WiFi.localIP();

    String wificonnected = "TTGO ist mit "+String(ssid)+" connected: IP " + String(ip);
    Serial.println(wificonnected);

    tft.drawString(wificonnected, 0, tft.height() / 10*9);
    
}

void setup()
{
    Serial.begin(115200);
    Serial.println("Start");
    tft.init();
    tft.setRotation(1);
    tft.fillScreen(TFT_BLACK);
    tft.setTextSize(2);
    tft.setTextColor(TFT_WHITE);
    tft.setCursor(0, 0);
    tft.setTextDatum(MC_DATUM);
    tft.setTextSize(1);

    if (TFT_BL > 0) { // TFT_BL has been set in the TFT_eSPI library in the User Setup file TTGO_T_Display.h
         pinMode(TFT_BL, OUTPUT); // Set backlight pin to output mode
         digitalWrite(TFT_BL, TFT_BACKLIGHT_ON); // Turn backlight on. TFT_BACKLIGHT_ON has been set in the TFT_eSPI library in the User Setup file TTGO_T_Display.h
    }

    tft.setSwapBytes(true);
    tft.pushImage(0, 0,  240, 135, ttgo);
    dht.setup(dhtPin, DHTesp::DHT22);
    espDelay(5000);

    //tft.setRotation(0);

    wifi_connection();
    client.setServer(mqtt_server, 1883);
    client.setCallback(callback);

    button_init();

    esp_adc_cal_characteristics_t adc_chars;
    esp_adc_cal_value_t val_type = esp_adc_cal_characterize((adc_unit_t)ADC_UNIT_1, (adc_atten_t)ADC1_CHANNEL_6, (adc_bits_width_t)ADC_WIDTH_BIT_12, 1100, &adc_chars);
    //Check type of calibration value used to characterize ADC
    if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF) {
        Serial.printf("eFuse Vref:%u mV", adc_chars.vref);
        vref = adc_chars.vref;
    } else if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP) {
        Serial.printf("Two Point --> coeff_a:%umV coeff_b:%umV\n", adc_chars.coeff_a, adc_chars.coeff_b);
    } else {
        Serial.println("Default Vref: 1100mV");
    }
}


void showVoltage()
{
        uint16_t v = analogRead(ADC_PIN);
        uint16_t s = analogRead(ADC_PIN_SOIL);
        uint16_t l = analogRead(ADC_PIN_LUFT);
        float temperature = dht.getTemperature();
        float humidity = dht.getHumidity();
        float battery_voltage = ((float)v / 4095.0) * 2.0 * 3.3 * (vref / 1000.0);
        float s_p = ((float)s/4095)*100;
        float l_p = ((float)l/4095)*100;
        String voltage = "Voltage :" + String(battery_voltage) + "V";
        String soil = "Trockenheit: " + String(s)+ " in %: " + String(s_p);
        String luft = "Luftverschmutzung: " + String(l)+ " in %: " + String(l_p);
        String temp = "Temperatur: " + String(temperature)+ "°C";
        String humi = "Luftfeuchtigkeit: " + String(humidity) + "%";
        //Serial.println(voltage);
        //Serial.println(soil);
        //Serial.println(luft);
        tft.fillScreen(TFT_BLACK);
        tft.setTextDatum(MC_DATUM);
        tft.drawString(voltage,  tft.width() / 2, tft.height() / 6*5 );
        tft.drawString(soil,  tft.width() / 2, tft.height() / 6*1 );
        tft.drawString(luft,  tft.width() / 2, tft.height() / 6*2 );
        tft.drawString(temp, tft.width()/ 2, tft.height()/6*3);
        tft.drawString(humi, tft.width()/2, tft.height()/6*4);
        String payload = "{\"id\":1,\"temp\":";
        payload += String(temperature);
        payload += ",\"humi\":";
        payload += String(humidity);
        payload += ",\"moist\":";
        payload += String(s_p);
        payload += ",\"gasconcentration\":";
        payload += String(l_p);
        payload += "}";
        Serial.println(payload);
        client.publish("monitoring/data/env", (char*)payload.c_str());
}

void callback(char* topic, byte* message, unsigned int length) {
       Serial.print("Message arrived on topic: ");
       Serial.print(topic);
       Serial.print(". Message: ");
       String messageTemp;

       for (int i = 0; i < length; i++) {
                     Serial.print((char)message[i]);
                     messageTemp += (char)message[i];
       }
       Serial.println();

       if (String(topic) == "esp32/output") {
                     Serial.print("Changing output to ");
                     if (messageTemp == "on") {
                                  Serial.println("on");
                               
                     }
                     else if (messageTemp == "off") {
                                  Serial.println("off");
                                  
                     }
       }
}


void reconnect() {
     // Loop until we're reconnected
     while (!client.connected()) {
           Serial.print("Attempting MQTT connection...");
           // Attempt to connect
           if (client.connect("TTGOClient")) {
                        Serial.println("connected");
                        // Subscribe
                        client.subscribe("esp32/output");
           }
           else {
                        Serial.print("failed, rc=");
                        Serial.print(client.state());
                        Serial.println(" try again in 5 seconds");
                        // Wait 5 seconds before retrying
                        delay(5000);
           }
     }
}




void loop()
{
    if (btnCick) {
       
       if (!client.connected()) {
            reconnect();
       }
       client.loop();
       long now = millis();
       if (now - lastMsg > 30000) {
            lastMsg = now;
            showVoltage();
       }
    }
    button_loop();
}
