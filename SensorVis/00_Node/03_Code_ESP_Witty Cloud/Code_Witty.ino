
#include <DHT.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define LDR A0





const char* SSID = "WullisPlaceVI";
const char* PSK = "espasswort";
const char* MQTT_BROKER = "192.168.178.183";
int delayTime = 30000;
long now = 0;
float ld = 0;


WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;
long rssi = 0;


bool st = false;
void setup() {
	pinMode(LDR, INPUT);
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, LOW);

	
	Serial.begin(115200);
	setup_wifi();
	client.setServer(MQTT_BROKER, 1883);


	digitalWrite(LED_BUILTIN, HIGH);
}

void setup_wifi() {
	delay(10);
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(SSID);

	WiFi.begin(SSID, PSK);

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());

}

void reconnect() {
	digitalWrite(LED_BUILTIN, LOW);
	while (!client.connected()) {
		Serial.print("Reconnecting...");
		if (!client.connect("WittyCloudLDR")) {
			Serial.print("failed, rc=");
			Serial.print(client.state());
			Serial.println(" retrying in 5 seconds");
			delay(5000);
		}
	}
	digitalWrite(LED_BUILTIN, HIGH);

}

void getData() {
	
	ld = ((analogRead(LDR) * 100) / 1023);
	rssi = WiFi.RSSI();
}
void loop() {

	if (!client.connected()) {
		reconnect();
	}
	client.loop();

	if (millis() > now + delayTime) {
		getData();
		snprintf(msg, 50, "Alive since %ld milliseconds", millis());
		Serial.print("Publish message: ");
		Serial.println(msg);
		String payload = "{\"id\":12,\"lightdensity\":";
		payload += String(ld);
		payload += ",\"rssi\":";
		payload += String(rssi);
		payload += "}";
		Serial.println(payload);
		byte value = analogRead(LDR);
		client.publish("monitoring/data/env", (char*)payload.c_str());

		now = millis();
	}

}
