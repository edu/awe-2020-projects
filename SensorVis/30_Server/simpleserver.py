from flask import Flask, request, jsonify #import main Flask class and request object
import datetime
import psycopg2
import json
import dbget



host = '0.0.0.0'
port = 5500


app = Flask(__name__) #create the Flask app



@app.route('/getsystemdata',methods=['GET','POST'])
def getSysData():
    return dbget.getDBInfo()

@app.route('/getdata',methods=['GET','POST'])
def getData():
    req_data = request.get_json()
    try:
        l = (len(req_data))
        print("start is "+str(req_data["start"]))
        print("end is "+str(req_data["end"]))
        return(dbget.getdatabytime(req_data["start"],req_data["end"]))
    except:
        return(dbget.getdatabytime(0,1588291200))
    return jsonify(data="todo..")


@app.route('/newconfig',methods=['GET','POST'])
def addConfig():
    return jsonify(data="todo..")

@app.route('/geterr',methods=['GET'])
def sendErrs():
    return dbget.geterrs()

@app.route('/getlast')
def getlasrec():
    return dbget.getlastrec()



@app.route('/test')
def testConn():
    try:
        min_time = dbget.dt2ts(dbget.get_min_time())
        return jsonify(connection=True, min_time=min_time)
    except:
        return jsonify(connection=True)

@app.route('/json-example',methods=['GET', 'POST'])
def jsonexample():
    yourtime = 0
    state = "parsing error"
    if request.method == 'POST':
        req_data = request.get_json()
        try:
            print("type: ")
            print(type(req_data.get("timestamp")[0]))
            print(req_data.get("timestamp")[0])
            print(datetime.date.fromtimestamp(req_data.get("timestamp")[0]))
            yourtime = datetime.datetime.fromtimestamp(req_data.get("timestamp")[0])
            state = "STATE_OK"
        except:
            print("parsing error!")

    return jsonify(
        state=state,
        answertime=datetime.datetime.now(),
        yourtime=yourtime
    )



if __name__ == '__main__':
    app.run(debug=True, port=port,host=host) #run app in debug mode on port 5000
