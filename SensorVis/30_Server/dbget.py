import datetime
import psycopg2
import json
from flask import jsonify
import time
import pytz
utc = pytz.UTC
localtz = pytz.timezone('Europe/Zurich')

conn = psycopg2.connect(
    'host=192.168.178.183 user=internaluser password=aweProjekt dbname=awe')
cur = conn.cursor()
min_time = datetime.datetime(
    2020, 4, 15, 14, 8, 25, 650971, tzinfo=datetime.timezone.utc)
time_now = datetime.datetime.now()


def get_min_time():
    query = "select min(time) from envdata"
    cur.execute(query)
    tm = cur.fetchall()[0][0]
    return(tm)


def dt2ts(dtobj):
    tts = time.mktime(dtobj.timetuple())
    return int(tts)


def getdatabytime(starting, ending):
    if (starting < dt2ts(min_time)):
        starting = min_time
    if(ending > dt2ts(time_now)):
        ending = dt2ts(datetime.datetime.now())
    starting = getTimeStamp(starting)
    ending = getTimeStamp(ending)
    td = (ending-starting)/100
    interval_min = int(td.seconds/60)

    print("getting Data from:"+starting.strftime("%d.%m. %H:%M:%S") +
          " to "+ending.strftime("%d. %m. %H:%M:%S"))
    query_b = """
    SELECT time_bucket_gapfill('%s min'::interval, time) as buck,
    locf(avg(temperature)) AS temperature,
    locf(avg(humidity)) AS humidity,
    
    locf(avg(moisture)) AS moisture,
    locf(avg(light)) AS light,
    locf(avg(gasconcentration)) AS gasconcentration
    FROM envdata
    WHERE
    (time > %s) and (time < %s) and (node_id != 12)
    GROUP BY buck;
    """
    args = (interval_min, starting, ending,)
    cur.execute(query_b, args)
    dbdata = cur.fetchall()
    l = len(dbdata)
    if dbdata == 0:
        return None
    data = []
    i = 0
    lc = len(dbdata[0])
    while(i < lc):
        content = []
        data.append(content)
        i += 1
    print(str(l)+" content: "+str(lc))
    i = 1
    while(i < l):
        tm = dbdata[i][0]
        data[0].append(dt2ts(tm))
        j = 1
        while(j < lc):
            data[j].append(dbdata[i][j])
            j += 1
        i += 1

    datadict = {"time": data[0],
                "temperature": data[1],
                "humidity": data[2],
                "moisture": data[3],
                "light": data[4],
                "gasconcentration": data[5]}
    datajson = json.dumps(datadict)
    f=open("last.json","w")
    f.write(datajson)
    f.close()

    return datajson


def getTimeStamp(unixtime):
    try:
        ts = datetime.datetime.fromtimestamp(unixtime)
        ts = localtz.localize(ts)
        return ts
    except:
        return min_time


def get_columns_names(table):
    col_names_str = "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE "
    col_names_str += "table_name = '{}';".format(table)
    try:
        cur.execute(col_names_str)
        dbdata = cur.fetchall()

        colnames = []
        try:
            for item in dbdata:
                colnames.append(item[0])
            return colnames
        except:
            print("error")
            return None
    except:
        print("error...")
        return None



def getDBInfo():
    query = """
    select 
    node.node_id as id, 
    node.last_update,
    count(envdata.*) as quantity,
    location_name as loc 
    from envdata
    inner join node on envdata.node_id = node.node_id 
    inner join location on location.location_id = node.location_id
    group by node.node_id, location.location_id
    """
    cur.execute(query)
    info = cur.fetchall()
    data = []
    errdata = []
    for node in info:
        nodeinfo = {"id": node[0],
                    "last_active": dt2ts(node[1]),
                    "available": node[2],
                    "location": node[3]}
        data.append(nodeinfo)
    query = "select * from err"
    cur.execute(query)
    info = cur.fetchall()
    for err in info:
        errinfo = {"type": err[1],
                   "content": err[0],
                   "time": dt2ts(err[2])}
        errdata.append(errinfo)
    datadict = {"nodes": data, "available": get_columns_names("envdata"), "min_time": dt2ts(get_min_time()),
                "alerts": errdata}
    datajson = json.dumps(datadict)

    return datajson


min_time = get_min_time()


def geterrs():
    query = "SELECT * FROM err order by time desc"
    cur.execute(query)
    errs = (cur.fetchall())
    warnings = []
    errors = []
    for item in errs:
        time_when = item[2].strftime("%d.%m.%Y %H:%M:%S")
        entry = {"type":item[1],"descr":item[0],"time":time_when}
        if(item[1]=="error"):
            errors.append(entry)
        if(item[1]=="warning"):
            warnings.append(entry)
    errdata = {"errors":errors,"warnings":warnings}
    return errdata

def getlastrec():
    colnames = ["node_id","temperature","humidity","moisture","light","gasconcentration"]
    query = "SELECT * FROM envdata order by time desc limit 5"
    cur.execute(query)
    errs = (cur.fetchall())
    lrec = []
    for item in errs:
        #time_when = (item[6]+datetime.timedelta(hours=1)).strftime("%d.%m.%Y %H:%M:%S")
        time_when = (item[6]+datetime.timedelta(hours=1)).strftime("%H:%M:%S")
        entry ={}
        entry.update({"time":time_when})
        entry.update({"node_id":item[0]})



        index = 1
        while(index<6):
            if(item[index]):
                entry.update({colnames[index]:round(item[index],2)})
            index+=1

        lrec.append(entry)
    return jsonify(lrec)