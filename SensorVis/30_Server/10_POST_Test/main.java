import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.Certificate;
import java.sql.Timestamp;

import javax.net.ssl.HttpsURLConnection;
import java.util.Date;
import org.json.*;

public class main {
	
	static int post (JSONObject postData)throws Exception {
		String body = postData.toString();
		try {
			URL srv = new URL("http://localhost:5000/data");
			HttpURLConnection connection = ( HttpURLConnection)srv.openConnection(); 
			connection.setRequestMethod( "POST" );
			connection.setDoInput( true );
			connection.setDoOutput( true );
			connection.setUseCaches( false );
			connection.setRequestProperty( "Content-Type",
			                               "application/json" );
			connection.setRequestProperty( "Content-Length", String.valueOf(body.length()) );
			OutputStreamWriter writer = new OutputStreamWriter( connection.getOutputStream() );
			writer.write( body );
			writer.flush();

			BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()) );
			for ( String line; (line = reader.readLine()) != null; )
			{
			  System.out.println( line );
			}

			writer.close();
			reader.close();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
		return 0;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Date date = new Date();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println(timestamp.getTime());
        //System.out.println(new Timestamp(date.getTime()));
		try{
			JSONObject data = new JSONObject();
			data.append("Succeeded", true);
			data.append("currenttime",date);
			data.append("starttime",timestamp.getTime()/1000-3600-300);
			data.append("endtime",timestamp.getTime()/1000);
			JSONArray params = new JSONArray();
			params.put("humi");
			params.put("temp");
			params.put("moist");
			data.append("params", params);
			post(data);
		}catch (Exception e) {
			System.err.print(e);
		}
	}

}
