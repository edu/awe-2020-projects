import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go

locations =[]
timeo = [47.372938,8.043970,"Timeo's Home"]
barbara = [47.497396, 8.158408,"Barbara's Home"]
fhnw = [47.480455, 8.211577,"FHNW, Gebäude 4"]
locations.append(timeo)
locations.append(barbara)
locations.append(fhnw)
mw = [0,0,"center"]
for item in locations:
    mw[0]+=item[0]
    mw[1]+=item[1]

mw[0]/=len(locations)
mw[1]/=len(locations)
locations.append(mw)



app = dash.Dash()
mapbox_access_token = open(".mapbox_token").read()
fig = go.Figure(go.Scattermapbox(
        lat=[str(locations[0][0])],
        lon=[str(locations[0][1])],
        mode='markers',
        marker=go.scattermapbox.Marker(
            size=21
        ),
        text=[str(locations[0][2])],
        showlegend=False,
        

    )
    )

i = 1
while(i<len(locations)-1):
    fig.add_trace(go.Scattermapbox(
    lat=[str(locations[i][0])],
        lon=[str(locations[i][1])],
        mode='markers',
        marker=go.scattermapbox.Marker(
            size=19
            #,color='red'
        ),
        text=[(locations[i][2])],
        showlegend=False,
        
    

    ))
    i+=1

"""
fig.add_trace(go.Scattermapbox(
    lat=['47.497396'],
        lon=['8.158408'],
        mode='markers',
        marker=go.scattermapbox.Marker(
            size=15
        ),
        text=['Barbara Home'],
    

))
"""
fig.update_layout(
    title_text="Locations",
    hovermode='closest',
    mapbox=dict(
        accesstoken=mapbox_access_token,
        bearing=0,
        
        center=go.layout.mapbox.Center(
            lat=locations[len(locations)-1][0],
            lon=locations[len(locations)-1][1]
        ),
        
        pitch=0,
       
        zoom=10
    ),
    height=600
)
fig.update_layout(mapbox_style="stamen-terrain")

app.layout = html.Div([


dcc.Graph(
        id='life-exp-vs-gdp',
        figure=fig)])


if __name__ == '__main__':
    app.run_server(debug=False,host='0.0.0.0')
