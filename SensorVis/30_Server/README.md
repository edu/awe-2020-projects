# Server



## HTTP Requests

### Allgemeine Informationen (first Connect)

Get Request an URL: `192.168.178.183:5500/getsystemdata`

>Todo..

Gibt zurück:

```json
{
    "nodes":[
        {"id":10,
        "name":"ENV-10",
        "location":"Timeo Home",
        "available":1240},
        {"id":20,
        "name":"ENV-20",
        "location":"Barbara Home",
        "available":140},
        {"id":21,
        "name":"ENV-21",
        "location":"Barbara Home",
        "available":0}
    ],
    "alerts":[{
        "type":"error",
        "node_id":21,
        "location":"Barbara Home",
        "description":"could not parse json",
        "time":1586880525 
    }]
}
```


### Spezifische Daten aus Datenbank abfragen:

Post Request mit folgender JSON Payload:
```json
{
    "starttime":1586878269,
    "endtime":1586888269,
    "data": "envdata" 
}
```
URL: `192.168.178.183:5500/getdata`

> Bei Error wird folgender Content als Payload geschickt:
> ```json
> {"state":"ERROR"}
> ```


### Configfiles an Datenbank schicken
Post Request mit folgendem Content an den Server schicken:
URL: `192.168.178.183:5500/newconfig`

```json
{
    "addtodb":false,
    "newconfig":{
        "id":232,
        "type":345,
        "dhcp":false,
        "ip":"192.186.1.1",
        "defgw":"192.186.1.1",
        "brokerip":"192.186.1.2",
        "mqttport":1883,
        "sendinterval":30
    }
}
```



