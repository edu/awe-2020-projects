package com.example.jump2;

import android.graphics.Canvas;
import android.graphics.Paint;



public class Ball {


    //Fields
    public float x; //Position X-Achse
    public float y; //Position Y-Achse
    public float radius; // Radius des Ball-Objekts
    private Paint color = new Paint();


    //Konstruktor
    public Ball(float x,float y, float radius, int ballcolor) {
        this.x =x;
        this.y =y;
        this.radius = radius;
        color.setColor(ballcolor);

    }
    //Zeichnet einen Ball
    public void drawBall(Canvas canvas)
    {
        canvas.drawCircle(x, y, radius, color);
    }
    //Bewegt den Ball in X-Richtung
    public void moveX (float speed){

        x = x - speed;
    }
    // Lässt den Ball springen
    public void jump(int accel){

        y = y - accel;
      }
     // Detektiert Kollisionen mit einem zweiten Ball-Objekt
    public boolean colliding(Ball ball ){
        double abstand= Math.hypot(this.x-ball.x,this.y-ball.y);
        return abstand < (2*radius);
    }


}
