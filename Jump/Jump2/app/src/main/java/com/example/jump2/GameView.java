package com.example.jump2;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.view.View;
import android.view.MotionEvent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Color;
import android.graphics.Bitmap;
import java.util.Timer;
import java.util.TimerTask;
public class GameView extends View
{


    //Bälle
    private Ball spielerBall;
    private Ball gegnerBall;
    private float radius = 0;

    //Spielvariablen
    private Timer timer;
    private float tau=0f;
    private int score =0;
    private int highscore=0;
    private boolean gameover = false;
    private boolean gameinit = true;
    private boolean gamerunning = false;

    //Grafikelemente und Farben
    private static final int green = Color.GREEN;
    private static final int white = Color.WHITE;
    private static final int red = Color.RED;
    private static final int black = Color.BLACK;
    private static final int gray = Color.GRAY;
    private Paint scorepaint = new Paint();
    private Paint gameoverpaint = new Paint();
    private Paint grasspaint = new Paint();
    private Paint opacpaint = new Paint();
    private Paint gamestartpaint = new Paint();
    private Paint instructionpaint = new Paint();
    private Bitmap background;
    final private RectF region=new RectF();
    private int x0,y0,x1,y1; //Eckpunkte des Bildschirms

    //Konstruktor
    GameView(Context context)
    {
        super(context);
        // textfonts
        scorepaint.setColor(white);
        gameoverpaint.setColor(red);
        grasspaint.setColor(green);
        opacpaint.setColor(white);
        opacpaint.setAlpha(190);
        gamestartpaint.setColor(black);
        instructionpaint.setColor(gray);
        //Erzeugung Hintergrund
        background= BitmapFactory.decodeResource (context.getResources() ,R.drawable.hintergrund);

        //Erzeugung Spieler und Gegnerball
        gegnerBall = new Ball(10000,10000, radius, Color.RED);
        spielerBall = new Ball(0,0,radius, Color.BLUE);

        //create new timer for the game
        timer = new Timer();
        timer.scheduleAtFixedRate(new TickTask(), 0, 20);


    }
    //Passt Variablen an die Bildschirmgrösse an
    public void onLayout(boolean changed,
                         int x0,int y0,
                         int x1,int y1)
    {
        // Vier Variablen geben die Abmessung des Bildschirms an
        this.x0=x0;
        this.y0=y0;
        this.x1=x1;
        this.y1=y1;
        region.left=0;
        region.top=0;
        region.right=x1-x0;
        region.bottom=y1-y0;

        // passt fonts und spielbälle an Bildschirmgrösse an
        gamestartpaint.setTextSize((x1-x0)/12);
        grasspaint.setStrokeWidth((int) ((y1-y0)*0.2));
        radius = (x1-x0)/35;
        spielerBall.radius = radius;
        gegnerBall.radius = radius;
        spielerBall.x= (float)(x1-x0)*0.25f;
        spielerBall.y= (float) (y1-y0)*0.8f-radius;
        gegnerBall.y= (float) (y1-y0)*0.8f-radius;
        gameoverpaint.setTextSize((x1-x0)*0.15f);
        scorepaint.setTextSize((y1-y0)*0.09f);
        instructionpaint.setTextSize((y1-y0)*0.05f);

    }

    // Zeitgesteuerte Klasse, die durch das Spiel führt
    class TickTask extends TimerTask{

        private float speed=8f;

        public void run(){
            //Ball bewegt, solange Spiel läuft
            if(gamerunning) {
                gegnerBall.moveX(speed);
            }

            // Spielerball kolidiert mit gegnerBall
                if (spielerBall.colliding(gegnerBall)) {
                    if(score>=highscore){
                        highscore= score;
                    }
                    gameover = true;
                    gamerunning = false;
                    speed=8f; //Rücksetzen Geschwindigkeit
                    tau=0f; // Rücksetzen tau

                }
                //Steuert Geschwindigkeit Gegnerball
                if (gegnerBall.x <= -10) {
                    score++;
                    gegnerBall.x = x1-x0+100;
                    speed =  40-30*(float) Math.pow(5.0, -0.1*tau);
                    tau= tau+0.7f;

                }
                 //Bewegt den Spielerball wieder nach unten
                if (spielerBall.y < (y1-y0)*0.8-radius) {
                    spielerBall.y = spielerBall.y + (x1-x0)/160;
                }

          postInvalidate(); // Zeichnet die View neu

        }
    }

    // Zeichnet auf dem Bildschirm
    protected void onDraw(Canvas canvas)
    {

        //Hintergrund aus bitmap setzen
        canvas.drawBitmap(background,null, region, null);
        //Gras zeichnen
        canvas.drawLine(0, (int) ((y1-y0)*0.9), x1-x0, (int) ((y1-y0)*0.9), grasspaint);

        //Baelle zeichnen
        spielerBall.drawBall(canvas);
        gegnerBall.drawBall(canvas);


        // Startbildschirm beim öffnen der App
        if(gameinit){
            canvas.drawText("Touch here to dare it", (int) ((x1-x0)*0.1), (int) ((y1-y0)*0.2), gamestartpaint);
            canvas.drawRect(0, 0, x1-x0, y1-y0, opacpaint);
        }

        //Bildschirm während des Spielens
        if(gamerunning) {
            canvas.drawText("Score: " + score, (int) ((x1-x0)*0.03), (int) ((y1-y0)*0.40), scorepaint);
            canvas.drawText("Highscore: "+highscore, (int) ((x1-x0)*0.03), (int) ((y1-y0)*0.5), scorepaint);
            canvas.drawText("Touch to jump", (int)((x1-x0)*0.25), (int) ((y1-y0)*0.9), instructionpaint);
        }
        //Bildschirm nach Niederlage
        if(gameover){
            canvas.drawText("Touch here to retry", (int) ((x1-x0)*0.15), (int) ((y1-y0)*0.2), gamestartpaint);
            canvas.drawRect(region, opacpaint);
            canvas.drawText("Game over!", (int) ((x1-x0)*0.10), (int) ((y1-y0)*0.5), gameoverpaint);
            canvas.drawText("Score: "+score, (int) ((x1-x0)*0.35), (int) ((y1-y0)*0.6), gamestartpaint);
        }
    }

    // callback für Bildschirmberührung
    public boolean onTouchEvent(MotionEvent event)
    {

        //Berühren um zu Starten, setzt verschiedene Variablen zurück
        if((gameover || gameinit)&& (event.getY()<(int) ((y1-y0)*0.3))){
            gameinit = false;
            gameover = false;
            gamerunning =true;
            score =0;
            gegnerBall.x = (x1-x0)+100;

        }
        // Berühren um mit Spielerball zu springen
        if((spielerBall.y>=((y1-y0)*0.8-radius))&gamerunning){
            spielerBall.jump((x1-x0)/6);
        }
        return true;
    }
}

