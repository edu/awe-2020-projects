import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:easy_cocktail/ui/widgets/adding_cocktail.dart';
import 'package:easy_cocktail/model/state.dart';
import 'package:easy_cocktail/utils/auth.dart';

final CollectionReference collectionReference = Firestore.instance.collection('Cocktail_rezepte');

class StateWidget extends StatefulWidget {
  final StateModel state;
  final Widget child;
  

  StateWidget({
    @required this.child,
    this.state,
  });

  // Returns data of the nearest widget _StateDataWidget
  // in the widget tree.
  static _StateWidgetState of(BuildContext context) {
    return (context.dependOnInheritedWidgetOfExactType<_StateDataWidget>().data);
  }

  @override
  _StateWidgetState createState() => new _StateWidgetState();
}

class _StateWidgetState extends State<StateWidget> {
  StateModel state;
  GoogleSignInAccount googleAccount;
  final GoogleSignIn googleSignIn = new GoogleSignIn();

  @override
  void initState() {
    super.initState();
    if (widget.state != null) {
      state = widget.state;
    } else {
      state = new StateModel(isLoading: true);
      initUser();
    }
  }

  Future<Null> initUser() async {
    googleAccount = await getSignedInAccount(googleSignIn);

    if (googleAccount == null) {
      setState(() {
        state.isLoading = false;
      });
    } else {
      await signInWithGoogle();
    }
  }

    Future<List<String>> getFavorites() async {
    DocumentSnapshot querySnapshot = await Firestore.instance
        .collection('users')
        .document(state.user.uid)
        .get();
    if (querySnapshot.exists &&
        querySnapshot.data.containsKey('favorites') &&
        querySnapshot.data['favorites'] is List) {
      // Create a new List<String> from List<dynamic>
      return List<String>.from(querySnapshot.data['favorites']);
    }
    return [];
  }

  Future<Null> signInWithGoogle() async {
    if (googleAccount == null) {
      // Start the sign-in process:
      googleAccount = await googleSignIn.signIn();
    }
    FirebaseUser firebaseUser = await signIntoFirebase();
    state.user = firebaseUser;
    List<String> favorites = await getFavorites();
    setState(() {
      state.isLoading = false;
      state.favorites = favorites;;
    });
  }

  
  Future<Null> signOutOfGoogle() async {
    // Sign out from Firebase and Google
    await FirebaseAuth.instance.signOut();
    await googleSignIn.signOut();
    // Clear variables
    googleAccount = null;
    state.user = null;
    setState(() {
      state = StateModel(user: null);
    });
  }

    Future<Cocktailadd> createCocktail(String cocktailname, List<String> zutaten,List<String> zubereitung,List<String> decor,String schwierigkeit,String imageURL, String typ) async {
      final TransactionHandler createTransaction = (Transaction tx) async {
      final DocumentSnapshot ds = await tx.get(collectionReference.document());

      final Cocktailadd cocktail = new Cocktailadd(cocktailname,zutaten,zubereitung,decor,schwierigkeit,imageURL,typ);
      final Map<String, dynamic> data = cocktail.toMap();
      await tx.set(ds.reference, data);
      return data;
    };
    
    return Firestore.instance.runTransaction(createTransaction).then((mapData) {
      return Cocktailadd.fromMap(mapData);
    }).catchError((error) {
      print('error: $error');
      return null;
    });
  }


  @override
  Widget build(BuildContext context) {
    return new _StateDataWidget(
      data: this,
      child: widget.child,
    );
  }
}

class _StateDataWidget extends InheritedWidget {
  final _StateWidgetState data;

  _StateDataWidget({
    Key key,
    @required Widget child,
    @required this.data,
  }) : super(key: key, child: child);
  
  // Rebuild the widgets that inherit from this widget
  // on every rebuild of _StateDataWidget: 
  @override
  bool updateShouldNotify(_StateDataWidget old) => true;
}

