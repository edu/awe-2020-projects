class Cocktailadd{
  String _cocktailname;
  List<String> _zutaten;
  List<String> _zubereitung;
  List<String> _decor;
  String _schwierigkeit;
  String _imageURL;
  String _typ;

  Cocktailadd(this._cocktailname,this._zutaten,this._zubereitung,this._decor,this._schwierigkeit,this._imageURL,this._typ);

  Cocktailadd.map(dynamic obj){
    this._cocktailname = obj['Name'];
    this._zutaten = obj['Zutaten'];
    this._zubereitung = obj['Zubereitung'];
    this._decor = obj['Decor'];
    this._schwierigkeit = obj['Schwierigkeit'];
    this._imageURL = obj['ImageURL'];
    this._typ = obj['Typ'];
  }

  String get  cocktailname => _cocktailname;
  List<String> get zutaten => _zutaten;
  List<String> get zubereitung => _zubereitung;
  List<String> get decor => _decor;
  String get schwierigkeit => _schwierigkeit;
  String get imageURL => _imageURL;
  String get typ => _typ;

  Map<String,dynamic> toMap(){
    var map=new Map<String,dynamic>();
    map['Name']=_cocktailname;
    map['Zutaten'] = _zutaten;
    map['Zubereitung'] = _zubereitung;
    map['Decor'] = _decor;
    map['Schwierigkeit'] = _schwierigkeit;
    map['ImageURL'] = _imageURL;
    map['Typ'] = _typ;
    return map;
  }
  Cocktailadd.fromMap(Map<String,dynamic> map){
    this._cocktailname= map['Name'];
    this._zutaten = map['Zutaten'];
    this._zubereitung = map['Zubereitung'];
    this._decor = map['Decor'];
    this._schwierigkeit = map['Schwierigkeit'];
    this._imageURL = map['Image'];
    this._typ = map['Typ'];
  }

}