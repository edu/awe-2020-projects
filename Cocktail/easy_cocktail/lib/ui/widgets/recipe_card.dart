import 'package:flutter/material.dart';
import 'package:easy_cocktail/model/cocktail_rezepte.dart';
import 'package:easy_cocktail/ui/widgets/Cocktail_image.dart';
import 'package:easy_cocktail/ui/widgets/Cocktail_title.dart';
import 'package:easy_cocktail/ui/screens/detail.dart';

class RecipeCard extends StatelessWidget {
  final Rezepte recipe;
  final bool inFavorites;
  final Function onFavoriteButtonPressed;
 

  RecipeCard(
      {@required this.recipe,
      @required this.inFavorites,
      @required this.onFavoriteButtonPressed});

  @override
  Widget build(BuildContext context) {
    RawMaterialButton _buildFavoriteButton() {
      return RawMaterialButton(
        constraints: const BoxConstraints(minWidth: 40.0, minHeight: 40.0),
        onPressed: () => onFavoriteButtonPressed(recipe.id),
        child: Icon(
 
          inFavorites == true ? Icons.favorite : Icons.favorite_border,
          color: Theme.of(context).iconTheme.color,
        ),
        elevation: 2.0,
        fillColor: Theme.of(context).buttonColor,
        shape: CircleBorder(),
      );
    }

    return GestureDetector(
            onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => new DetailScreen(recipe, inFavorites),
            ),
          ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
        child: Card(
            shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
            ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // We overlap the image and the button by
              // creating a Stack object:
              Stack(
                children: <Widget>[
                  CocktailImage(recipe.imageURL),
                  Positioned(
                    child: _buildFavoriteButton(),
                    top: 2.0,
                    right: 2.0,
                  ),
                ],
              ),
              CocktailTitel(recipe, 15),
            ],
          ),
        
        ),
      ),
    );
  }
}