  
import 'package:flutter/material.dart';

import 'package:easy_cocktail/model/cocktail_rezepte.dart';

class CocktailTitel extends StatelessWidget {
  final Rezepte recipe;
  final double padding;

  CocktailTitel(this.recipe, this.padding);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(padding),
      child: Column(
        // Default value for crossAxisAlignment is CrossAxisAlignment.center.
        // We want to align title and description of recipes left:
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            recipe.cocktailname,
            style: Theme.of(context).textTheme.headline6,
          ),
          // Empty space:
          SizedBox(height: 10.0),
          Row(
            children: [
              Icon(Icons.timer, size: 20.0, color: Colors.black,),
              SizedBox(width: 5.0),
              Text(
                recipe.schwierigkeit,
                style: Theme.of(context).textTheme.caption,
              ),
            ],
          ),
        ],
      ),
    );
  }
}