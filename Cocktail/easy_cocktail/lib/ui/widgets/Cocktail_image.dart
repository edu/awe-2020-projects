import 'package:flutter/material.dart';

class CocktailImage extends StatelessWidget {
  final String imageURL;

  CocktailImage(this.imageURL);

  @override
  Widget build(BuildContext context) {
    return Flexible (
      child:Center(
    child:AspectRatio(
      aspectRatio: 16.0 / 9.0,
      child:ClipRRect(borderRadius: new BorderRadius.circular(12.0),
      child: Image.network(
        imageURL,
        fit: BoxFit.cover,
        loadingBuilder: (context,child,progress){
          return progress == null
          ?child
          :LinearProgressIndicator(
          backgroundColor: Colors.grey,
          valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),                          
          );
        },
      ),
      ),
    ),
      ),
    );
  }
}