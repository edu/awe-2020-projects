import 'package:flutter/material.dart';
import 'package:easy_cocktail/ui/widgets/google_sign_in_button.dart';
import 'package:easy_cocktail/ui/widgets/state_widget.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isVisible = false;
    @override
  Widget build(BuildContext context) {
    BoxDecoration _buildBackground() {
      return BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/splash_picture3.jpg"),
          fit: BoxFit.cover,
        ),
      );
    }
    Text _buildText() {
      return Text(
        'Easy Cocktail',
        style: Theme.of(context).textTheme.headline5,
        textAlign: TextAlign.center,
      );
    }

        return Scaffold(
      body: Container(
        decoration: _buildBackground(),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildText(),
               Visibility(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Color(0xFFB2F2D52)),
                ),
                visible: isVisible,
              ),
              SizedBox(height: 300.0),
              GoogleSignInButton(
                onPressed:() => StateWidget.of(context).signInWithGoogle(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
