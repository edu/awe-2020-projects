import 'package:flutter/material.dart';
import 'package:easy_cocktail/model/cocktail_rezepte.dart';
import 'package:easy_cocktail/utils/store.dart';
import 'package:easy_cocktail/ui/widgets/recipe_card.dart';
import 'package:easy_cocktail/ui/widgets/state_widget.dart';
import 'package:easy_cocktail/ui/screens/login.dart';
import 'package:easy_cocktail/model/state.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_cocktail/ui/widgets/settings_button.dart';


class HomeScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => new HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  StateModel appState;

  // String cocktailname;
  List<String> zutaten = List<String>();
  List<String> zubereitung = List<String>();
  List<String> decor= List<String>();
  // String schwierigkeit;
  // String imageURL;
  // String typ;
  // String zutat1;
  // String zutat2;
  // String zutat3;
  // String zutat4;
  // String zutat5;
  // String zutat6;
  // String step1;
  // String step2;
  // String step3;
  // String step4;
  // String step5;
  // String decor1;
  // String decor2;
  // String decor3;
  final TextEditingController _cocktailname = new TextEditingController();
  final TextEditingController _zutat1 = new TextEditingController();
  final TextEditingController _zutat2 = new TextEditingController();
  final TextEditingController _zutat3 = new TextEditingController();
  final TextEditingController _zutat4 = new TextEditingController();
  final TextEditingController _zutat5 = new TextEditingController();
  final TextEditingController _step1 = new TextEditingController();
  final TextEditingController _step2 = new TextEditingController();
  final TextEditingController _step3 = new TextEditingController();
  final TextEditingController _step4 = new TextEditingController();
  final TextEditingController _step5 = new TextEditingController();
  final TextEditingController _decor1 = new TextEditingController();
  final TextEditingController _decor2 = new TextEditingController();
  final TextEditingController _decor3 = new TextEditingController();
  final TextEditingController _schwierigkeit = new TextEditingController();
  final TextEditingController _imageURL = new TextEditingController();


  // getCocktailName(cocktailname){
  //   this.cocktailname=cocktailname;
  // }
  // getCocktailZutaten(zutaten){
  //   this.zutaten=zutaten;
  // }
  //   getCocktailZutat1(zutat1){
  //   this.zutat1=zutat1;
  // }
  //   getCocktailZutat2(zutat2){
  //   this.zutat2=zutat2;
  // }
  //     getCocktailZutat3(zutat3){
  //   this.zutat3=zutat3;
  // }
  //     getCocktailZutat4(zutat4){
  //   this.zutat4=zutat4;
  // }
  //     getCocktailZutat5(zutat5){
  //   this.zutat5=zutat5;
  // }
  //     getCocktailZutat6(zutat6){
  //   this.zutat6=zutat6;
  // }
  //     getCocktailZubereitung(zubereitung){
  //   this.zubereitung=zubereitung;
  // }
  //     getCocktailStep1(step1){
  //   this.step1=step1;
  // }
  //     getCocktailStep2(step2){
  //   this.step2=step2;
  // }
  //     getCocktailStep3(step3){
  //   this.step3=step3;
  // }
  //     getCocktailStep4(step4){
  //   this.step4=step4;
  // }
  //     getCocktailStep5(step5){
  //   this.step5=step5;
  // }
  //     getCocktailDecor(decor){
  //   this.decor=decor;
  // }
  //      getCocktailDecor1(decor1){
  //   this.decor1=decor1;
  // }
  //      getCocktailDecor2(decor2){
  //   this.decor2=decor2;
  // }
  //      getCocktailDecor3(decor3){
  //   this.decor3=decor3;
  // }
  //     getCocktailSchwierigkeit(schwierigkeit){
  //   this.schwierigkeit=schwierigkeit;
  // }
  //     getCocktailImageURL(imageURL){
  //   this.imageURL=imageURL;
  // }
  
      int cocktailtyp = 0;
    String cocktailval;

    void handleCocktailType(int value) {
    setState(() {
      cocktailtyp = value;
      switch (cocktailtyp) {
        case 1:
         cocktailval='apero';
          break;
        case 2:
           cocktailval='classic';
          break;
        case 3:
           cocktailval='party';
          break;
        case 4:
           cocktailval='alkoholfrei';
          break;
        case 5:
           cocktailval='sommer';
          break;
      }
    });
  }

    createData(){
    zutaten.add(_zutat1.text);
    if (_zutat2.text != null){
      zutaten.add(_zutat2.text);
    }
     if (_zutat3.text != null){
      zutaten.add(_zutat3.text);
    }
     if (_zutat4.text != null){
      zutaten.add(_zutat4.text);
    }
     if (_zutat5.text != null){
      zutaten.add(_zutat5.text);
    }
    zubereitung.add(_step1.text);

     if (_step2.text != null){
      zubereitung.add(_step2.text);
    }
     if (_step3.text != null){
      zubereitung.add(_step3.text);
    }
     if (_step4.text != null){
      zubereitung.add(_step4.text);
    }
     if (_step5.text != null){
      zubereitung.add(_step5.text);
    }

    decor.add(_decor1.text);

    if (_decor2.text != null){
      decor.add(_decor2.text);
    }
    if (_decor3.text != null){
      decor.add(_decor3.text);
    }


    DocumentReference ds=Firestore.instance.collection('Cocktail_rezepte').document(_cocktailname.text);
    Map<String,dynamic> cocktail={
      "Name":_cocktailname.text,
      "Schwierigkeit":_schwierigkeit.text,
      "Image":_imageURL.text,
      "Zutaten":zutaten,
      "Zubereitung":zubereitung,
      "Decor":decor,
      "Typ":cocktailtyp,

    };

    ds.setData(cocktail).whenComplete((){
      print("task updated");
    });



  }

  DefaultTabController _buildTabView({Widget body}) {
    const double _iconSize = 20.0;

    return DefaultTabController(
      length: 5,
      child: Scaffold(
        appBar: PreferredSize(
          // We set Size equal to passed height (50.0) and infinite width:
          preferredSize: Size.fromHeight(50.0),
          child: AppBar(
            elevation: 2.0,
            bottom: TabBar(
              labelColor: Theme.of(context).indicatorColor,
              tabs: [
                Tab(icon: Icon(Icons.local_bar, size: _iconSize)),
                Tab(icon: Icon(Icons.wb_sunny, size: _iconSize)),
                Tab(icon: Icon(Icons.favorite, size: _iconSize)),
                Tab(icon: Icon(Icons.add, size: _iconSize)),
                Tab(icon: Icon(Icons.settings, size: _iconSize)),
              ],
            ),  
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(5.0),
          child: body,
        ),
      ),
    );
  }

  Widget _buildContent() {
    if (appState.isLoading) {
      return _buildTabView(
        body: _buildLoadingIndicator(),
      );
    } else if (!appState.isLoading && appState.user == null) {
      return new LoginScreen();
    } else {
      return _buildTabView(
        body: _buildTabsContent(),
      );
    }
  }

  Center _buildLoadingIndicator() {
    return Center(
      child: new CircularProgressIndicator(),
    );
  }

  Column _buildSettings() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SettingsButton(
          Icons.exit_to_app,
          "Log out",
          appState.user.displayName,
          () async {
            await StateWidget.of(context).signOutOfGoogle();
          },
        ),
      ],
    );
  }

  Column _buildAddCocktail (){
    return Column(
      children: <Widget>[
        Flexible(
          // width: MediaQuery.of(context).size.width,
          // height: MediaQuery.of(context).size.height - 80,
          child: new ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                    style: TextStyle(
                      fontWeight:FontWeight.bold,
                      fontSize: 20.0,
                      ) ,
                    controller: _cocktailname,
                    // onChanged: (String cocktailname){
                    //    getCocktailName(cocktailname);   
                    // },
                    decoration: InputDecoration(
                      labelText: "Cocktailname: ",
                      // fillColor: Colors.blueGrey[300],
                      // filled: true,
                      ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                    style: TextStyle(
                      fontWeight:FontWeight.bold,
                      fontSize: 20.0,
                    ),
                    controller: _zutat1,
                    decoration: InputDecoration(labelText: "Zutat 1: "),
                    // onChanged: (String zutat1){
                    //    getCocktailZutat1(zutat1);   
                    // },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                    controller: _zutat2,
                    decoration: InputDecoration(labelText: "Zutat 2: "),
                    // onChanged: (String zutat2){
                    //    getCocktailZutat2(zutat2);   
                    // },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                    controller: _zutat3,
                    decoration: InputDecoration(labelText: "Zutat 3: "),
                    // onChanged: (String zutat3){
                    //    getCocktailZutat3(zutat3);   
                    // },
                  ),
                ),                                
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                    controller: _zutat4,
                    decoration: InputDecoration(labelText: "Zutat 4: "),
                    // onChanged: (String zutat4){
                    //    getCocktailZutat4(zutat4);   
                    // },
                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                    controller: _zutat5,
                    decoration: InputDecoration(labelText: "Zutat 5: "),
                    // onChanged: (String zutat5){
                    //    getCocktailZutat5(zutat5);   
                    // },
                  ),
                ), 

                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                    style: TextStyle(
                      fontWeight:FontWeight.bold,
                      fontSize: 20.0,
                      ) ,                    
                    controller: _step1,
                    decoration: InputDecoration(labelText: "Zubereitung Step 1: "),
                    // onChanged: (String step1){
                    //    getCocktailStep1(step1);   
                    // },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                    controller: _step2,
                    decoration: InputDecoration(labelText: "Zubereitung Step 2: "),
                    // onChanged: (String step2){
                    //    getCocktailStep2(step2);   
                    // },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                   controller: _step3,
                    decoration: InputDecoration(labelText: "Zubereitung Step 3: "),
                    // onChanged: (String step3){
                    //    getCocktailStep3(step3);   
                    // },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                    controller: _step4,
                    decoration: InputDecoration(labelText: "Zubereitung Step 4: "),
                    // onChanged: (String step4){
                    //    getCocktailStep4(step4);   
                    // },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                    controller: _step5,
                    decoration: InputDecoration(labelText: "Zubereitung Step 5: "),
                    // onChanged: (String step5){
                    //    getCocktailStep5(step5);   
                    // },
                  ),
                ),                                                                 
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                    style: TextStyle(
                      fontWeight:FontWeight.bold,
                      fontSize: 20.0,
                      ) ,                    
                    controller: _imageURL,
                    decoration: InputDecoration(labelText: "ImageURL: "),
                    // onChanged: (String imageURL){
                    //    getCocktailImageURL(imageURL);   
                    // },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                    style: TextStyle(
                      fontWeight:FontWeight.bold,
                      fontSize: 20.0,
                      ) ,                    
                    controller: _decor1,
                    decoration: InputDecoration(labelText: "Decor 1: "),
                    // onChanged: (String decor1){
                    //    getCocktailDecor1(decor1);   
                    // },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(                
                    controller: _decor2,
                    decoration: InputDecoration(labelText: "Decor 2: "),
                    // onChanged: (String decor2){
                    //    getCocktailDecor2(decor2);   
                    // },
                  ),
                ),                
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(                
                    controller: _decor3,
                    decoration: InputDecoration(labelText: "Decor 3: "),
                    // onChanged: (String decor3){
                    //    getCocktailDecor1(decor3);   
                    // },
                  ),
                ),                                                
                Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: TextField(
                    style: TextStyle(
                      fontWeight:FontWeight.bold,
                      fontSize: 20.0,
                      ) ,                    
                    controller: _schwierigkeit,
                    decoration: InputDecoration(labelText: "Schwierigkeit: "),
                    // onChanged: (String schwierigkeit){
                    //    getCocktailSchwierigkeit(schwierigkeit);   
                    // },
                  ),
                ),                                
                SizedBox(
                  height: 10.0,
                ),
                Center(
                  child: Text(
                    'Select Cocktail Type:',
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Radio(
                          value: 1,
                          groupValue: cocktailtyp,
                          onChanged: handleCocktailType,
                          activeColor: Color(0xFF1A237E),
                        ),
                        Text(
                          'apero',
                          style: TextStyle(fontSize: 16.0),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Radio(
                          value: 2,
                          groupValue: cocktailtyp,
                          onChanged: handleCocktailType,
                          activeColor: Color(0xFF1A237E),
                        ),
                        Text(
                          'classic',
                          style: TextStyle(
                            fontSize: 16.0,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Radio(
                          value: 3,
                          groupValue: cocktailtyp,
                          onChanged: handleCocktailType,
                          activeColor: Color(0xFF1A237E),
                        ),
                        Text(
                          'party',
                          style: TextStyle(fontSize: 16.0),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Radio(
                          value: 4,
                          groupValue: cocktailtyp,
                          onChanged: handleCocktailType,
                          activeColor: Color(0xFF1A237E),
                        ),
                        Text(
                          'alkoholfrei',
                          style: TextStyle(fontSize: 16.0),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Radio(
                          value: 5,
                          groupValue: cocktailtyp,
                          onChanged: handleCocktailType,
                          activeColor: Color(0xFF1A237E),
                        ),
                        Text(
                          'sommer',
                          style: TextStyle(fontSize: 16.0),
                        ),
                      ],
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    RaisedButton(
                        color: Color(0xFFFF5722),
                        shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.red[900])
                        ),
                        onPressed: () {
                          _cocktailname.dispose();
                          _step1.dispose();
                          _step2.dispose();
                          _step3.dispose();
                          _step4.dispose();
                          _step5.dispose();
                          _zutat1.dispose();
                          _zutat2.dispose();
                          _zutat3.dispose();
                          _zutat4.dispose();
                          _zutat5.dispose();
                          _decor1.dispose();
                          _decor2.dispose();
                          _decor3.dispose();
                          _schwierigkeit.dispose();
                          _imageURL.dispose();
                          Navigator.pushNamed(context, '/');
                        },
                        child: const Text(
                          "Cancel",
                          style: TextStyle(color: Color(0xFF455A64)),
                        )),
                    // This button results in adding the contact to the database
                    RaisedButton(
                        color: Color(0xFFC5E1A5),
                        shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.green[900])
                    ),
                        onPressed: () {
                          createData();
                          _cocktailname.clear();
                          _step1.clear();
                          _step2.clear();
                          _step3.dispose();
                          _step4.dispose();
                          _step5.dispose();
                          _zutat1.dispose();
                          _zutat2.dispose();
                          _zutat3.dispose();
                          _zutat4.dispose();
                          _zutat5.dispose();
                          _decor1.dispose();
                          _decor2.dispose();
                          _decor3.dispose();
                          _schwierigkeit.dispose();
                          _imageURL.dispose();
                          Navigator.pushNamed(context, '/');
                        },
                        child: const Text(
                          "Submit",
                          style: TextStyle(color: Color(0xFF455A64)),
                          
                        ))
                  ],
                )
              ],
            ),
          )
        ],
    
    );
  }

   
  // }

  TabBarView _buildTabsContent() {
    Padding _buildRecipes({List<String> ids}) {
      CollectionReference collectionReference =
          Firestore.instance.collection('Cocktail_rezepte');
      Stream<QuerySnapshot> stream;
      // The argument recipeType is set
      // if (recipeType != null) {
      //   stream = collectionReference
      //       .where("type", isEqualTo: recipeType.index)
      //       .snapshots();
      // } else {
        // Use snapshots of all recipes if recipeType has not been passed
        stream = collectionReference.orderBy('Name', descending: false).snapshots();
       
      
  

      // Define query depeneding on passed args
      return Padding(
        // Padding before and after the list view:
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: new StreamBuilder(
                stream: stream,
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (!snapshot.hasData) return _buildLoadingIndicator();
                  return new ListView(
                    children: snapshot.data.documents
                        // Check if the argument ids contains document ID if ids has been passed:
                        .where((d) => ids == null || ids.contains(d.documentID))
                        .map((document) {
                      return new RecipeCard(
                        recipe:
                            Rezepte.fromMap(document.data, document.documentID),
                        inFavorites:
                            appState.favorites.contains(document.documentID),
                        onFavoriteButtonPressed: _handleFavoritesListChanged,
                      );
                    }).toList(),
                  );
                },
              ),
            ),
          ],
        ),
      );
    }

    return TabBarView(
      children: [
        _buildRecipes(),
         Center(child: Icon(Icons.wb_sunny)),
        _buildRecipes(ids: appState.favorites),
        _buildAddCocktail(),
        _buildSettings(),
      ],
    );
  }

  // Inactive widgets are going to call this method to
  // signalize the parent widget HomeScreen to refresh the list view:
 void _handleFavoritesListChanged(String recipeID) {
    updateFavorites(appState.user.uid, recipeID).then((result) {
      // Update the state:
      if (result == true) {
        setState(() {
          if (!appState.favorites.contains(recipeID))
            appState.favorites.add(recipeID);
          else
            appState.favorites.remove(recipeID);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // Build the content depending on the state:
    appState = StateWidget.of(context).state;
    return _buildContent();
  }

}

