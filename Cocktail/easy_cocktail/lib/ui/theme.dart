import 'package:flutter/material.dart';

ThemeData buildTheme() {
  // We're going to define all of our font styles
  // in this method:
  TextTheme _buildTextTheme(TextTheme base) {
    return base.copyWith(
      headline5: base.headline5.copyWith(
        fontFamily: 'NotoSerifJP',
        fontSize: 50.0,
        fontWeight: FontWeight.w400,
        color: const Color(0xFFBDBDBD),
      ),
      // Used for the recipes' title:
      headline6: base.headline6.copyWith(
        fontFamily: 'NotoSerifJP',
        fontSize: 20.0,
        color: const Color(0xFF616161),
        fontWeight: FontWeight.bold,
      ),
      // Used for the recipes' duration:
      caption: base.caption.copyWith(
        color: const Color(0xFF424242),
      ),
      bodyText2: base.bodyText2.copyWith(color: const Color(0xFF616161))
    );
  }

  // We want to override a default light blue theme.
  final ThemeData base = ThemeData.light();

  return base.copyWith(
    textTheme: _buildTextTheme(base.textTheme),
    primaryColor: const Color(0xFFB0BEC5),
    indicatorColor: const Color(0xFF000000),
    scaffoldBackgroundColor: const Color(0xFFF5F5F5),
    accentColor: const Color(0xFFFFF8E1),
    iconTheme: IconThemeData(
      color: const Color(0xFFB71C1C),
      size: 20.0,
    ),
    accentIconTheme: IconThemeData(
      color: const Color(0xFF000000),
      size: 20.0,
    ),
    buttonColor: Colors.white,
    backgroundColor: Colors.white,
    tabBarTheme: base.tabBarTheme.copyWith(
      labelColor: const Color(0xFF616161),
      unselectedLabelColor: const Color(0xFF757575),
      
    )
  );
}