import 'package:cloud_firestore/cloud_firestore.dart';

Future<bool> updateFavorites(String uid, String recipeId) {
  DocumentReference favoritesReference =
      Firestore.instance.collection('users').document(uid);

  return Firestore.instance.runTransaction((Transaction tx) async {
    DocumentSnapshot postSnapshot = await tx.get(favoritesReference);
    if (postSnapshot.exists) {
      // Extend 'favorites' if the list does not contain the recipe ID:
      if (!postSnapshot.data['favorites'].contains(recipeId)) {
        await tx.update(favoritesReference, <String, dynamic>{
          'favorites': FieldValue.arrayUnion([recipeId])
        });
      // Delete the recipe ID from 'favorites':
      } else {
        await tx.update(favoritesReference, <String, dynamic>{
          'favorites': FieldValue.arrayRemove([recipeId])
        });
      }
    } else {
      // Create a document for the current user in collection 'users'
      // and add a new array 'favorites' to the document:
      await tx.set(favoritesReference, {
        'favorites': [recipeId]
      });
    }
  }).then((result) {
    return true;
  }).catchError((error) {
    print('Error: $error');
    return false;
  });
}
// import 'package:easy_cocktail/model/cocktail_rezepte.dart';

// List<Rezepte> getRecipes() {
//   return [
//     Rezepte(
//       id: '0',
//       cocktailname: 'Negroni',
//       typ: Cocktailtyp.classic,
//       zutaten: [
//         '3 cl Dry Gin',
//         '3 cl Roter Wermut',
//         '3 cl Campari',
//       ],
//       zubereitung: [
//         'Step 1',
//         'Step 2',
//         'Step 3',
//       ],
//       decor: [
//         'Orangenscheibe',
//         'decor 2',
//         'decor 3',
//       ],      
//       schwierigkeit: 'easy',
      
//       imageURL:
//           'https://images.unsplash.com/photo-1582457601528-5f8757143fb1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1001&q=80',
//     ),
//     Rezepte(
//       id: '1',
//       cocktailname: 'Aperol Spritz',
//       typ: Cocktailtyp.sommer,
//       zutaten: [
//         '3 cl Prosseco',
//         '2 cl Aperol',
//         '1 cl Mineralwasser',
//       ],
//       zubereitung: [
//         'Step 1',
//         'Step 2',
//         'Step 3',
//       ],
//       decor: [
//         'Orangenscheibe',
//         'decor 2',
//         'decor 3',
//       ],      
//       schwierigkeit: 'easy',
      
//       imageURL:
//       'https://images.unsplash.com/photo-1560512823-829485b8bf24?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80'
//     ), 
//     Rezepte(
//       id: '2',
//       cocktailname: 'Tom Collins',
//       typ: Cocktailtyp.sommer,
//       zutaten: [
//         '6 cl Dry Gin',
//         '3-4 cl Zitronensaft',
//         '10 cl Mineralwasser',
//       ],
//       zubereitung: [
//         'Step 1',
//         'Step 2',
//         'Step 3',
//       ],
//       decor: [
//         'Zitroinen Twist',
//         'decor 2',
//         'decor 3',
//       ],      
//       schwierigkeit: 'easy',
      
//       imageURL:
//       'https://www.pamperedchef.ca/iceberg/com/recipe/1445515-lg.jpg'
//     ),   
//   ];
// }

// List<String> getFavoritesIDs() {
//   return [
//     '0',
//     '1',
//     '2',
//   ];
// }