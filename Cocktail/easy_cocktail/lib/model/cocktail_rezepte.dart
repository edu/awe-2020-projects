

enum Cocktailtyp {
  apero,
  classic,
  party,
  alkoholfrei,
  sommer,
}

class Rezepte {

  final String id;
  final String cocktailname;
  final Cocktailtyp typ;
  final List<String> zutaten;
  final List<String> zubereitung;
  final List<String> decor;
  final String schwierigkeit;
  final String imageURL;


  const Rezepte ({
    this.id,
    this.cocktailname,
    this.typ,
    this.zutaten,
    this.zubereitung,
    this.decor,
    this.schwierigkeit,
    this.imageURL,
  });

  Rezepte.fromMap(Map<String, dynamic> data, String id)
      : this(
          id: id,
          typ: Cocktailtyp.values[data['Typ']],
          cocktailname: data['Name'],
          schwierigkeit: data['Schwierigkeit'],
          zutaten: new List<String>.from(data['Zutaten']),
          zubereitung: new List<String>.from(data['Zubereitung']),
          decor: new List<String>.from(data['Decor']),
          imageURL: data['Image'],
          
        );
}