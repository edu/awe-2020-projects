
import 'package:flutter/material.dart';
import 'package:easy_cocktail/app.dart';
import 'package:easy_cocktail/ui/widgets/state_widget.dart';

void main() => runApp(new StateWidget(
      child: new CocktailApp(),
    ));


