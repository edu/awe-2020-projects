
import 'package:flutter/material.dart';
import 'package:easy_cocktail/ui/screens/home.dart';
import 'package:easy_cocktail/ui/screens/login.dart';
import 'package:easy_cocktail/ui/theme.dart';

class CocktailApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cocktail Rezepte',
      theme: buildTheme(),
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => HomeScreen(),
        '/login': (context) => LoginScreen(),
      },
    );
  }
}