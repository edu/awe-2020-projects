package ch.fabian.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import java.util.Random;

public class game extends ApplicationAdapter {
	SpriteBatch batch;
	Texture background;
	Texture [] beens;
	Texture flyswatter_botton;
	Texture flyswatter_top;
	ShapeRenderer shapeRenderer;
	BitmapFont font;
	Texture gameOver;

	int score = 0;
	int scoreFlyswatter = 0;
	int BeenMove = 0;
	float flyswatter_Velosity= 4;
	int amountOfFlywatters = 4;
	float distanceBetweenFlyswatters;
	float gap = 350;
	float maxflyswatter;
	float[] cuttentlyflayswatter = new float[amountOfFlywatters];
	float[] flyswatterPos = new float[amountOfFlywatters];
	float beenY = 0;
	float gravity =2;
	float velocity = 0;
	int state = 0;
	Rectangle[] topFlyswatterRec;
	Rectangle[] bottonFlyswatterRec;



	Circle beenBox;

	Random randomGenerator;


	
	@Override
	public void create () {
		batch = new SpriteBatch();
		background = new Texture("background.png");
		beens= new Texture[2];
		shapeRenderer = new ShapeRenderer();
		beenBox = new Circle();
		gameOver = new Texture("Loserbanner.png");

		font = new BitmapFont();
		font.setColor(Color.WHITE);
		font.getData().setScale(10);

		beens[0] = new Texture("beeny.png");
		beens[1]	= new Texture("beeny2.png");
		flyswatter_botton = new Texture("flyswatter_botton_red_narrow.png");
		flyswatter_top = new Texture("flyswatter_Top_red_narrow.png");

		maxflyswatter= Gdx.graphics.getHeight()/2 -gap /2 -50;
		randomGenerator = new Random();
		distanceBetweenFlyswatters = Gdx.graphics.getWidth() /2;

		topFlyswatterRec = new Rectangle[amountOfFlywatters];
		bottonFlyswatterRec= new Rectangle[amountOfFlywatters];

		start();


	}

	public void start()

	{
		beenY = Gdx.graphics.getHeight() / 2;

		for(int i = 0; i< amountOfFlywatters;i++)
		{
			cuttentlyflayswatter[i] = (randomGenerator.nextFloat() -0.5f )* (Gdx.graphics.getHeight()-gap-300);
			flyswatterPos[i] = Gdx.graphics.getWidth() /2 -flyswatter_botton.getWidth() /2 + (Gdx.graphics.getWidth() /2) +  i * distanceBetweenFlyswatters;

			topFlyswatterRec[i]= new Rectangle();
			bottonFlyswatterRec[i] = new Rectangle();
		}


	}
	@Override
	public void render () {

		batch.begin();
		batch.draw(background,0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());

	if(state == 1)
	{
		if(flyswatterPos[scoreFlyswatter] < Gdx.graphics.getWidth() /2)
		{
			score ++;
			Gdx.app.log("socre",String.valueOf(score));
			if (scoreFlyswatter < amountOfFlywatters -1)
			{
				scoreFlyswatter ++;
			}else{
				scoreFlyswatter = 0;
			}
		}

		if(Gdx.input.justTouched())
		{
			velocity = -30;
		}

		for(int i = 0; i< amountOfFlywatters;i++)
		{
			if(flyswatterPos[i] < - flyswatter_top.getWidth())
			{
				cuttentlyflayswatter[i] = (randomGenerator.nextFloat() -0.5f )* (Gdx.graphics.getHeight()-gap-300);
				flyswatterPos[i] += amountOfFlywatters * distanceBetweenFlyswatters;
			}else{
				flyswatterPos[i] = flyswatterPos[i] - flyswatter_Velosity;
			}

			batch.draw(flyswatter_top,flyswatterPos[i], Gdx.graphics.getHeight()/2 +gap + cuttentlyflayswatter[i] );
			batch.draw(flyswatter_botton,flyswatterPos[i],Gdx.graphics.getHeight()/2  - flyswatter_botton.getHeight()-gap + cuttentlyflayswatter[i]);

			topFlyswatterRec[i] = new Rectangle(flyswatterPos[i] + flyswatter_botton.getWidth()/3, Gdx.graphics.getHeight()/2 +gap + cuttentlyflayswatter[i] , flyswatter_top.getWidth() /6 , flyswatter_top.getHeight());
			bottonFlyswatterRec[i]	= new Rectangle(flyswatterPos[i] + flyswatter_botton.getWidth()/3,Gdx.graphics.getHeight()/2  - flyswatter_botton.getHeight()-gap + cuttentlyflayswatter[i] , flyswatter_botton.getWidth()/6, flyswatter_botton.getHeight());

		}





		if(beenY > 0|| velocity < 0 )
		{
			velocity += gravity;
			beenY -= velocity;
		}else{

			state = 2;
		}

	}else if(state == 0){
		if(Gdx.input.justTouched())
		{
			state = 1;
		}
	}else if(state == 2)
	{
		batch.draw(gameOver, Gdx.graphics.getWidth() / 2 - gameOver.getWidth() /2, Gdx.graphics.getHeight() / 2 - gameOver.getHeight() / 2);

		if(Gdx.input.justTouched())
		{
			state = 1;

			start();
			score = 0;
			velocity = 0;
			scoreFlyswatter = 0;
		}
	}


		if(BeenMove == 0) // Flügelschlag der Biene
		{
			BeenMove = 1;
		} else{
			BeenMove = 0;
		}
		batch.draw(beens[BeenMove],Gdx.graphics.getWidth() / 3 - beens[BeenMove].getWidth(), beenY);

		font.draw(batch,String.valueOf(score),Gdx.graphics.getWidth()/2,200);

		batch.end();

		beenBox.set(Gdx.graphics.getWidth()/3 - (beens[BeenMove].getWidth() /2) , beenY + beens[BeenMove].getHeight() /2, beens[BeenMove].getWidth() /2);

		/*
		shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
		shapeRenderer.setColor(Color.RED);
		shapeRenderer.circle(beenBox.x, beenBox.y,beenBox.radius);
		*/


		for (int i = 0; i < amountOfFlywatters; i++)
		{
			/*
			shapeRenderer.rect(flyswatterPos[i] + flyswatter_botton.getWidth()/3, Gdx.graphics.getHeight()/2 +gap + cuttentlyflayswatter[i] , flyswatter_top.getWidth() /6 , flyswatter_top.getHeight());
			shapeRenderer.rect(flyswatterPos[i] + flyswatter_botton.getWidth()/3,Gdx.graphics.getHeight()/2  - flyswatter_botton.getHeight()-gap + cuttentlyflayswatter[i] , flyswatter_botton.getWidth()/6, flyswatter_botton.getHeight());
			*/

			if(Intersector.overlaps(beenBox,topFlyswatterRec[i])  || Intersector.overlaps(beenBox,bottonFlyswatterRec[i]))
			{
				Gdx.app.log("Collison" , "yes");
				state = 2;
			}
		}
		/*
		shapeRenderer.end();

		 */


	}

	@Override
	public void dispose () {
		batch.dispose();
		background.dispose();
	}
}
