const express = require("express");
const http = require("http");
const socketio = require("socket.io");
const Joi = require("@hapi/joi");

const ValidationError = require("@hapi/joi/lib/errors").ValidationError;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Server setup
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Express app
const app = express();
app.use(express.static("./public"));
app.use("/", (req, res) => {
    res.status(404).end("Not Found");
});


// Server setup
const server = http.createServer(app);
const port = process.env.PORT || 3000;
server.listen(port, () => {
    console.log("Listening on port " + port);
});

// Socket setup
const io = socketio(server);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Message types
const MESSAGE_GAME_STATE_UPDATE =    "game-state-update";
const MESSAGE_GAME_MESSAGE =         "game-message";
const MESSAGE_ERROR =                "ERROR";

// Game states
const STATE_CONNECTED =              "connected";
const STATE_NEW_GAME =               "new-game";
const STATE_WAITING_ROOM =           "waiting-room";
const STATE_GAME_START =             "game-start";
const STATE_SHIPS_READY =            "ships-ready";
const STATE_WAIT_FOR_OPPONENT =      "wait-for-opponent";
const STATE_PLAYERS_READY =          "players-ready";
const STATE_GAME_FINISHED =          "game-finished";
const STATE_GAME_TERMINATED =        "game-terminated";

// Game message actions
const GAME_MESSAGE_ACTION_SHOT =     "shot";
const GAME_MESSAGE_ACTION_ANSWER =   "answer";

// Game terminated reasons
const GAME_TERMINATED_REASON_TERMINATED = "terminated";
const GAME_TERMINATED_REASON_TIMEOUT =    "timeout";


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Schemas
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// GSU general schema
const schemaGSU = Joi.object({
    state: Joi.string().required().valid(
        STATE_NEW_GAME,
        STATE_SHIPS_READY,
        STATE_GAME_FINISHED,
        STATE_GAME_TERMINATED
    ),
    data: Joi.object().unknown().default({})
});

// GSU new-game
const schemaNewGameData = Joi.object({
    nickname: Joi.string().alphanum().min(3).max(25).required(),
    blacklist: Joi.array().items(
        Joi.string().alphanum().min(3).max(25)
    ).default([]),
});

// GSU game-terminated
const schemaGameTerminatedData = Joi.object({
    reason: Joi.string().required().valid(
        GAME_TERMINATED_REASON_TERMINATED,
        GAME_TERMINATED_REASON_TIMEOUT
    ),
});


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Game logic
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Array to store the current players
const players = [];

// A new player connects
io.on("connection", socket => {
    console.log(`${socket.id} connected`);

    // Create a player object and push in players list
    const player = {
        socket: socket,
        state: STATE_CONNECTED
    };
    players.push(player);

    // A game state update is received
    socket.on(MESSAGE_GAME_STATE_UPDATE, gsuNotVal => {
        // Log the game state update
        logGameStateUpdate(player, gsuNotVal, false);

        // Check general schema of the update message
        schemaGSU.validateAsync(gsuNotVal)
            .then( gsu => {
                console.log("  Game State Update Object OK");

                // Distinguish the states
                switch (gsu.state) {
                    case STATE_NEW_GAME:
                        // The player wants to start a new game

                        // Can only transit there from STATE_CONNECTED
                        if (player.state !== STATE_CONNECTED) {
                            sendTransitionError(player, player.state, STATE_NEW_GAME);
                            return;
                        }

                        // Validate the player data
                        return schemaNewGameData.validateAsync(gsu.data)
                            .then( playerData => {
                                console.log("  Player data OK");

                                // Player data ok, assign to the player object
                                Object.assign(player, playerData);

                                // Put the player in the waiting room
                                putInWaitingRoom(player);
                                logPlayers();

                                // Try to find an opponent for the player
                                findOpponentFor(player);
                            });
                        break;

                    case STATE_SHIPS_READY:
                        // The player has placed his ships

                        // Update the player object
                        player.state = STATE_SHIPS_READY;

                        // Send game state update
                        sendGameStateUpdate(player, STATE_WAIT_FOR_OPPONENT);

                        // Check if opponent is done
                        const opponent = findPlayer(player.opponentId);
                        if (opponent.state === STATE_SHIPS_READY) {
                            // Both players are ready
                            console.log("  opponent ready");

                            // Update states
                            player.state = STATE_PLAYERS_READY;
                            opponent.state = STATE_PLAYERS_READY;

                            // Decide first draw
                            const playerFirstDraw = Math.random() >= 0.5;
                            // Send the game state update
                            sendGameStateUpdate(player, STATE_PLAYERS_READY, {
                                firstDraw: playerFirstDraw
                            });
                            sendGameStateUpdate(opponent, STATE_PLAYERS_READY, {
                                firstDraw: !playerFirstDraw
                            });
                        }
                        else {
                            console.log("  Opponent not ready");
                        }
                        break;

                    case STATE_GAME_FINISHED:
                        // The player lost the game

                        // Get the opponent
                        const opp = findPlayer(player.opponentId);
                        if (opp) {
                            console.log("  Game finished.", opp.nickname, "won.");

                            // Update the player and opponent object
                            // It goes back to connected, as the player is still connected but not in a game anymore
                            player.state = STATE_CONNECTED;
                            opp.state = STATE_CONNECTED;

                            // Send the game state updates
                            sendGameStateUpdate(player, STATE_GAME_FINISHED, {
                                winner: opp.nickname
                            });
                            sendGameStateUpdate(opp, STATE_GAME_FINISHED, {
                                winner: opp.nickname
                            });

                            // Log the players list
                            logPlayers()
                        }

                        break;

                    case STATE_GAME_TERMINATED:
                        // The player wants to end the game

                        // Check the data payload
                        return schemaGameTerminatedData.validateAsync(gsu.data)
                            .then(data => {

                                // Update the player state
                                player.state = STATE_CONNECTED;

                                // Check if the player has already an opponent
                                const opp2 = findPlayer(player.opponentId);
                                if (opp2) {

                                    // If an opponent exists, send a game state update
                                    sendGameStateUpdate(opp2, STATE_GAME_TERMINATED, {
                                        reason: data.reason,
                                    })

                                    // Update opponent state
                                    opp2.state = STATE_CONNECTED;

                                }
                                else {
                                    console.log(`  ${player.nickname} has no opponent.`)
                                }

                                console.log(`  ${player.nickname} terminated`);
                                logPlayers();
                            });

                        break;

                    default:
                        console.log("Unknown GSU:", gsu.state);
                        break;
                }

            })
            .catch( err => {
                console.log("  Invalid GSU:", gsuNotVal);
                if (err instanceof ValidationError) {
                    // If the error is a validation error, send back the error message
                    sendError(player, err.details[0].message);
                    console.log("  Validation Error:", err.details[0].message)
                }
                else {
                    console.log("  ", err);
                }
            })
    });


    // Forward game messages
    socket.on(MESSAGE_GAME_MESSAGE, msg => {
        // Get the opponent
        const opponent = findPlayer(player.opponentId);

        // Only in playing state
        if (player.state === STATE_PLAYERS_READY && opponent.state === STATE_PLAYERS_READY) {
            opponent.socket.emit(MESSAGE_GAME_MESSAGE, msg);
            logGameMessage(player, opponent, msg);
        }
    });


    // When the client disconnects
    socket.on("disconnect", () => {
        console.log(socket.id, "disconnected");

        // Remove the player from the players object
        removePlayer(player);
        logPlayers();
    })

});


// Sends an Error to the client
function sendError(player, errorMessage, data) {
    player.socket.emit(MESSAGE_ERROR, {
        errorMessage: errorMessage,
        data: data
    });
}

// Sends an error when a client cannot transit to a certain state
function sendTransitionError(player, fromState, toState) {
    sendError(player, `Cannot transit from ${fromState} to ${toState}`);
    console.log(`  Invalid transition ${fromState} to ${toState}`);
}


// Sends a game state update to a player
function sendGameStateUpdate(player, state, data) {
    const gsu = {
        state: state,
        data: data ? data : {}
    };
    // Send the game state update
    player.socket.emit(MESSAGE_GAME_STATE_UPDATE, gsu);
    // Then log it
    logGameStateUpdate(player, gsu, true)
}

// Updates the state of a player and sends a game state update and sends along some optional data
function updatePlayerState(player, state, data) {
    player.state = state;
    sendGameStateUpdate(player, state, data)
;}


// Logs a game state update to the console
function logGameStateUpdate(player, update, servertoClient) {
    const name = player.nickname ? player.nickname : player.socket.id;
    console.log(
        servertoClient ? `Server -> ${name}:` : `${name} -> Server:`,
        update.state,
        update.data
    );
}


// Logs a game message
function logGameMessage(sender, receiver, gm) {
    console.log(`${sender.nickname} -> ${receiver.nickname}`, gm.action, gm.data);
}


// Puts a player into the waiting room by setting his state and sending a game state update
function putInWaitingRoom(player) {
    player.state = STATE_WAITING_ROOM;
    sendGameStateUpdate(player, STATE_WAITING_ROOM, {});
    console.log(`  Put ${player.nickname} in the waiting room`);
}


// Logs the players in a readable manner
function logPlayers() {
    if (players.length === 0) {
        console.log("[No players online]");
    }
    else {
        console.log(players.reduce((acc, p) => (
            acc +
            (p.nickname ? p.nickname : p.socket.id) +
            ` (${p.state}), `
        ), "[").slice(0, -2) + "]");
    }
}


// Removes a certain player from the players list by socket id
function removePlayer(player) {
    players.splice(players.findIndex(p => p.socket.id === player.socket.id), 1);
}


// Tries to match two players. The flag is used in order to avoid racing conditions
let findOpponentFor_searching = false;
function findOpponentFor(player) {
    if (findOpponentFor_searching) {
        // Try again later
        setTimeout(matchPlayers, 100);
        return;
    }
    else {
        findOpponentFor_searching = true;

        // Search two players
        const opponent = players.find(p =>
            p.socket.id !== player.socket.id &&
            p.state === STATE_WAITING_ROOM &&
            player.blacklist.indexOf(p.nickname) === -1);

        // Check if an opponent was found
        if (opponent) {
            // Update the player and send game state updates
            player.state = STATE_GAME_START;
            player.opponentId = opponent.socket.id;
            updatePlayerState(player, STATE_GAME_START, {
                opponentNickname: opponent.nickname
            });
            // Update the opponent and send game state updates
            opponent.state = STATE_GAME_START;
            opponent.opponentId = player.socket.id;
            updatePlayerState(opponent, STATE_GAME_START, {
                opponentNickname: player.nickname
            });
            console.log(`  ${player.nickname} plays vs. ${opponent.nickname}`);
        }
        else {
            console.log(`  No opponent found for ${player.nickname}`);
        }

        findOpponentFor_searching = false;
    }
}

// Finds and returns a player by his socket id
function findPlayer(id) {
    return players.find(p => p.socket.id === id);
}

