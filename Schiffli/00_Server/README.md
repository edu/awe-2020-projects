## Installation
* (Nodejs und npm installieren)
* `npm i` im Projektverzeichnis ausführen

**Ausführen**

`npm start` im Projektverzeichnis ausführen


## Kommunikation
Da es in dem Modul Anwendungsentwicklung um das Programmieren von Android Apps geht,
dient der Server nur der Kommunikation und dem paaren von Spielern. Die Logik des Spiels
wird im Client implementiert. Als Beispiel: Es interessiert den Server nicht,
ob der Spieler die Schiffe gemäss den Regeln plaziert hat. Das muss die Client App überprüfen.

Alle Kommunikation läuft über einen der folgenden drei Kanäle:
 * `game-state-update`
 * `game-message`
 * `ERROR`
 
Die versendeten Daten sind in jedem Fall direkt im JSON-Format.
 
## Game State Update
Nachrichten im Kanal `game-state-update` bedeuten eine Änderung des Zustands des Spiels.
Der Aufbau des versendeten Objekts sieht generell so aus:
```
{
    state: String,
    data: Object
}
```
`state` Status des Game State Updates  
`data` (Optional) Ein Objekt mit den zugehörigen Daten.

Folgende Game States Updates gibt es:


##### New Game
Wird vom Client an den Server gesendet, wenn der Client ein neues Spiel starten möchte.
Das Spiel wird vom Server beim Empfang dieses Updates erstellt und befindet sich dann 
direkt im Zustand `new-game`.
```
{
    state: "new-game",
    data: {
        nickname: String,
        blacklist: [String]
    }
}
```
`nickname`: Anzeigename des Spielers. Zwischen 3 und 25 alphanumerische Zeichen.  
`blacklist`: (Optional) Array mit Namen von Spielern, gegen die der Spieler nicht
spielen will.

##### Waiting Room
Wird vom Server an den Client gesendet, wenn der Server den Client dem Warteraum
hinzugefügt hat. Dieses Update wird in jedem Fall versendet, auch wenn direkt ein
bereits wartender Gegner gefunden wurde. Das Spiel befindet sich nach erhalt dieses
Updates im Zustand `waiting-room`.
```
{
    state: "waiting-room",
    data: {}
}
``` 

##### Game Started
Wird vom Server an den Client gesendet, wenn der Server einen Gegner für den Client
gefunden hat und das Spiel beginnen kann.
```
{
    state: "new-game",
    data: {
        opponentNickname: String
    }
}
```
`opponentNickname`: Nickname des Gegners.

##### Schiffe bereit
Wird vom Client an den Server gesendet, wenn der Spieler seine Schiffe platziert hat.
Der Server weiss nicht, wo der spieler die Schiffe platziert. Der Client muss also selbst
prüfen, ob die Schiffe gemäss den Regeln platziert wurden.
```
{
    state: "ships-ready",
    data: {}
}
```

##### Auf Gegner warten
Wird vom Server an den Client gesendet, wenn darauf gewartet werden muss, dass der
Gegner seine Schiffe platziert hat. Wird in jedem fall gesendet, auch an den zweiten 
Spieler, der mit platzieren fertig ist.
```
{
    state: "wait-for-opponent",
    data: {}
}
```

##### Spieler Bereit
Wird vom Server an den Client gesendet, wenn beide Spieler ihre Schiffe gesetzt haben
und das Spiel beginnen kann.
```
{
    state: "players-ready",
    data: {
        firstDraw: Boolean
    }
}
```
`firstDraw`: Ist bei einem der beiden Spieler `true`, beim Anderen `false`. Der Spieler mit
 `true` hat den ersten Zug.

##### Spiel beendet
Wird einerseits vom Client an den Server gesendet, wenn der Client das Spiel verloren hat. Der Client,
welcher dieses Game State Update sendet, ist automatisch der Verlierer. Es werden keine Daten
mitgesendet.
```
{
    state: "game-finished",
    data: {}
}
```
Andererseits wird dieselbe Nachricht als Antwort vom Server an beide clients gesendet. In den
Daten wird der Nickname des Gewinners mitgesendet.
```
{
    state: "game-finished",
    data: winner: String
}
```
`winner`: Der Nickname des Spielers, der gewonnen hat.

##### Spiel abgebochen
Wird einerseits vom Client an den Server gesendet, wenn der Client das Spiel abbrechen
will. Andererseits wird es vom Server an den anderen Client gesendet, wenn der
Erste das Spiel abgebrochen hat oder die Verbindung verloren hat.
```
{
    state: "game-terminated",
    data: {
        reason: String
    }
}
```
`reason`: Warum der Client das Spiel abbricht. / Warum der Gegner das Spiel
abgebrochen hat. Muss einer der folgenden Werte sein:
* `terminated`: Wenn der Spieler das Spiel aktiv abbricht oder die Client App schliesst.
* `timeout`: Wenn ein Spieler nicht innerhalb von `60s` einen Zug getätigt hat.
 
 
 #### Game Messages
 Game Messages werden vom einen Spieler an seinen Gegner gesendet. Der Server überprüft
 Struktur und Inhalt dieser Nachrichten nicht. Trotzdem wird hier eine Struktur definiert.
 Game Messages können erst nach dem Game State Update `players-ready` versendet werden.
 
 ##### Schuss
Wird vom Spieler, der gerade and der Reihe ist, an seinen Gegner gesendet.
 ```
 {
     action: "shot",
     data: {
         coordinates: {
            x: Int,
            y: Int
        }
     }
 }
 ```
 `coordinates`: Die Koordinaten der Zelle, die beschossen werden soll. 
 Im Objekt `coordinates` befinden sich die beiden Integer `x` und `y`. Die Zelle 
 mit den Koordinaten (0/0) ist die Zelle links oben. Die positive x-Achse geht
 nach rechts und die positive y-Achse nach Unten.
 
 ##### Antwort
Wird vom Spieler, der gerade and der Reihe ist, an seinen Gegner gesendet.
 ```
 {
     action: "answer",
     data: {
         hit: Boolean,
         sunk: Boolean
     }
 }
 ```
 `hit`: Gibt an, ob ein Schiffsteil getroffen wurde. Ist auch `true`, wenn das Schiff
 versenkt wurde.  
 `sunken`: Gibt an, ob das Schiff versenkt wurde oder nicht.
 


####Fehlerkanal
Alle Fehler werden über den Kanal _ERROR_ kommuniziert.
Das JSON Objekt hat folgenden Aufbau:
```
{
    errorMessage: String,
    data: Object
}
```