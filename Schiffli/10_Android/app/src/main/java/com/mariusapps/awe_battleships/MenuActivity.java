package com.mariusapps.awe_battleships;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.service.autofill.FieldClassification;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MenuActivity extends AppCompatActivity {

    // Log tag
    private static final String TAG = "AWE_Bs.MenuActivity";

    // References for the layout's views
    private EditText et_nickname;

    // Key for the nickname shared preference
    private static final String KEY_NICKNAME = "nickname";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate");

        // Inflate the layout
        setContentView(R.layout.activity_menu);

        // Find the necessary views
        et_nickname = findViewById(R.id.et_nickname);

        // Get the last used nickname from the shared preferences
        String nickname = ""; // Default value if shared preference not found
        nickname = getPreferences(Context.MODE_PRIVATE).getString(KEY_NICKNAME, nickname);

        Log.d(TAG, "onCreate: loaded Nickname: " + nickname);
        // Set the Text of the EditText to the nickname from the preferences
        et_nickname.setText(nickname);
    }


    /**
     * Callback method for the "New Game" Button. Validates the entered Nickname and then either
     * starts the GameActivity, putting the nickname as a extra, or displays a error message
     * @param view: The mandatory view argument for a onClick callback
     */
    public void onNewGameButtonClick(View view) {
        // Get the entered Nickname from the EditText
        String nickname = et_nickname.getText().toString();

        // Create a Regex matcher in order to validate the nickname
        final Pattern pattern = Pattern.compile("[a-zA-Z0-9]{3,25}", Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(nickname);

        // Validate the nickname
        if (matcher.matches()) {
            Log.d(TAG, "onNewGameButtonClick: Nickname " + nickname + " OK");

            // Store the nickname in a shared preference for the next time the app is used
            SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
            editor.putString(KEY_NICKNAME, nickname);
            editor.apply();


            // Start the GameActivity
            Intent newGameIntent = new Intent(this, GameActivity.class);
            newGameIntent.putExtra("nickname", nickname);
            startActivity(newGameIntent);
        }
        else {
            Log.d(TAG, "onNewGameButtonClick: Nickname " + nickname + " not OK");
            // Show error message in edit text
            et_nickname.setError(getString(R.string.menu_invalid_nickname_message));
        }

    }



    // Die folgenden Lifecycle - Methoden sind nur für Verständnis- und Debugzwecke eingebaut
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
}
