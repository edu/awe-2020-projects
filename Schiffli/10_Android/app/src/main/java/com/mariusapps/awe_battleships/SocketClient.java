package com.mariusapps.awe_battleships;

import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

public class SocketClient {

    // Log tag
    private static final String TAG = "AWE_Bs.SocketClient";


    // Socket messages constants
    public static final String MESSAGE_GAME_STATE_UPDATE = "game-state-update";
    public static final String MESSAGE_GAME_MESSAGE = "game-message";
    public static final String MESSAGE_ERROR = "ERROR";

    // Game state update values
    public static final String STATE_NEW_GAME = "new-game";

    // Server address
    private static final String serverURL = "https://awe-battleship.herokuapp.com/";
//    private static final String serverURL = "http://10.0.2.2:3000"; // Localhost
//    private static final String serverURL = "http://192.168.43.214:3000/";
//    private static final String serverURL = "http://192.168.1.98:3000/";

    // Socket instance
    private Socket socket;

    // Reference to the Game Manager
    GameManager gm;

    // Store if the socket was already connected or not
    private boolean connected = false;

    // Store the player's nickname
    private String nickname;


    SocketClient(GameManager gm) {
        Log.d(TAG, "SocketClient: Constructor");

        this.gm = gm;

        // Get a socket instance
        try {
            socket = IO.socket(serverURL);
        } catch (URISyntaxException e) {
            Log.d(TAG, "connect: URISyntaxException");
            e.printStackTrace();
        }

        // Add a callback for the successful connection event
        socket.on(Socket.EVENT_CONNECT, onConnect);

        // Add a callback for connection error
        // ToDo: Fehlermeldung an User
        socket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);

        // Add a callback for game state update messages
        socket.on(MESSAGE_GAME_STATE_UPDATE, onGameStateUpdate);

        // Add a callback for game messages
        socket.on(MESSAGE_GAME_MESSAGE, onGameMessage);

        // Add a callback for game error messages
        socket.on(MESSAGE_ERROR, onErrorMessage);
    }


    // Connect to the server. If the connection is successful, a new game is requested automatically
    public void connect(String nickname) {
        Log.d(TAG, "connect");
        // Store the nickname
        this.nickname = nickname;
        // Try to connect
        socket.connect();
    }

    // Disconnect from the server
    public void disconnect() {
        Log.d(TAG, "disconnect");
        // Disconnect from the server
        socket.disconnect();
    }

    // Sends a game state update to the server
    public void sendGSU(String state, JSONObject data) {
        JSONObject gsu = new JSONObject();
        try {
            gsu.put("state", state);
            gsu.put("data", data);
            socket.emit(MESSAGE_GAME_STATE_UPDATE, gsu);
            Log.d(TAG, "sendGSU: Emitted GSU " + state);
        } catch (JSONException e) {
            Log.d(TAG, "sendGSU: JSON ERROR");
            e.printStackTrace();
        }
    }

    // Sends a game message to the opponent
    public void sendGameMessage(String action, JSONObject data) {
        JSONObject gameMsg = new JSONObject();
        try {
            gameMsg.put("action", action);
            gameMsg.put("data", data);
            socket.emit(MESSAGE_GAME_MESSAGE, gameMsg);
            Log.d(TAG, "sendGameMessage: Emitted Game Message " + action);
        } catch (JSONException e) {
            Log.d(TAG, "sendGameMessage: JSON ERROR");
            e.printStackTrace();
        }
    }


    // On successful connection
    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // Sometime, this Event fires twice for some unknown reason. But it is important to
            // send the nickname only once. Thus, a flag is used to check if the connection was
            // already made
            if (!connected) {
                // Set flag
                connected = true;
                gm.onConnected();
            }
        }
    };


    // Listen to connection errors
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
//            Log.d(TAG, "EVENT_CONNECT_ERROR");
//            for (Object arg : args) {
//                Log.d(TAG, "  " + arg.toString());
//            }
            gm.onConnectionError();
            // ToDo: Fehlermeldung anzeigen
        }
    };

    // Listen to Game State Updates
    private Emitter.Listener onGameStateUpdate = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // Decode the GSU and call the Game Manager
            if (args.length > 0 && args[0] instanceof  JSONObject) {
                JSONObject gsu = (JSONObject) args[0];
                try {
                    String state = gsu.getString("state");
                    JSONObject data = gsu.getJSONObject("data");
                    Log.d(TAG, "onGameStateUpdate: Received GSU " + state);
                    gm.onGsuReceived(state, data);
                } catch (JSONException ignore) {}
            }
        }
    };

    // Listen to Game Messages
    private Emitter.Listener onGameMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // Decode the Game Message and call the Game Manager
            if (args.length > 0 && args[0] instanceof  JSONObject) {
                JSONObject msg = (JSONObject) args[0];
                try {
                    String action = msg.getString("action");
                    JSONObject data = msg.getJSONObject("data");
                    Log.d(TAG, "onGameMessage: Received Game Message " + action);
                    gm.onGameMessageReceived(action, data);
                } catch (JSONException ignore) {}
            }
        }
    };

    // Listen to Error messages from the server
    private Emitter.Listener onErrorMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "MESSAGE_ERROR");
            for (Object arg : args) {
                Log.d(TAG, "  " + arg);
            }
        }
    };




}
