package com.mariusapps.awe_battleships;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.github.oxo42.stateless4j.StateMachine;
import com.github.oxo42.stateless4j.StateMachineConfig;
import com.github.oxo42.stateless4j.delegates.Action;

class GameStateMachine {
    // Log tag
    private static final String TAG = "AWE_Bs.StateMachine";


    // Mutable live data for Game State Machine state changes
    MutableLiveData<State> stateChange;

    // States
    public enum State {
        IDLE,                   // Initial state
        CONNECTING,             // Connection is being established
        CONNECTION_ERROR,       // A connection error occurred
        NEW_GAME,               // Join the game server, gameStateUpdate new-game
        WAITING_ROOM,           // Wait for a new opponent,
        PLACING_SHIPS,          // Place ships,
        SHIPS_PLACED,           // Ships are placed, wait for server answer
        WAITING_FOR_OPPONENT,   // Wait till opponent placed ships,
        PLAYERS_TURN,           // It is the player's turn
        PLAYER_FIRING,          // The Player fired a shot
        PLAYER_GOT_ANSWER,      // The player received an answer from the opponent after a shot
        OPPONENTS_TURN,         // It is the opponent's turn
        SENDING_ANSWER,         // Sending an answer to the opponent and wait for the turn to end
        PLAYER_WON,             // The player won the game
        OPPONENT_WON,           // The opponent won the game
        OPPONENT_TERMINATED      // The opponent terminated the game
    }

    // Triggers
    public enum Trigger {
        CONNECT,
        CONNECTED,
        CONNECTION_FAILED,
        GO_TO_WAITING_ROOM,
        GAME_START,
        SHIPS_READY,
        WAIT_FOR_OPPONENT,
        PLAYER_TURN,
        PLAYER_FIRE,
        OPPONENT_ANSWERED,
        OPPONENT_TURN,
        OPPONENT_FIRE,
        GAME_FINISHED,
        OPPONEND_TERMINATE
    }

    private StateMachine<State, Trigger> gameState;

    public GameStateMachine(
        MutableLiveData<State> stateChange,
        Action enterConnecting,
        Action enterNewGame,
        Action enterShipsPlaced,
        Action enterPlayerFiring,
        Action enterPlayerGotAnswer,
        Action enterSendingAnswer,
        Action enterPlayerWon,
        Action enterOpponentWon
    ) {

        // Assign Mutable Live Data
        this.stateChange = stateChange;

        // Set up state machine
        StateMachineConfig<State, Trigger> gameStateConfig = new StateMachineConfig<State, Trigger>();

        gameStateConfig.configure(State.IDLE)
                .permit(Trigger.CONNECT, State.CONNECTING);

        gameStateConfig.configure(State.CONNECTING)
                .permit(Trigger.CONNECTED, State.NEW_GAME)
                .permit(Trigger.CONNECTION_FAILED, State.CONNECTION_ERROR)
                .onEntry(enterConnecting);

        gameStateConfig.configure(State.CONNECTION_ERROR)
                .permit(Trigger.CONNECTED, State.NEW_GAME)
                .permitReentry(Trigger.CONNECTION_FAILED);

        gameStateConfig.configure(State.NEW_GAME)
                .permit(Trigger.GO_TO_WAITING_ROOM, State.WAITING_ROOM)
                .onEntry(enterNewGame);

        gameStateConfig.configure(State.WAITING_ROOM)
                .permit(Trigger.GAME_START, State.PLACING_SHIPS);

        gameStateConfig.configure(State.PLACING_SHIPS)
                .permit(Trigger.SHIPS_READY, State.SHIPS_PLACED)
                .permit(Trigger.OPPONEND_TERMINATE, State.OPPONENT_TERMINATED);

        gameStateConfig.configure(State.SHIPS_PLACED)
                .permit(Trigger.WAIT_FOR_OPPONENT, State.WAITING_FOR_OPPONENT)
                .permit(Trigger.OPPONEND_TERMINATE, State.OPPONENT_TERMINATED)
                .onEntry(enterShipsPlaced);

        gameStateConfig.configure(State.WAITING_FOR_OPPONENT)
                .permit(Trigger.PLAYER_TURN, State.PLAYERS_TURN)
                .permit(Trigger.OPPONEND_TERMINATE, State.OPPONENT_TERMINATED)
                .permit(Trigger.OPPONENT_TURN, State.OPPONENTS_TURN);

        gameStateConfig.configure(State.PLAYERS_TURN)
                .permit(Trigger.PLAYER_FIRE, State.PLAYER_FIRING)
                .permit(Trigger.OPPONEND_TERMINATE, State.OPPONENT_TERMINATED)
                .permit(Trigger.GAME_FINISHED, State.PLAYER_WON);

        gameStateConfig.configure(State.PLAYER_FIRING)
                .permit(Trigger.OPPONENT_ANSWERED, State.PLAYER_GOT_ANSWER)
                .permit(Trigger.GAME_FINISHED, State.PLAYER_WON)
                .permit(Trigger.OPPONEND_TERMINATE, State.OPPONENT_TERMINATED)
                .onEntry(enterPlayerFiring);

        gameStateConfig.configure(State.PLAYER_GOT_ANSWER)
                .permit(Trigger.OPPONENT_TURN, State.OPPONENTS_TURN)
                .permit(Trigger.PLAYER_TURN, State.PLAYERS_TURN)
                .permit(Trigger.GAME_FINISHED, State.PLAYER_WON)
                .permit(Trigger.OPPONEND_TERMINATE, State.OPPONENT_TERMINATED)
                .onEntry(enterPlayerGotAnswer);

        gameStateConfig.configure(State.OPPONENTS_TURN)
                .permit(Trigger.OPPONENT_FIRE, State.SENDING_ANSWER)
                .permit(Trigger.GAME_FINISHED, State.OPPONENT_WON)
                .permit(Trigger.OPPONEND_TERMINATE, State.OPPONENT_TERMINATED);

        gameStateConfig.configure(State.SENDING_ANSWER)
                .permit(Trigger.PLAYER_TURN, State.PLAYERS_TURN)
                .permit(Trigger.OPPONENT_TURN, State.OPPONENTS_TURN)
                .permit(Trigger.GAME_FINISHED, State.OPPONENT_WON)
                .permit(Trigger.OPPONEND_TERMINATE, State.OPPONENT_TERMINATED)
                .onEntry(enterSendingAnswer);

        gameStateConfig.configure(State.PLAYER_WON)
                .permit(Trigger.OPPONEND_TERMINATE, State.OPPONENT_TERMINATED)
                .onEntry(enterPlayerWon);

        gameStateConfig.configure(State.OPPONENT_WON)
                .permit(Trigger.OPPONEND_TERMINATE, State.OPPONENT_TERMINATED)
                .onEntry(enterOpponentWon);


        gameState = new StateMachine<>(State.IDLE, gameStateConfig);
        gameState.fireInitialTransition();
    }

    public void fireTransition(Trigger trigger) {
        Log.d(TAG, "fireTransition: Fire " + trigger);
        Log.d(TAG, "fireTransition: Current state: " + gameState.getState());
        gameState.fire(trigger);
        stateChange.postValue(getState());
        Log.d(TAG, "Entered " + getState());
    }

    public State getState() {
        return gameState.getState();
    }

    public boolean isInState(State state) {
        return gameState.isInState(state);
    }
}