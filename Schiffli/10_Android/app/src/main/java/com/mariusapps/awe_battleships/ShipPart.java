package com.mariusapps.awe_battleships;

public class ShipPart extends PlayFieldCell {

    // Constants for ship part kinds
    public static final int MIDDLE    = 0;
    public static final int END_UP    = 1;
    public static final int END_DOWN  = 2;
    public static final int END_LEFT  = 3;
    public static final int END_RIGHT = 4;
    public static final int ROUND     = 5;

    // Store the kind of ship part
    private int kindOfPart;

    // If the ship has an invalid position
    private boolean invalidPos = false;


    public ShipPart(int x, int y, int kindOfPart) {
        super(x, y);
        this.kindOfPart = kindOfPart;
    }

    // Getter for the ship part kind
    public int getShipPartKind() {
        return kindOfPart;
    }

    public void setInvalidPos() {
        this.invalidPos = true;
    }
    public void setValidPos() {
        this.invalidPos = false;
    }
    public boolean hasInvalidPos() {
        return invalidPos;
    }
}
