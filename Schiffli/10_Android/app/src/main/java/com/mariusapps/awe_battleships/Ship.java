package com.mariusapps.awe_battleships;

import android.util.Log;

import androidx.annotation.NonNull;

public class Ship {

    private static final String TAG = "AWE_Bs.Ship";

    // Position of the ship. The position of the ship is always the top left cell of the ship
    private int x;
    private int y;

    // Size of the ship (number of cells)
    private int size;

    // Orientation of the ship. True = down, False = right
    private boolean goesDown;

    // Play field cells belonging to this ship
    private ShipPart[] shipParts;


    // Public constructor
    public Ship(int x, int y, int size, boolean goesDown) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.goesDown = goesDown;

        // Generate the ship
        shipParts = generateShip(x, y, size, goesDown);
    }

    // Ship generator. Returns an array of Play Field Cells representing a ship with a given
    // Configuration
    private ShipPart[] generateShip(int x, int y, int size, boolean goesDown) {
        ShipPart[] shipParts = new ShipPart[size];
        for (int i = 0; i < size; i++) {
            if (goesDown) {
                int kind = size == 1 ? ShipPart.ROUND : i == 0 ? ShipPart.END_UP : i == size - 1 ? ShipPart.END_DOWN : ShipPart.MIDDLE;
                shipParts[i] = new ShipPart(x, y + i, kind);
            }
            else {
                int kind = size == 1 ? ShipPart.ROUND : i == 0 ? ShipPart.END_LEFT : i == size - 1 ? ShipPart.END_RIGHT : ShipPart.MIDDLE;
                shipParts[i] = new ShipPart(x + i, y, kind);
            }
        }
        return shipParts;
    }

    // Rotate the ship and set the ship parts
    public void rotate(int playFieldSize) {
        Log.d(TAG, "rotate: ");
        if (goesDown) {
            if (x + size <= playFieldSize) {
                shipParts = generateShip(x, y, size, false);
                goesDown = false;
            }
        }
        else {
            if (y + size <= playFieldSize) {
                shipParts = generateShip(x, y, size, true);
                goesDown = true;
            }
        }
    }

    // Rotates the ship at a certain coordinate. If the coordinate does not belong to this ship
    // or if the rotation would exceed the play field, nothing is changed
    public void rotateAt(int xCoord, int yCoord, int playFieldSize) {
        // Check if the coords belong to this ship
        if (isAtCoord(xCoord, yCoord)) {
            // Calculate distance from ship start cell
            int d = goesDown ? yCoord - y : xCoord - x;
            // Calculate the new ship position
            int newX = goesDown ? x - d : x + d;
            int newY = goesDown ? y + d : y - d;
            // Check if the new ship would be inside the play field
            if (possibleAt(newX, newY, !goesDown, playFieldSize)) {
                shipParts = generateShip(newX, newY, size, !goesDown);
                x = newX;
                y = newY;
                goesDown = !goesDown;
            }
        }
    }

    // Moves a certain part of the ship to a new location and moves the other parts accordingly. If
    // the movement exceeds the play field borders, nothing is done.
    public void moveAt(int fromX, int fromY, int toX, int toY, int playFieldSize) {
        // CHeck if "from" coords belong to this ship
        if (isAtCoord(fromX, fromY)) {
            // Calculate new x and y pos
            int newX = x + (toX - fromX);
            int newY = y + (toY - fromY);
            Log.d(TAG, "moveAt: " + this + " -> " + newX + "/" + newY);
            if (possibleAt(newX, newY, goesDown, playFieldSize)) {
                shipParts = generateShip(newX, newY, size, goesDown);
                x = newX;
                y = newY;
                Log.d(TAG, "moveAt: New Ship: " + this);
            }
        }
    }

    // Returns true if this ship would be possible at a certain position and rotation respecting the play field size
    private boolean possibleAt(int posX, int posY, boolean hereGoesDown, int playFieldSize) {
        return ((!hereGoesDown && posX >= 0 && posX + size <= playFieldSize && posY >= 0 && posY < playFieldSize) ||
                ( hereGoesDown && posX >= 0 && posX < playFieldSize && posY >= 0 && posY + size <= playFieldSize));
    }


    // Get the parts of the ships as an array
    public ShipPart[] getShipParts() {
        return shipParts;
    }

    // Return if this ship covers a certain coordinate
    public boolean isAtCoord(int xCoord, int yCoord) {
        return ( goesDown && xCoord == x && yCoord >= y && yCoord < y + size) ||
               (!goesDown && yCoord == y && xCoord >= x && xCoord < x + size);
    }

    // Returns true if the ship is sunk (all Ship Parts hit).
    public boolean isSunk() {
        for (ShipPart p : shipParts) {
            if (!p.wasHit()) return false;
        }
        return true;
    }


    // Get X-position of Ship
    public int getX() {
        return x;
    }
    // Get Y-position of ship
    public int getY() {
        return y;
    }
    // Get size of ship
    public int getSize() { return size; }
    // Get direction of ship
    public boolean isGoingDown() { return goesDown; }

    @NonNull
    @Override
    public String toString() {
        return "Ship (" + x + "/" + y + ", " + size + (goesDown ? " down" : " right") + ")";
    }

    // Mark a ship as invalidly placed
    public void markInvalidPos() {
        for (ShipPart part : shipParts) {
            part.setInvalidPos();
        }
    }

    // Mark a ship as valid placed
    public void markValidPos() {
        for (ShipPart part : shipParts) {
            part.setValidPos();
        }
    }




}
