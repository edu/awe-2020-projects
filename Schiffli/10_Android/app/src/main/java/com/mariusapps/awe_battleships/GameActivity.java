package com.mariusapps.awe_battleships;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.content.pm.ActivityInfo;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class GameActivity extends FragmentActivity {

    // Log tag
    private static final String TAG = "AWE_Bs.GameActivity";

    private GameActivity self;

    // UI Elements
    private TextView tv_nicknames;
    private TextView tv_stateText;
    private Button btn_action;
    private ImageButton btn_mark;
    private ImageButton btn_mute;

    // Game Manager Instance
    GameManager gm;

    // Fragment Manager
    private FragmentManager fragmentManager;

    // Play field fragments for the player and the opponent
    private PlayFieldFragment playerFragment;
    private PlayFieldFragment opponentFragment;

    // Game started flag
    private boolean gameStarted = false;

    // Muted flag
    private boolean muted = false;

    // Sounds
    private SoundPool soundPool;
    private int explosion, splash;
    
    // Flag for which layout is used
    boolean isPhone;


    // Toast for displaying a message on a state change. This Toast object has to be declared at
    // class level because toast messages are being buffered on some android versions. When two
    // toasts are shown within a short amount of time on a Nexus phone (e.g.), they are displayed
    // one after another in full length. On Huawai phones (e.g.) the second toast gets displayed
    // immediately, "overwriting" the first toast. With a class level Toast object, the same toast
    // can be used all the time and be cancelled first on each state change
    Toast stateChangeToast;



    @SuppressLint({"ShowToast", "SourceLockedOrientationActivity"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        self = this;

        // Get the GameManager instance
        gm = new ViewModelProvider(this).get(GameManager.class);

        // create soundPool
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            soundPool = new SoundPool.Builder()
                    .setMaxStreams(1)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        }
        explosion = soundPool.load(getApplicationContext(), R.raw.explosion,1);
        splash = soundPool.load(getApplicationContext(), R.raw.splash,1);



        // Inflate the layout
        setContentView(R.layout.activity_game);

        // Check if the app is in tablet or phone layout
        isPhone = getResources().getBoolean(R.bool.portrait_only);

        // Restrict to portrait orientation if phone or to landscape if tablet
        if (isPhone) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        // Receive the nickname from the intent
        Intent intent = getIntent();
        String nickname = intent.getStringExtra("nickname");
        Log.d(TAG, "onCreate: Nickname: " + nickname);

        // Get the UI elements' references
        tv_nicknames = findViewById(R.id.tv_nicknames);
        tv_stateText = findViewById(R.id.tv_stateText);
        btn_action = findViewById(R.id.btn_action);
        btn_mark = findViewById(R.id.btn_mark);
        btn_mute = findViewById(R.id.btn_mute);

        // Set the Nickname in the nickname TextView
        tv_nicknames.setText(getString(R.string.game_tv_nicknames_no_opponent, nickname));

        // Disable the buttons initially
        btn_action.setEnabled(false);
        btn_mark.setEnabled(false);

        // Initialize the state change toast
        stateChangeToast = new Toast(this);

        // Get the fragment manager
        fragmentManager = getSupportFragmentManager();

        // Create the player and the opponent play field fragment
        playerFragment = new PlayFieldFragment("(Player) ->");
        opponentFragment = new PlayFieldFragment("(Opponent) ->");

        // Add the fragments to the layout, but detach the opponent fragment, as the player play
        // field should be shown initially
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (isPhone) {
            transaction.add(R.id.frag_playfield, playerFragment);
            transaction.add(R.id.frag_playfield, opponentFragment);
            transaction.detach(opponentFragment);
        }
        else {
            transaction.add(R.id.frag_playfield, playerFragment);
            transaction.add(R.id.frag_playfield_2, opponentFragment);
        }
        transaction.commit();

        Log.d(TAG, "onCreate: BEFORE CALLBACK");

        // Play Field View touch event listener for player play field and opponent play field
        playerFragment.setOnCellActionListener(new PlayFieldView.OnCellActionListener() {
            @Override
            public void onCellTouched(PlayFieldCell cell) {
                gm.onCellTouched(cell, true);
            }

            @Override
            public void onCellClicked(PlayFieldCell cell) {
                gm.onCellClicked(cell);
            }

            @Override
            public void onCellReleased(PlayFieldCell cell) {
                gm.onCellReleased(cell);
            }

            @Override
            public void onDraggedFromTo(PlayFieldCell from, PlayFieldCell to) {
                gm.onCellDraggedFromTo(from, to);
            }
        });
        opponentFragment.setOnCellActionListener(new PlayFieldView.OnCellActionListener() {
            @Override
            public void onCellTouched(PlayFieldCell cell) {
                gm.onCellTouched(cell, false);
            }

            @Override
            public void onCellClicked(PlayFieldCell cell) {
                gm.onCellClicked(cell);
            }

            @Override
            public void onCellReleased(PlayFieldCell cell) {
                gm.onCellReleased(cell);
            }

            @Override
            public void onDraggedFromTo(PlayFieldCell from, PlayFieldCell to) {
                gm.onCellDraggedFromTo(from, to);
            }
        });


        // This callback gets executed after the layout was created
        getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        if (!gameStarted) {
                            // Start the game
                            gm.newGame(nickname, playerFragment.getPlayFieldData(), opponentFragment.getPlayFieldData());
                            // Set the game started flag
                            gameStarted = true;

                            // Initialize the player Play Field Fragment with ships
                            playerFragment.initializeShips();
                        }

                    }
        });


        // Get the state updates from the Game Manager
        gm.getStateChange().observe(this, new Observer<GameStateMachine.State>() {
            @Override
            public void onChanged(GameStateMachine.State state) {
                Log.d(TAG, "onStateChanged: " + state);

                // Cancel the last toast
                stateChangeToast.cancel();

                switch (state) {
                    case CONNECTION_ERROR:
                        showToast("Connection Error", Toast.LENGTH_SHORT);
                        break;

                    case WAITING_ROOM:
                        showToast("Waiting for an opponent...", Toast.LENGTH_LONG);
                        tv_stateText.setText(R.string.game_tv_stateText_wait_opponent);
                        showPlayerField();
                        break;

                    case PLACING_SHIPS:
                        tv_nicknames.setText(getString(
                                R.string.game_tv_nicknames_opponent,
                                gm.getPlayerNickname(),
                                gm.getOpponentNickname()
                        ));
                        btn_action.setText(R.string.game_btn_action_place_ships);
                        btn_action.setEnabled(true);
                        showToast("Place your Ships!", Toast.LENGTH_LONG);
                        tv_stateText.setText(R.string.game_tv_stateText_place_ships);
                        break;

                    case SHIPS_PLACED:
                        btn_action.setText("");
                        btn_action.setEnabled(false);
                        break;

                    case WAITING_FOR_OPPONENT:
                        showToast("Waiting for the opponent to place his ships...", Toast.LENGTH_LONG);
                        tv_stateText.setText(R.string.game_tv_stateText_wait_opponent);
                        break;

                    case PLAYERS_TURN:
                        tv_stateText.setText(R.string.game_tv_stateText_your_turn);
                        btn_action.setText(R.string.game_btn_action_fire);
                        btn_action.setEnabled(true);
                        btn_mark.setEnabled(true);
                        showOpponentsField();
                        break;

                    case PLAYER_FIRING:
                        btn_action.setEnabled(false);
                        btn_mark.setEnabled(false);
                        break;

                    case PLAYER_GOT_ANSWER:
                    case SENDING_ANSWER:
                        if (gm.wasLastShotSunk()) {
                            showToast("Sunk!", Toast.LENGTH_LONG);
                            if (!muted) {
                                soundPool.play(explosion,1,1,1,0,1);
                            }
                        }
                        else if(gm.wasLastShotHit()) {
                            showToast("Hit!",Toast.LENGTH_LONG);
                            if (!muted) {
                                soundPool.play(explosion,1,1,1,0,1);
                            }
                        }
                        else {
                            showToast("Missed!",Toast.LENGTH_LONG);
                            if (!muted) {
                                soundPool.play(splash,1,1,1,0,1);
                            }
                        }

                        // Start a delay. This is needed in this state to activate the next transition.
                        // Like this, the player can see the result of his / the opponent's shot and then switch to the
                        // other play field on the next transition after a delay
                        Handler handler = new Handler();
                        Runnable afterDelay = () -> {
                            gm.onDelayFinished();
                        };
                        handler.postDelayed(afterDelay, 3000);
                        break;

                    case OPPONENTS_TURN:
                        tv_stateText.setText(R.string.game_tv_stateText_his_turn);
                        btn_action.setText(R.string.game_btn_action_fire);
                        btn_action.setEnabled(false);
                        showPlayerField();
                        break;

                    case PLAYER_WON:
                        tv_stateText.setText(R.string.game_tv_stateText_you_won);
                        btn_action.setText("");
                        btn_action.setEnabled(false);
                        break;

                    case OPPONENT_WON:
                        tv_stateText.setText(R.string.game_tv_stateText_he_won);
                        btn_action.setText("");
                        btn_action.setEnabled(false);
                        break;

                    case OPPONENT_TERMINATED:
                        showToast("The opponent left the game", Toast.LENGTH_LONG);
                        btn_action.setEnabled(false);
                        btn_mark.setEnabled(false);
                        tv_stateText.setText(R.string.game_tv_stateText_opponent_terminaterd);
                        break;


                    default:
                        break;
                }
            }
        });

    }



    /***********************************************************************************************
     * Helper Methods
     **********************************************************************************************/
    private void showToast(String text, int duration) {
        stateChangeToast = Toast.makeText(this, text, duration);
        stateChangeToast.setGravity(Gravity.CENTER, 0, 0);
        stateChangeToast.show();
    }

    private void showPlayerField() {
        Log.d(TAG, "showPlayerField: PLAYER FIELD " + isPhone);
        if (isPhone) {
            FragmentTransaction t = fragmentManager.beginTransaction();
            t.detach(opponentFragment);
            t.attach(playerFragment);
            t.commit();
        }
    }

    private void showOpponentsField() {
        Log.d(TAG, "showOpponentsField: OPPONENT FIELD " + isPhone);
        if (isPhone) {
            FragmentTransaction t = fragmentManager.beginTransaction();
            t.detach(playerFragment);
            t.attach(opponentFragment);
            t.commit();
        }
    }


    /***********************************************************************************************
     * UI Callbacks
     **********************************************************************************************/

    // Handler for the action button
    public void onActionBtnClick(View view) {
        Log.d(TAG, "onActionBtnClick");
        gm.onActionButtonClick();
    }

    // Handler for the abort button
    public void onAbortBtnClick(View view) {
        Log.d(TAG, "onAbortBtnClick: ABORT");
        finish();
    }

    // Handler for the mark button
    public void onMarkBtnClick(View v) {
        Log.d(TAG, "onMarkBtnClick");
        gm.onMarkButtonClick();
    }

    // Handler for the mute button
    public void onMuteBtnClick(View v) {
        Log.d(TAG, "onMuteBtnClick");
        muted = !muted;
        if (muted) {
            btn_mute.setImageResource(R.drawable.ic_volume_off_black_24dp);
        }
        else {
            btn_mute.setImageResource(R.drawable.ic_volume_up_black_24dp);
        }
    }


    /***********************************************************************************************
     * Lifecycle Methods
     **********************************************************************************************/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        gm.onActivityDestroyed();

        soundPool.release();
        soundPool=null;
    }





    // Die folgenden Lifecycle - Methoden sind nur für Verständnis- und Debugzwecke eingebaut
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }
}
