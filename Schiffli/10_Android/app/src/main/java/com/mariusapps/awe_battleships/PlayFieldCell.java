package com.mariusapps.awe_battleships;

public class PlayFieldCell {

    // Position
    private int x;
    private int y;

    // Whether to show a crosshair on the cell or not
    private boolean crosshair = false;

    // Whether the cell was hit or not
    boolean hit = false;


    // Constructor
    public PlayFieldCell(int x, int y) {
        this.x = x;
        this.y = y;
    }


    // Getter for x and y
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }

    // Get the crosshair flag
    public boolean hasCrosshair() {
        return crosshair;
    }
    // Sets or resets the crosshair flag
    public void setCrosshair() {
        crosshair = true;
    }
    public void resetCrosshair() {
        crosshair = false;
    }

    // Get and set the hit flag
    public boolean wasHit() {
        return hit;
    }
    public void setHit() {
        hit = true;
    }
    public void resetHit() {
        hit = false;
    }


}
