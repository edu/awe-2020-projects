package com.mariusapps.awe_battleships;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.github.oxo42.stateless4j.delegates.Action;

import org.json.JSONException;
import org.json.JSONObject;

public class GameManager extends ViewModel {

    private static final String TAG = "AWE_Bs.GameManager";

    // Nicknames of the Player and Opponent
    private String playerNickname;
    private String opponentNickname;

    // Flag to save whose turn it is
    private boolean myTurn;

    // Socket Client Class
    private SocketClient socket;

    // Reference to the play field data of player and opponent play fields
    private PlayFieldData playerPlayFieldData;
    private PlayFieldData opponentPlayFieldData;

    // Game State Machine
    private GameStateMachine sm;

    // Mutable live data for Game State Machine state changes
    private MutableLiveData<GameStateMachine.State> stateChange;

    // Flags for last hit / sunk message
    private boolean lastShotHit;
    private boolean lastShotSunk;

    // Store last coordinates on which the opponent fired
    private int[] lastCoords;

    // Store win / lost flag on GSU game-finished
    private boolean playerWon;

    // Saves whether the game finished or not
    boolean gameFinished = false;


    public GameManager() {
        super();
        Log.d(TAG, "GameManager: Constructor");

        /**********************************
         *  Set up Game Manager
         **********************************/

        stateChange = new MutableLiveData<>();
        stateChange.setValue(GameStateMachine.State.IDLE);


        /**********************************
         *  Set up Networking
         **********************************/

        // Create socket client
        socket = new SocketClient(this);


        /**********************************
         *  Set up UI Callbacks
         **********************************/

        // Cell touched listeners


        /**********************************
         *  Set up State Machine
         **********************************/

        // All game logic is implemented in the following onEntry methods of the Game State
        // Machine states. The callbacks following this constructor just fire transitions and
        // assign values.  Updates to the UI are made in the Game Activity

        sm = new GameStateMachine(
                stateChange,

                // Enter CONNECTING
                new Action() {
                    @Override
                    public void doIt() {
                        socket.connect(playerNickname);
                    }
                },

                // Enter NEW_GAME
                new Action() {
                    @Override
                    public void doIt() {
                        // Send GSU new-game
                        try {
                            JSONObject data = new JSONObject("{\"nickname\": \"" + playerNickname + "\"}");
                            socket.sendGSU("new-game", data);
                        } catch (JSONException ignored) {
                        }
                    }
                },

                // Enter SHIPS_PLACED
                new Action() {
                    @Override
                    public void doIt() {
                        // Send GSU ships-ready
                        socket.sendGSU("ships-ready", null);
                    }
                },

                // Enter PLAYER_FIRING
                new Action() {
                    @Override
                    public void doIt() {
                        int[] coords = opponentPlayFieldData.getCrosshairCoords();
                        if (coords != null) {
                            try {
                                JSONObject data = new JSONObject("{\"coordinates\": {\"x\": " + coords[0] + ", \"y\": " + coords[1] + "}}");
                                socket.sendGameMessage("shot", data);
                            } catch (JSONException ignored) {
                            }
                        } else {
//                            ToDo: Nachricht "Please select a cell". Braucht vielleich einen zusätzlichen Zustand
                        }
                    }
                },

                // Enter PLAYER_GOT_ANSWER
                new Action() {
                    @Override
                    public void doIt() {
                        // If the player hit, unveil the cell
                        // ToDo: Zelle auf einen zerstörten / getroffenen Zustand setzen und entsprechendes Bild anzeigen, sound abspielen, animation...
                        int[] coords = opponentPlayFieldData.getCrosshairCoords();
                        if (coords != null && coords.length == 2) {
                            if (lastShotHit) {
                                opponentPlayFieldData.unveilCellAsShipPart(coords[0], coords[1]);
                                opponentPlayFieldData.setCellHit(coords[0], coords[1]);
                            }
                            // If not, mark the cell as hit
                            else {
                                opponentPlayFieldData.setCellHit(coords[0], coords[1]);
                            }
                        }
                    }
                },

                // Enter SENDING_ANSWER
                new Action() {
                    @Override
                    public void doIt() {
                        if (lastCoords != null && lastCoords.length == 2) {

                            // Update the UI
                            playerPlayFieldData.setCrosshairTo(lastCoords[0], lastCoords[1]);

                            // Mark the cell as hit
                            playerPlayFieldData.setCellHit(lastCoords[0], lastCoords[1]);

                            // Check whether the opponent's shot was a hit or not
                            lastShotHit = playerPlayFieldData.isShipAt(lastCoords[0], lastCoords[1]);
                            if (lastShotHit) {
                                Ship s = playerPlayFieldData.getShipAt(lastCoords[0], lastCoords[1]);
                                lastShotSunk = s.isSunk();
                            } else lastShotSunk = false;


                            // Assemble and send the answer
                            try {
                                JSONObject data = new JSONObject("{\"hit\": " + lastShotHit + ", \"sunk\": " + lastShotSunk + "}");
                                socket.sendGameMessage("answer", data);
                            } catch (JSONException e) {
                                Log.d(TAG, "doIt: " + e);
                                e.printStackTrace();
                            }

                            // Check if the player lost, if yes send the game-finished GSU
                            if (playerPlayFieldData.allShipPartsHit()) {
                                socket.sendGSU("game-finished", null);
                            }
                        }
                    }
                },

                // Enter PLAYER_WON
                new Action() {
                    @Override
                    public void doIt() {
                        gameFinished = true;
                    }
                },

                // Enter OPPONENT_WON
                new Action() {
                    @Override
                    public void doIt() {
                        gameFinished = true;
                    }
                }
        );


    }


    /***********************************************************************************************
     * UI Callbacks (Data from GameActivity to GameManager)
     **********************************************************************************************/

    // Allows the GameActivity to start a new Game
    public void newGame(String playerNickname, PlayFieldData playerPlayFieldData, PlayFieldData opponentPlayFieldData) {
        // Set the nickname
        this.playerNickname = playerNickname;
        // Set play field data
        this.playerPlayFieldData = playerPlayFieldData;
        this.opponentPlayFieldData = opponentPlayFieldData;

        // Fire initial Transition
        sm.fireTransition(GameStateMachine.Trigger.CONNECT);
    }

    // Cell touched callback
    public void onCellTouched(PlayFieldCell cell, boolean wasPlayerField) {
        Log.d(TAG, "onPlayerCellTouched: " + cell.getX() + "/" + cell.getY());

        // If in state PLACE_SHIPS
        switch (sm.getState()) {
            case PLACING_SHIPS:
                // ToDo: Remove and replace with the placement of Ships rather than ShipParts
                break;

            case PLAYERS_TURN:
                if (!wasPlayerField) {
                    opponentPlayFieldData.setCrosshairTo(cell.getX(), cell.getY());
                }
                break;

            default:
                break;
        }

    }

    // Cell clicked callback
    public void onCellClicked(PlayFieldCell cell) {
        Log.d(TAG, "onCellClicked: " + cell.getX() + "/" + cell.getY());

        // If in state PLACE_SHIPS
        switch (sm.getState()) {
            case WAITING_ROOM:
            case PLACING_SHIPS:
                playerPlayFieldData.rotateShipAt(cell.getX(), cell.getY());
                for (Ship ship:playerPlayFieldData.getShips()){
                    ship.markValidPos();
                }
                for (Ship ship:playerPlayFieldData.testShips()){
                    ship.markInvalidPos();
                }

                break;

            default:
                break;
        }
    }

    // Drag from a cell to another cell
    public void onCellDraggedFromTo(PlayFieldCell from, PlayFieldCell to) {
        Log.d(TAG, "onCellDraggedFromTo: " + from.getX() + "/" + from.getY() + " -> " + to.getX() + "/" + to.getY());

        // If in state PLACE_SHIPS
        switch (sm.getState()) {
            case WAITING_ROOM:
            case PLACING_SHIPS:
                // Start the drag motion
                if (!playerPlayFieldData.isDragging()) {
                    playerPlayFieldData.startDragging(from.getX(), from.getY());
                }
                // Drag the ship
                playerPlayFieldData.drag(from.getX(), from.getY(), to.getX(), to.getY());
                for (Ship ship:playerPlayFieldData.getShips()){
                    ship.markValidPos();
                }
                for (Ship ship:playerPlayFieldData.testShips()){
                    ship.markInvalidPos();
                }
                break;

            default:
                break;
        }
    }

    // A cell was released which was not clicked (Cell released after a drag)
    public void onCellReleased(PlayFieldCell cell) {
        Log.d(TAG, "onCellReleased: " + cell.getX() + "/" + cell.getY());

        // If in state PLACE_SHIPS
        switch (sm.getState()) {
            case WAITING_ROOM:
            case PLACING_SHIPS:
                playerPlayFieldData.stopDragging();
                break;

            default:
                break;
        }
    }

    // Action Button clicked
    public void onActionButtonClick() {
        switch (sm.getState()) {
            case PLACING_SHIPS:
                Log.d(TAG, "onActionButtonClick: Test ships: "+ playerPlayFieldData.testShips());
                if (playerPlayFieldData.testShips().isEmpty()) {
                    sm.fireTransition(GameStateMachine.Trigger.SHIPS_READY);
                }
                break;
            case PLAYERS_TURN:
                if (opponentPlayFieldData.getCrosshairCoords() != null) {
                    sm.fireTransition(GameStateMachine.Trigger.PLAYER_FIRE);
                }
                break;
        }
    }

    // A delay finished
    public void onDelayFinished() {
        switch (sm.getState()) {
            case PLAYER_GOT_ANSWER:
                opponentPlayFieldData.removeCrosshair();
                if (lastShotHit) {
                    sm.fireTransition(GameStateMachine.Trigger.PLAYER_TURN);
                } else {
                    sm.fireTransition(GameStateMachine.Trigger.OPPONENT_TURN);
                }
                break;

            case SENDING_ANSWER:
                playerPlayFieldData.removeCrosshair();
                if (lastShotHit) {
                    sm.fireTransition(GameStateMachine.Trigger.OPPONENT_TURN);
                } else {
                    sm.fireTransition(GameStateMachine.Trigger.PLAYER_TURN);
                }
                break;

            default:
                break;
        }
    }

    // Click on the mark button
    public void onMarkButtonClick() {
        switch (sm.getState()) {
            case PLAYERS_TURN:
                int[] pos = opponentPlayFieldData.getCrosshairCoords();
                if (pos != null && pos.length == 2) {
                    opponentPlayFieldData.toggleCellHit(pos[0], pos[1]);
                }
                break;

            default:
                break;
        }
    }

    // Activity was destroyed
    public void onActivityDestroyed() {
        GameStateMachine.State state = sm.getState();
        if (!gameFinished && !(state.equals(GameStateMachine.State.PLAYER_WON) || state.equals(GameStateMachine.State.OPPONENT_WON))) {
            Log.d(TAG, "onActivityDestroyed: ABORT");
            gameFinished = true;
            abortGame();
        }
        else {
            Log.d(TAG, "onActivityDestroyed: No Abort");
        }

    }


    /***********************************************************************************************
     * UI Events (Data from GameManager to GameActivity)
     **********************************************************************************************/

    public MutableLiveData<GameStateMachine.State> getStateChange() {
        return stateChange;
    }

    public String getPlayerNickname() {
        return playerNickname;
    }

    public String getOpponentNickname() {
        return opponentNickname;
    }

    public boolean wasLastShotHit() {
        return lastShotHit;
    }

    public boolean wasLastShotSunk() {
        return lastShotSunk;
    }

    /***********************************************************************************************
     * Callbacks for the socket client
     **********************************************************************************************/

    // Gets called by the Socket Client on a successful connection
    public void onConnected() {
        Log.d(TAG, "onConnected: Socket client CONNECTED");
        sm.fireTransition(GameStateMachine.Trigger.CONNECTED);
    }

    // Gets called by the Socket Client on a connection error
    public void onConnectionError() {
        Log.d(TAG, "onConnectionError: Socket client CONNECTION ERROR");
        sm.fireTransition(GameStateMachine.Trigger.CONNECTION_FAILED);
    }

    // Gets called by the Socket Client whenever a Game State Update was received from the server
    // See readme of the server repo for detailed descriptions of the Game State Updates
    public void onGsuReceived(String state, JSONObject data) throws JSONException {
        switch (state) {

            case "waiting-room":
                if (sm.isInState(GameStateMachine.State.NEW_GAME)) {
                    sm.fireTransition(GameStateMachine.Trigger.GO_TO_WAITING_ROOM);
                }
                break;

            case "game-start":
                if (sm.isInState(GameStateMachine.State.WAITING_ROOM)) {
                    opponentNickname = data.getString("opponentNickname");
                    sm.fireTransition(GameStateMachine.Trigger.GAME_START);
                }
                break;

            case "wait-for-opponent":
                if (sm.isInState(GameStateMachine.State.SHIPS_PLACED)) {
                    sm.fireTransition(GameStateMachine.Trigger.WAIT_FOR_OPPONENT);
                }
                break;

            case "players-ready":
                if (sm.isInState(GameStateMachine.State.WAITING_FOR_OPPONENT)) {
                    // Get whose turn it is
                    myTurn = data.getBoolean("firstDraw");
                    Log.d(TAG, "onGsuReceived: First Draw: " + (myTurn ? "Me" : "Opponent"));
                    // Fire corresponding transition
                    if (myTurn) {
                        sm.fireTransition(GameStateMachine.Trigger.PLAYER_TURN);
                    } else {
                        sm.fireTransition(GameStateMachine.Trigger.OPPONENT_TURN);
                    }
                }
                break;

            case "game-finished":
                // Get winner's Nickname
                String winner = data.getString("winner");
                playerWon = winner.equals(playerNickname);
                sm.fireTransition(GameStateMachine.Trigger.GAME_FINISHED);
                break;

            case "game-terminated":
                if (data.getString("reason").equals("terminated")) {
                    sm.fireTransition(GameStateMachine.Trigger.OPPONEND_TERMINATE);
                }
                break;

            default:
                break;
        }
    }

    // Gets called by the Socket Client whenever a Game Message was received from the opponent
    // See readme of the server repo for detailed descriptions of the Game Messages
    public void onGameMessageReceived(String action, JSONObject data) throws JSONException {
        // ToDo: Ist hier, weil es manchmal crasht wenn eine SM zweimal empfangen wird (warum passiert das? Sendet server 2 mal?)
        Log.d(TAG, "onGameMessageReceived: GM RECEIVED: " + action + ", Random: " + Math.random());
        switch (action) {
            case "answer":
                if (sm.isInState(GameStateMachine.State.PLAYER_FIRING)) {
                    // Store answer and fire transition
                    lastShotHit = data.getBoolean("hit");
                    lastShotSunk = data.getBoolean("sunk");
                    sm.fireTransition(GameStateMachine.Trigger.OPPONENT_ANSWERED);
                }
                break;

            case "shot":
                if (sm.isInState(GameStateMachine.State.OPPONENTS_TURN)) {
                    // Get the coordinates and fire the transition
                    JSONObject coords = data.getJSONObject("coordinates");
                    lastCoords = new int[]{
                            coords.getInt("x"),
                            coords.getInt("y")
                    };
                    sm.fireTransition(GameStateMachine.Trigger.OPPONENT_FIRE);
                }
                break;

            default:
                break;
        }
    }



    /***********************************************************************************************
     * Lifecycle Methods
     **********************************************************************************************/

    // GameManager-ViewModel gets cleared
    @Override
    protected void onCleared() {
        super.onCleared();
        Log.d(TAG, "onCleared: GAME MANAGER CLEARED");
        GameStateMachine.State state = sm.getState();
        if (!gameFinished && !(state.equals(GameStateMachine.State.PLAYER_WON) || state.equals(GameStateMachine.State.OPPONENT_WON))) {
            Log.d(TAG, "onCleared: ABORT");
            gameFinished = true;
            abortGame();
        }
        else {
            Log.d(TAG, "onCleared: No Abort");
        }

        // Disconnect from the server
        socket.disconnect();
    }


    /***********************************************************************************************
     * Helper Methods
     **********************************************************************************************/

    private void abortGame() {
        // Assemble and send the game state update
        try {
            JSONObject data = new JSONObject();
            data.put("reason", "terminated");
            socket.sendGSU("game-terminated", data);
        } catch (JSONException e) {
            Log.d(TAG, "abortGame: " + e);
            e.printStackTrace();
        }
    }

}
