package com.mariusapps.awe_battleships;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Arrays;
import java.util.ArrayList;

// Representation of the play field (where ships are, where water, ect...)
public class PlayFieldData extends ViewModel {

    private static final String TAG = "AWE_Bs.PlayFieldData";

    // Store the play field size
    private int size;

    // Store all ships in the play field
    private ArrayList<Ship> ships = new ArrayList<>();

    // Store whether a drag is currently going on ot not
    boolean dragging = false;
    Ship draggingShip;

    // ToDo: Remove. For debug only
    private String name = "";

    // Representation of the play field is an array of integers for now
    // ToDo: Create Ship class and change this to an ArrayList<Ship>
    private MutableLiveData<PlayFieldCell[][]> playFieldData;


    /***********************************************************************************************
     * Constructor, Initialization
     **********************************************************************************************/

    public PlayFieldData() {
        Log.d(TAG, "PlayFieldData: Constructor");
    }

    // Initializes the play field with a certain size. Not in Construction because this class
    // is a View Model
    public void initialize(int size) {
        this.size = size;

        playFieldData = new MutableLiveData<>();
        playFieldData.setValue(shipsToPlayField());
    }

    public void initializeShips() {
        // Place initial ships
        placeNewShip(0, 0, 4, false);
        placeNewShip(5, 0, 3, false);
        placeNewShip(0, 2, 3, false);
        placeNewShip(4, 2, 2, false);
        placeNewShip(7, 2, 2, false);
        placeNewShip(0, 4, 2, false);
        placeNewShip(3, 4, 1, false);
        placeNewShip(5, 4, 1, false);
        placeNewShip(7, 4, 1, false);
        placeNewShip(9, 4, 1, false);

//        placeNewShip(0, 0, 5, false);
//        placeNewShip(6, 0, 4, false);
//        placeNewShip(0, 2, 4, false);
//        placeNewShip(5, 2, 3, false);
//        placeNewShip(8, 2, 3, false);
//        placeNewShip(0, 4, 3, false);
//        placeNewShip(4, 4, 2, false);
//        placeNewShip(7, 4, 2, false);
//        placeNewShip(0, 6, 2, false);
//        placeNewShip(4, 6, 2, false);


        playFieldData.setValue(shipsToPlayField());
    }


    /***********************************************************************************************
     * Ships manipulation
     **********************************************************************************************/

    // Places a new ship in the play field. If the ship does not fit the play field fully,
    // nothing is done
    private void placeNewShip(int x, int y, int shipSize, boolean goesDown) {
        // Calculate if ship is within the play field
        if ((goesDown && y + shipSize <= size) || (!goesDown && x + shipSize <= size)) {
            Ship ship = new Ship(x, y, shipSize, goesDown);
            ships.add(ship);
        }
    }

    // Takes a list of ships and assembles a Play Field Cell array of it
    private PlayFieldCell[][] shipsToPlayField() {
        PlayFieldCell[][] cells = new PlayFieldCell[size][size];
        // Fill with water
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                cells[x][y] = new Water(x, y);
            }
        }
        // Replace some water with Ship Parts
        for (Ship ship : ships) {
            for (ShipPart part : ship.getShipParts()) {
                cells[part.getX()][part.getY()] = part;
            }
        }
        return cells;
    }

    // Tries to rotate a ship at a certain coordinate. Does nothing if a rotation is not possible
    public void rotateShipAt(int x, int y) {
        Log.d(TAG, "rotateShipAt: " + x + "/" + y);
        // Search the ship belonging to that coordinate
        for (Ship ship : ships) {
            Log.d(TAG, "rotateShipAt: Looking at  " + ship + "  -> is at " + x + "/" + y + ": " + ship.isAtCoord(x, y));
            if (ship.isAtCoord(x, y)) {
                ship.rotateAt(x, y, size);
            }
        }
        playFieldData.setValue(shipsToPlayField());
    }

    // Tries to move the currently dragged ship from "from" coordinates to "to" coordinates. If the
    // motion is not inside the play field, nothing is done
    public void drag(int fromX, int fromY, int toX, int toY) {
        if (dragging && draggingShip != null) {
            draggingShip.moveAt(fromX, fromY, toX, toY, size);
            playFieldData.setValue(shipsToPlayField());
        }
    }

    // Returns whether a ship is currently being dragged or not
    public boolean isDragging() {
        return dragging;
    }

    // Start a drag
    public void startDragging(int startX, int startY) {
        if (!dragging) {
            dragging = true;
            for (Ship ship : ships) {
                if (ship.isAtCoord(startX, startY)) {
                    draggingShip = ship;
                    break;
                }
            }
        }
    }

    // Stop a drag
    public void stopDragging() {
        dragging = false;
    }

    // Get the ship at a certain coordinate
    public Ship getShipAt(int x, int y) {
        for (Ship ship : ships) {
            if (ship.isAtCoord(x, y)) return ship;
        }
        return  null;
    }

    // Returns all ships
    public ArrayList<Ship> getShips() {
        return ships;
    }

    /***********************************************************************************************
     * Cell manipulation
     **********************************************************************************************/

    // Mark a certain cell as hit
    public void setCellHit(int x, int y) {
        PlayFieldCell[][] data = playFieldData.getValue();
        if (data != null && isInField(x, y, data.length)) {
            data[x][y].setHit();
        }
        playFieldData.postValue(data);
    }

    // Toggles the hit state of a cell
    public void toggleCellHit(int x, int y) {
        PlayFieldCell[][] data = playFieldData.getValue();
        if (data != null && isInField(x, y, data.length)) {
            if (data[x][y].wasHit()) {
                data[x][y].resetHit();
            }
            else {
                data[x][y].setHit();
            }
        }
        playFieldData.postValue(data);
    }

    // Returns whether a certain cell is a ship part or not
    public boolean isShipAt(int x, int y) {
        PlayFieldCell[][] data = playFieldData.getValue();
        // Check coordinates
        if (data != null && isInField(x, y, data.length)) {
            return data[x][y] instanceof ShipPart;
        }
        return false;
    }

    // Idea: Eine Klasse PossibleShipPart erstellen und diese dem spielfeld hinzufügen bei hit.
    //       Bei versenkt alle benachbarten PossibleShipParts entfernen und mit regulärem Ship ersetzen.
    public void unveilCellAsShipPart(int x, int y) {
        PlayFieldCell[][] data = playFieldData.getValue();
        if (data != null) {
            data[x][y] = new ShipPart(x, y, ShipPart.MIDDLE);
        }
        playFieldData.postValue(data);

    }

    // Returns a boolean, true if all ship parts of all ships were hit and false otherwise
    boolean allShipPartsHit() {
        PlayFieldCell[][] data = playFieldData.getValue();
        if (data != null) {
            // Search for a ship part which was not yet hit
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data.length; x++) {
                    if (data[x][y] instanceof ShipPart && !data[x][y].wasHit()) {
                        // Found a ship part which was not yet hit
                        return false;
                    }
                }
            }
            // No ship part found which was not yet hit
            return true;
        }
        // Data is null
        return false;
    }


    /***********************************************************************************************
     * Crosshair
     **********************************************************************************************/

    // Sets the crosshair to a certain cell and removes it from all others
    public void setCrosshairTo(int x, int y) {
        // Remove crosshairs
        PlayFieldCell[][] data = playFieldData.getValue();
        if (data != null) {
            removeCrosshair(data);
            data[x][y].setCrosshair();
            playFieldData.postValue(data);
        }
    }

    // Removes the crosshair of all cells
    public void removeCrosshair() {
        PlayFieldCell[][] data = playFieldData.getValue();
        if (data != null) {
            removeCrosshair(data);
            playFieldData.postValue(data);
        }
    }

    // Removes the crosshair of all cells
    private void removeCrosshair(PlayFieldCell[][] data) {
        for (int cellY = 0; cellY < data.length; cellY++) {
            for (int cellX = 0; cellX < data.length; cellX++) {
                data[cellX][cellY].resetCrosshair();
            }
        }
    }

    // Returns the coordinates of the current crosshair
    public int[] getCrosshairCoords() {
        PlayFieldCell[][] data = playFieldData.getValue();
        if (data != null) {
            for (int cellY = 0; cellY < data.length; cellY++) {
                for (int cellX = 0; cellX < data.length; cellX++) {
                    if (data[cellX][cellY].hasCrosshair()) {
                        return new int[] {cellX, cellY};
                    }
                }
            }
        }
        return null;
    }

    /***********************************************************************************************
     * Ships placement check
     **********************************************************************************************/
    public ArrayList<Ship> testShips() {
        PlayFieldCell[][] data = playFieldData.getValue();
        ArrayList<Ship> invalidShips = new ArrayList<>();
        for (Ship ship : ships) {
            // Get ship information
            int x = ship.getX();
            int y = ship.getY();
            int shipSize = ship.getSize();
            Log.d(TAG, "test: ships coordinates: x; y; shipSize: " + x +"; " + y+"; "+shipSize );
            // Calculate all neighbour cells
            int[] xCells = new int[(2 * shipSize + 6)];
            int[] yCells = new int[(2 * shipSize + 6)];
            int i = 0; //Cell counter
            if (ship.isGoingDown()) {
                //Upper Side
                for (int j = 0; j < 3; j++) {
                    xCells[i] = x - 1 + j;
                    yCells[i] = y - 1;
                    i++;
                }
                //Left Side
                for (int j = 0; j < shipSize; j++) {
                    xCells[i] = x - 1;
                    yCells[i] = y + j;
                    i++;
                }
                //Right Side
                for (int j = 0; j < shipSize; j++) {
                    xCells[i] = x + 1;
                    yCells[i] = y + j;
                    i++;
                }
                //Under Side
                for (int j = 0; j < 3; j++) {
                    xCells[i] = x - 1 + j;
                    yCells[i] = y + shipSize;
                    i++;
                }
            } else {
                //Left Side
                for (int j = 0; j < 3; j++) {
                    xCells[i] = x - 1;
                    yCells[i] = y - 1 + j;
                    i++;
                }
                //Upper Side
                for (int j = 0; j < shipSize; j++) {
                    xCells[i] = x + j;
                    yCells[i] = y - 1;
                    i++;
                }
                //Under Side
                for (int j = 0; j < shipSize; j++) {
                    xCells[i] = x + j;
                    yCells[i] = y + 1;
                    i++;
                }
                //Right Side
                for (int j = 0; j < 3; j++) {
                    xCells[i] = x + shipSize;
                    yCells[i] = y - 1 + j;
                    i++;
                }
            }
            Log.d(TAG, "test: neighbor cells: X" + Arrays.toString(xCells));
            Log.d(TAG, "test: neighbor cells: Y" + Arrays.toString(yCells));
            // Check Coordinates, Return false if Cell is in Field and not Water
            for (int j = 0; j < xCells.length; j++) {
                x = xCells[j];
                y = yCells[j];
                if (isInField(x, y,size )) {
                    if (data[x][y] instanceof ShipPart) {
                        invalidShips.add(ship);
                        break;
                    }
                }
            }

        }
        return invalidShips;
}


    /***********************************************************************************************
     * Getters / Setters
     **********************************************************************************************/

    public MutableLiveData<PlayFieldCell[][]> getPlayFieldData() {
        return playFieldData;
    }


    // ToDo: Remove. For debug only
    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.d(TAG, name + " " + "onCleared");
    }


    // Returns true if a coordinate is inside the current play field
    private boolean isInField(int x, int y, int size) {
        return x >= 0 && x < size && y >= 0 && y < size;
    }
}


