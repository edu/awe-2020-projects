package com.mariusapps.awe_battleships;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


/**
 * A simple {@link Fragment} subclass.
 */
public class PlayFieldFragment extends Fragment {

    private static final String TAG = "AWE_Bs.PlayFieldFrag";

    // Size of the play field (number of cells in both dimensions)
    private int playFieldSize = 10;

    // View model of the play field data representation
    private PlayFieldData playFieldData;

    // View for the game field
    private PlayFieldView playFieldView;

    // Temporarily store the OnGameCellClicked callback if it was set before onCreateView finished
    private PlayFieldView.OnCellActionListener tmpOnCellActionListener = null;

    // ToDo: Remove. For debug only
    private String name = "Unnamed";

    public PlayFieldFragment() {
        // Create the play field view
    }

    // ToDo: Remove Name argument. For debug logs only
    public PlayFieldFragment(String name) {
        this.name = name;
    }


    // Only called on start, not on fragment attach -> not called when play field fragments are
    // switched
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, name + " " + "onCreate");

        // Get the play field representation View Model
        playFieldData = new ViewModelProvider(this).get(PlayFieldData.class);
        playFieldData.setName(this.name);

        // Initialize the play field data
        playFieldData.initialize(playFieldSize);
    }

    // Gets called every time the fragment gets attached -> called when play field fragments are
    // switched
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(TAG, name + " " + "onCreateView ");

        // Inflate the layout for this fragment
        View layout =  inflater.inflate(R.layout.fragment_play_field, container, false);

        // Get the context
        Context context = getContext();

        // Get the root linear layout
        LinearLayout root = layout.findViewById(R.id.ll_playfield_layout);

        // Create the play field view and add it to the root layout element
        playFieldView = new PlayFieldView(context);
        root.addView(playFieldView);

        // If setOnCellActionListener was called before this onCreateView finished, set it now
        // from the temporary field. Additionally, when the play field fragments get exchanged,
        // the playFieldView gets re-instantiated but the fragment survives. Thus, set the callback
        // again on the new PlayFieldView instance
        if (tmpOnCellActionListener != null) {
            playFieldView.setOnCellActionListener(tmpOnCellActionListener);
        }


        // Observe the live data of the play field representation
        playFieldData.getPlayFieldData().observe(getViewLifecycleOwner(), new Observer<PlayFieldCell[][]>() {
            @Override
            public void onChanged(PlayFieldCell[][] cellElements) {
                playFieldView.update(cellElements);
            }
        });

        return layout;
    }

    // Initialize the play field with ships
    public void initializeShips() {
        playFieldData.initializeShips();
    }

    // Enables the Game Activity to receive the touch events from the Play Field View
    public void setOnCellActionListener(PlayFieldView.OnCellActionListener listener) {
        if (playFieldView != null) {
            playFieldView.setOnCellActionListener(listener);
        }
        // Save the listener for later, because the view gets re-instantiated  when this fragment
        // gets exchanged, so this listener has to be set again
        tmpOnCellActionListener = listener;
    }

    // Allows the Game Activity to get the Play Field Data
    public PlayFieldData getPlayFieldData() {
        return playFieldData;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, name + " " + "onDestroyView");
        playFieldView.clearBitmaps();
    }




     // Die folgenden Lifecycle - Methoden sind nur für Verständnis- und Debugzwecke eingebaut
     @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG, name + " " + "onAttach");
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, name + " " + "onActivityCreated");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, name + " " + "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, name + " " + "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, name + " " + "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, name + " " + "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, name + " " + "onDestroy");
    }
    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, name + " " + "onDetach");
    }
}
