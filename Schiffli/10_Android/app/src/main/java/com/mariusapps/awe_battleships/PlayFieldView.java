package com.mariusapps.awe_battleships;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class PlayFieldView extends View {

    private static final String TAG = "AWE_Bs.GameFieldView";

    // The images for water and ship parts
    private Bitmap water;
    private Bitmap shipMiddle;
    private Bitmap shipLeft;
    private Bitmap shipRight;
    private Bitmap shipUp;
    private Bitmap shipDown;
    private Bitmap shipRound;
    private Bitmap shipMiddleRed;
    private Bitmap shipLeftRed;
    private Bitmap shipRightRed;
    private Bitmap shipUpRed;
    private Bitmap shipDownRed;
    private Bitmap shipRoundRed;
    private Bitmap crosshair;
    private Bitmap crosshairHit;

    // Store the cell size and startY on draw in order to calculate the touched cell
    private float cellSize = -1;
    private int startY = -1;
    private int startX = -1;

    // Store coordinates for MotionEvent on ACTION_DOWN (start cell of click / drag)
    private int onTouchDownX = -1;
    private int onTouchDownY = -1;

    // Store coordinates for MotionEvent on ACTION_MOVE (last cell touched while moving)
    private int onMoveLastX = -1;
    private int onMoveLastY = -1;

    // Store the play field representation temporarily between the render() and the onDraw() method
    PlayFieldCell[][] data;

    // The callback function (listener) for the onCellTouched event
    OnCellActionListener onCellActionListener;


    // Default constructor
    public PlayFieldView(Context context) {
        super(context);
    }

    // Update the play field data and redraw
    public void update(PlayFieldCell[][] data) {
        this.data = data;
        invalidate();
    }

    // Draw the play field view
    @Override
    protected void onDraw(Canvas canvas) {
        // Calculate the playfield size
        // Get the play field size. It is the smaller of width and height
        int size = Math.min(getWidth(), getHeight());

        // Load the images, if not already loaded
        if (water == null) {
            // Calculate the cell size and load the images
            loadImages(Math.round(size / ((float) data.length)));
        }
        // Render the play field
         renderPlayField(canvas, size);
    }

    // Register touch events on the view and call corresponding event listeners
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // Check if already rendered
        if (cellSize > 0) {

            // Calculate the touched cell
            int x = (int) Math.floor(event.getX() / cellSize);
            int y = (int) Math.floor((event.getY() - startY) / cellSize);

            // Check if it a cell was touched or the border of the view
            if (x >= 0 && x < data.length && y >= 0 && y < data.length) {

                // Switch actions
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // If it was a down event, store the coordinates and fire the event
                        onTouchDownX = x;
                        onTouchDownY = y;
                        onCellTouched(data[x][y]);
                        break;

                    case MotionEvent.ACTION_UP:
                        if (onTouchDownX >= 0 && onTouchDownY >= 0) {
                            // If the up event is on the same cell as the down event, it is a click
                            if (onTouchDownX == x && onTouchDownY == y) {
                                onCellClicked(data[x][y]);
                            }
                            else {
                                // If not, trigger the released event
                                onCellReleased(data[x][y]);
                            }
                        }

                        // Reset drag values
                        onMoveLastX = -1;
                        onMoveLastY = -1;
                        break;

                    case MotionEvent.ACTION_MOVE:
                        // Detect cell change (drag across cell border
                        if (onMoveLastX >= 0 && onMoveLastY >= 0 && (x != onMoveLastX || y != onMoveLastY)) {
                            // Fire the event
                            onDraggedFromTo(data[onMoveLastX][onMoveLastY], data[x][y]);
                        }
                        // Assign new values
                        onMoveLastX = x;
                        onMoveLastY = y;
                        break;

                    default:
                        break;
                }
            }

        }

        return true;
    }


    // Renders the play field with the current data
    private void renderPlayField(Canvas canvas, int playFieldSize) {
        Log.d(TAG, "renderPlayField: DRAW");

//        canvas.drawColor(Color.rgb(8, 169, 236));

        // Get the number of cells by looking at the size of data. Data is always square.
        int nCells = data.length;

        // Get the height and calculate the starting y coordinates in order to center the play field
        // vertically in the view
        startY = (canvas.getHeight() - playFieldSize) / 2;

        // Same for x
        startX = (canvas.getWidth() - playFieldSize) / 2;

        // Calculate the cell size
        cellSize = playFieldSize / ((float) nCells);

        // Draw the cells
        for (int y = 0; y < nCells; y++) {
            for (int x = 0; x < nCells; x++) {

                // Calculate destination rectangle for Bitmaps
                float xPos = startX + x * cellSize;
                float yPos = startY + y * cellSize;
                RectF dst = new RectF(
                        xPos,
                        yPos,
                        xPos + cellSize,
                        yPos + cellSize
                );

                // Draw the water background
                canvas.drawBitmap(water, null, dst, null);

                // Draw the cell element (water, ship part)
                if (data[x][y] instanceof ShipPart) {
                    ShipPart s = (ShipPart) data[x][y];
                    switch (s.getShipPartKind()) {
                        case ShipPart.MIDDLE:
                            canvas.drawBitmap(s.hasInvalidPos() ? shipMiddleRed : shipMiddle, null, dst, null);
                            break;
                        case ShipPart.END_LEFT:
                            canvas.drawBitmap(s.hasInvalidPos() ? shipLeftRed : shipLeft, null, dst, null);
                            break;
                        case ShipPart.END_RIGHT:
                            canvas.drawBitmap(s.hasInvalidPos() ? shipRightRed : shipRight, null, dst, null);
                            break;
                        case ShipPart.END_UP:
                            canvas.drawBitmap(s.hasInvalidPos() ? shipUpRed : shipUp, null, dst, null);
                            break;
                        case ShipPart.END_DOWN:
                            canvas.drawBitmap(s.hasInvalidPos() ? shipDownRed : shipDown, null, dst, null);
                            break;
                        case ShipPart.ROUND:
                            canvas.drawBitmap(s.hasInvalidPos() ? shipRoundRed : shipRound, null, dst, null);
                            break;
                        default:
                            break;
                    }
                }
                // Draw the hit shape if there is no crosshair on the current cell
                if (data[x][y].wasHit() && !data[x][y].hasCrosshair()) {
                    // Prepare paint
                    // ToDo: Move to constructor
                    Paint p = new Paint();
                    p.setColor(Color.RED);
                    p.setStrokeWidth(3);
                    // Calculate cross margin
                    int cm = Math.round(cellSize / 8);
                    // Draw cross
                    canvas.drawLine(xPos + cm, yPos + cm, xPos + cellSize - cm, yPos + cellSize - cm, p);
                    canvas.drawLine(xPos + cellSize - cm, yPos + cm, xPos + cm, yPos + cellSize - cm, p);
                }

                // Draw the crosshair
                if(data[x][y].hasCrosshair()) {
                    if (data[x][y].wasHit()) {
                        canvas.drawBitmap(crosshairHit, null, dst, null);
                    }
                    else {
                        canvas.drawBitmap(crosshair, null, dst, null);
                    }
                }
            }
        }

    }


    // Implementation of a OnCellActionListener event
    interface OnCellActionListener {
        public void onCellTouched(PlayFieldCell cell);
        public void onCellClicked(PlayFieldCell cell);
        public void onCellReleased(PlayFieldCell cell);
        public void onDraggedFromTo(PlayFieldCell from, PlayFieldCell to);
    }

    // Setter for the callback
    public void setOnCellActionListener(OnCellActionListener listener) {
        onCellActionListener = listener;
    }

    // Fire the events
    private void onCellTouched(PlayFieldCell cell) {
        if (onCellActionListener != null) {
            onCellActionListener.onCellTouched(cell);
        }
    }
    private void onCellClicked(PlayFieldCell cell) {
        if (onCellActionListener != null) {
            onCellActionListener.onCellClicked(cell);
        }
    }
    private void onCellReleased(PlayFieldCell cell) {
        if (onCellActionListener != null) {
            onCellActionListener.onCellReleased(cell);
        }
    }
    private void onDraggedFromTo(PlayFieldCell from, PlayFieldCell to) {
        if (onCellActionListener != null) {
            onCellActionListener.onDraggedFromTo(from, to);
        }
    }



    private Bitmap loadBitmap(Resources res, int resId, int sampleSize) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = sampleSize;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    private void loadImages(int size) {
        int sampleSize = calculateInSampleSize(size);
        Resources res = getResources();
        water = loadBitmap(res, R.drawable.water4, sampleSize);
        shipMiddle = loadBitmap(res, R.drawable.ship_middle, sampleSize);
        shipLeft = loadBitmap(res, R.drawable.ship_left, sampleSize);
        shipRight = loadBitmap(res, R.drawable.ship_right, sampleSize);
        shipUp = loadBitmap(res, R.drawable.ship_up, sampleSize);
        shipDown = loadBitmap(res, R.drawable.ship_down, sampleSize);
        shipRound = loadBitmap(res, R.drawable.ship_round, sampleSize);
        shipMiddleRed = loadBitmap(res, R.drawable.ship_middle_red, sampleSize);
        shipLeftRed = loadBitmap(res, R.drawable.ship_left_red, sampleSize);
        shipRightRed = loadBitmap(res, R.drawable.ship_right_red, sampleSize);
        shipUpRed = loadBitmap(res, R.drawable.ship_up_red, sampleSize);
        shipDownRed = loadBitmap(res, R.drawable.ship_down_red, sampleSize);
        shipRoundRed = loadBitmap(res, R.drawable.ship_round_red, sampleSize);
        crosshair = loadBitmap(res, R.drawable.crosshair, sampleSize);
        crosshairHit = loadBitmap(res, R.drawable.crosshair_hit, sampleSize);
    }


    // Zu einem grossen Teil kopiert von
    // https://developer.android.com/topic/performance/graphics/load-bitmap#load-bitmap
    // , angepasst
    public static int calculateInSampleSize(int reqSize) {

        // Assume both a height and width of 500px of the original image
        final int halfOrigSize = 500;
        int inSampleSize = 1;

        if (halfOrigSize * 2 > reqSize) {

            while ((halfOrigSize / inSampleSize) >= reqSize) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    public void clearBitmaps() {
        water.recycle();
        shipMiddle.recycle();
        shipLeft.recycle();
        shipRight.recycle();
        shipUp.recycle();
        shipDown.recycle();
        shipRound.recycle();
        shipMiddleRed.recycle();
        shipLeftRed.recycle();
        shipRightRed.recycle();
        shipUpRed.recycle();
        shipDownRed.recycle();
        shipRoundRed.recycle();
        crosshair.recycle();
        crosshairHit.recycle();
    }




    }
