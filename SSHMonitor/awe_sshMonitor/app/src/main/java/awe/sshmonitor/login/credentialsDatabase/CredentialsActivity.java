package awe.sshmonitor.login.credentialsDatabase;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import awe.sshmonitor.R;

public class CredentialsActivity extends AppCompatActivity implements CredentialsListAdapter.OnCredentialsListener {

    private CredentialsViewModel mCredentialsViewModel;
    private static final String TAG = "SM.CredentialsActivity";
    public static final int LOGIN_ACTIVITY_REQUEST_CODE = 1;
    final static String username = "username";
    final static String target = "target";
    final static String port = "port";
    final static String password = "password";
    private int mSelectedPosition = -1;
    private List<Credentials> mCredentials;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credentials);
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final CredentialsListAdapter adapter = new CredentialsListAdapter(this, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mCredentialsViewModel = new ViewModelProvider(this).get(CredentialsViewModel.class);
        insertNewCredentials(getIntent());
        mCredentialsViewModel.getAllCredentials().observe(this, new Observer<List<Credentials>>() {
            @Override
            public void onChanged(List<Credentials> credentials) {
                adapter.setCredentials(credentials);
                mCredentials = credentials;
            }
        });

        FloatingActionButton selectButton = findViewById(R.id.select_button);
        selectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (0 <= mSelectedPosition) {
                    Credentials requested = mCredentials.get(mSelectedPosition);
                    replyIntent.putExtra(username, requested.getUsername());
                    replyIntent.putExtra(target, requested.getTarget());
                    replyIntent.putExtra(port, requested.getPort());
                    replyIntent.putExtra(password, requested.getPassword());
                    setResult(RESULT_OK, replyIntent);
                } else {
                    setResult(RESULT_CANCELED, replyIntent);
                }
                finish();
            }
        });

        FloatingActionButton deleteButton = findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (0 <= mSelectedPosition) {
                    mCredentialsViewModel.deleteSingleEntry(mCredentials.get(mSelectedPosition));
                    mSelectedPosition = -1;
                } else {
                    Toast.makeText(CredentialsActivity.this, "Nothing selected, nothing deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void insertNewCredentials(Intent data) {
        if (data.hasExtra(username)) { ;
            Credentials credentials = new Credentials(data.getStringExtra(username),
                    data.getStringExtra(target),
                    data.getStringExtra(port),
                    data.getStringExtra(password));
            mCredentialsViewModel.insert(credentials);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LOGIN_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Credentials credentials = new Credentials(data.getStringExtra(username),
                    data.getStringExtra(target),
                    data.getStringExtra(port),
                    data.getStringExtra(password));
            mCredentialsViewModel.insert(credentials);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    "Credentials not saved because they're empty",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onCredentialsClick(int pos) {
        mSelectedPosition = pos;
        Credentials requested = mCredentials.get(mSelectedPosition);
        Toast.makeText(this, requested.getTarget() + " selected", Toast.LENGTH_SHORT).show();
    }
}
