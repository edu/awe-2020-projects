package awe.sshmonitor.login.credentialsDatabase;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "credentials_table")
public class Credentials {
    final static String TAG = "SM.Credentials";
    final static String username = "username";
    final static String target = "target";
    final static String port = "port";
    final static String password = "password";

    @ColumnInfo(name = username)
    private String mUsername;

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = target)
    private String mTarget;

    @ColumnInfo(name = port)
    private String mPort;

    @ColumnInfo(name = password)
    private String mPassword;

    public Credentials(String username, String target, String port, String password) {
        this.mUsername = username;
        this.mTarget = target;
        this.mPort = port;
        this.mPassword = password;
    }

    public void setUsername(@NonNull String mUsername) {
        this.mUsername = mUsername;
    }

    public void setTarget(String mTarget) {
        this.mTarget = mTarget;
    }

    public void setPort(String mPort) {
        this.mPort = mPort;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getUsername() {
        return this.mUsername;
    }

    public String getTarget() {
        return this.mTarget;
    }

    public String getPort() {
        return this.mPort;
    }

    public String getPassword() {
        return this.mPassword;
    }
}
