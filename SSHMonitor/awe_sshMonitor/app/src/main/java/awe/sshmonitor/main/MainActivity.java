package awe.sshmonitor.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONObject;

import awe.sshmonitor.R;
import awe.sshmonitor.main.graph.LiveDataGraphViewModel;
import awe.sshmonitor.main.home.LiveDataHomeViewModel;
import awe.sshmonitor.main.terminal.TerminalViewModel;

import static java.lang.Character.isDigit;

public class MainActivity extends AppCompatActivity
        implements RemoteExecution.Listener {

    public final static String TAG = "SM.MainActivity";
    private JSONObject credentials;
    private String terminalCommand;
    private int counter = 0;
    private final int DELAY = 20000;
    private final int MEMDELAY = 1000;

    public LiveDataGraphViewModel mLiveDataGraphViewModel;
    public LiveDataHomeViewModel mLiveDataHomeViewModel;
    public TerminalViewModel mTerminalViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLiveDataHomeViewModel = new ViewModelProvider(this).get(LiveDataHomeViewModel.class);
        mLiveDataGraphViewModel = new ViewModelProvider(this).get(LiveDataGraphViewModel.class);
        mTerminalViewModel = new ViewModelProvider(this).get(TerminalViewModel.class);
        getCredentialsFromIntent(getIntent());
        BottomNavigationView navView = findViewById(R.id.nav_view);
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);
        setLiveDataHomeViewModelUserName(credentials);
        getHostValues(credentials);
        getMemory(credentials);
    }

    private void getCredentialsFromIntent(Intent intent) {
        try {
            if (intent.hasExtra("json")) {
                this.credentials = new JSONObject(intent.getStringExtra("json"));
            } else {
                Log.d(TAG, "onCreate: ERROR - no JSONObject Extra attached to Intent");
            }
        } catch (Exception e) {
            Log.d(TAG, "onCreate: " + e.getMessage());
        }
    }

    void setLiveDataHomeViewModelUserName(JSONObject credentials) {
        try {
            mLiveDataHomeViewModel.setUsername(credentials.getString("username"));
        } catch (Exception e) {
            Log.d(TAG, "onCreate: " + e.getMessage());
        }
    }

    void getHostValues(final JSONObject credentials) {
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                start(credentials, getResources().getString(R.string.command_hostname));
                start(credentials, getResources().getString(R.string.command_kernelversion));
                start(credentials, getResources().getString(R.string.command_osversion));
                start(credentials, getResources().getString(R.string.command_uptime));
                handler.postDelayed(this, DELAY);
            }
        });
    }

    void getMemory(final JSONObject credentials) {
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                start(credentials, "free -m");
                handler.postDelayed(this, MEMDELAY);
            }
        });
    }

    public void executeCommand(String command) {
        terminalCommand = command;
        start(credentials, command);
    }

    private void start(JSONObject credentials, String command) {
        new RemoteExecution(this, credentials, command);
    }

    public void onDone(String result, String command) {
        result = checkIfStringIsEmpty(result);
        switch (command) {
            case "hostname":
                mLiveDataHomeViewModel.setHostname(result);
                break;
            case "lsb_release -d":
                result = removeDescription(result);
                mLiveDataHomeViewModel.setOsVersion(result);
                break;
            case "uname -sr":
                mLiveDataHomeViewModel.setKernelVersion(result);
                break;
            case "uptime -p":
                mLiveDataHomeViewModel.setUpTime(result);
                break;
            case "free -m":
                int[] memArray = parseMemoryStringToInt(result);
                mLiveDataGraphViewModel.setGraphTotal(memArray[0]);
                mLiveDataGraphViewModel.setGraphUsed(memArray[1]);
                mLiveDataGraphViewModel.setGraphFree(memArray[2]);
                mLiveDataGraphViewModel.setGraphShared(memArray[3]);
                mLiveDataGraphViewModel.setGraphCache(memArray[4]);
                mLiveDataGraphViewModel.setGraphAvailable(memArray[5]);
                break;
            default:
                break;
        }
        if (command.equals(terminalCommand)) {
            mTerminalViewModel.setOutput(result);
        }
    }

    private String removeDescription(String s) {
        try {
            int startIndex = s.indexOf("D");
            int endIndex = s.indexOf(":");
            String replacement = "";
            String toBeReplaced = s.substring(startIndex, endIndex + 1);
            s = s.replace(toBeReplaced, replacement);
        } catch (Exception e) {
            Log.d(TAG, "removeDescription: " + e.getMessage());
        }
        return s;
    }

    private String checkIfStringIsEmpty(String s) {
        if (s.isEmpty()) {
            s = getResources().getString(R.string.no_valid_data);
            return s;
        } else {
            return s;
        }
    }

    private int[] parseMemoryStringToInt(String s) {
        int[] indexes = new int[9];
        int[] arr = new int[6];
        int m = 0;
        s = addCharAfterNumbers(s);
        s = s.replaceAll("[^0-9x]", "");
        for (int i = 0; i <= s.length() - 1; i++) {
            if (s.charAt(i) == 'x') {
                indexes[m] = i;
                m++;
            }
        }
        try {
            arr[0] = Integer.parseInt(s.substring(0, indexes[0]));
            arr[1] = Integer.parseInt(s.substring(indexes[0] + 1, indexes[1]));
            arr[2] = Integer.parseInt(s.substring(indexes[1] + 1, indexes[2]));
            arr[3] = Integer.parseInt(s.substring(indexes[2] + 1, indexes[3]));
            arr[4] = Integer.parseInt(s.substring(indexes[3] + 1, indexes[4]));
            arr[5] = Integer.parseInt(s.substring(indexes[4] + 1, indexes[5]));
            return arr;
        } catch (NumberFormatException nfe) {
            Log.d(TAG, "parseMemoryStringToInt: " + nfe.getMessage());
            for (int n = 0; n < arr.length - 1; n++) {
                arr[n] = 0;
            }
            return null;
        }
    }

    private String addCharAfterNumbers(String s) {
        StringBuilder sb = new StringBuilder(s);
        for (int j = 1; j <= s.length() - 1; j++) {
            if (isDigit(sb.charAt(j - 1)) && !(isDigit(sb.charAt(j)))) {
                sb.setCharAt(j, 'x');
            }
        }
        s = sb.toString();
        return s;
    }
}
