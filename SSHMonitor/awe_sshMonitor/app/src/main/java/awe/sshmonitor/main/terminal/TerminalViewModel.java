package awe.sshmonitor.main.terminal;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TerminalViewModel extends ViewModel {
    public final static String TAG = "SM.SlideshowViewModel";

    private MutableLiveData<String> mCommand = new MutableLiveData<>();
    private MutableLiveData<String> mOutput = new MutableLiveData<>();


    public TerminalViewModel() {
    }

    LiveData<String> getCommand() {
        return mCommand;
    }

    LiveData<String> getOutput() {
        return mOutput;
    }

    public void setCommand(String s) {
        mCommand.setValue(s);
    }

    public void setOutput(String s) {
        mOutput.setValue(s);
    }
}