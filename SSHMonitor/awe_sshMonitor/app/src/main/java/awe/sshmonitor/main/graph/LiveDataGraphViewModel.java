package awe.sshmonitor.main.graph;

import android.graphics.Color;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

public class LiveDataGraphViewModel extends ViewModel {
    public final static String TAG = "SM.GraphViewModel";

    private MutableLiveData<Integer> mGraphTotal = new MutableLiveData<>();
    private MutableLiveData<Integer> mGraphUsed = new MutableLiveData<>();
    private MutableLiveData<Integer> mGraphFree = new MutableLiveData<>();
    private MutableLiveData<Integer> mGraphShared = new MutableLiveData<>();
    private MutableLiveData<Integer> mGraphCache = new MutableLiveData<>();
    private MutableLiveData<Integer> mGraphAvailable = new MutableLiveData<>();
    private MutableLiveData<Integer> mGraphAverage = new MutableLiveData<>();
    private ArrayList<Entry> mGraphFragmentUsedArrayList = new ArrayList<>();
    private LineData mGraphViewModelLineData = new LineData();
    private ArrayList<PieEntry> mGraphViewModelPieArrayList = new ArrayList<>();
    private PieData mGraphViewModelPieData = new PieData();

    public LiveDataGraphViewModel() {
        mGraphViewModelPieArrayList.add(new PieEntry(1, "Free"));
        mGraphViewModelPieArrayList.add(new PieEntry(1, "Shared"));
        mGraphViewModelPieArrayList.add(new PieEntry(1, "Cache"));
        mGraphViewModelPieArrayList.add(new PieEntry(1, "Used"));
    }

    MutableLiveData<Integer> getGraphTotal() {
        return mGraphTotal;
    }

    MutableLiveData<Integer> getGraphUsed() {
        return mGraphUsed;
    }

    MutableLiveData<Integer> getGraphFree() {
        return mGraphFree;
    }

    MutableLiveData<Integer> getGraphShared() {
        return mGraphShared;
    }

    MutableLiveData<Integer> getGraphCache() {
        return mGraphCache;
    }

    MutableLiveData<Integer> getGraphAvailable() {
        return mGraphAvailable;
    }

    MutableLiveData<Integer> getGraphAverage() {
        return mGraphAverage;
    }

    public void setGraphTotal(int i) {
        mGraphTotal.setValue(i);
    }

    public void setGraphUsed(int i) {
        mGraphUsed.setValue(i);
    }

    public void setGraphFree(int i) {
        mGraphFree.setValue(i);
    }

    public void setGraphShared(int i) {
        mGraphShared.setValue(i);
    }

    public void setGraphCache(int i) {
        mGraphCache.setValue(i);
    }

    public void setGraphAvailable(int i) {
        mGraphAvailable.setValue(i);
    }

    private void setGraphAverage(int i) {
        mGraphAverage.setValue(i);
    }

    void addEntryToUsedArrayList(Integer i) {
        Entry entry = new Entry();
        entry.setY(i);
        entry.setX(mGraphFragmentUsedArrayList.size());
        mGraphFragmentUsedArrayList.add(entry);
        int sum = 0;
        int counter = mGraphFragmentUsedArrayList.size();
        if (30 <= mGraphFragmentUsedArrayList.size()) {
            mGraphFragmentUsedArrayList.remove(0);
            for (int m = 0; m <= mGraphFragmentUsedArrayList.size() - 1; m++) {
                mGraphFragmentUsedArrayList.get(m).setX(m);
            }
            counter--;
        }
        for (int m = 0; m <= mGraphFragmentUsedArrayList.size() - 1; m++) {
            sum += mGraphFragmentUsedArrayList.get(m).getY();
        }
        setGraphAverage((sum / counter));
        LineDataSet set = new LineDataSet(mGraphFragmentUsedArrayList, "Current Memory Usage");
        set.setDrawValues(false);
        set.setColor(Color.parseColor("#F87400"));
        set.setCircleColor(Color.parseColor("#FBB900"));
        mGraphViewModelLineData = new LineData(set);
    }

    LineData getGraphViewModelLineData() {
        return mGraphViewModelLineData;
    }

    PieData getGraphViewModelPieData() {
        return mGraphViewModelPieData;
    }

    void calculatePercentages() {
        float sum;
        try {
            sum = mGraphFree.getValue()
                    + mGraphShared.getValue()
                    + mGraphCache.getValue()
                    + mGraphUsed.getValue();
        } catch (NullPointerException npe) {
            Log.d(TAG, "calculatePercentages: " + npe.getMessage());
            sum = 4;
        }
        try {
            mGraphViewModelPieArrayList.set(0,
                    new PieEntry(((100 / sum) * mGraphFree.getValue()), "Free"));
            mGraphViewModelPieArrayList.set(1,
                    new PieEntry(((100 / sum) * mGraphShared.getValue()), "Shared"));
            mGraphViewModelPieArrayList.set(2,
                    new PieEntry(((100 / sum) * mGraphCache.getValue()), "Cache"));
            mGraphViewModelPieArrayList.set(3,
                    new PieEntry(((100 / sum) * mGraphUsed.getValue()), "Used"));
        } catch (NullPointerException npe) {
            Log.d(TAG, "calculatePercentages: " + npe.getMessage());
        }
        PieDataSet set = new PieDataSet(mGraphViewModelPieArrayList, "");
        set.setColors(Color.parseColor("#F87400"),
                Color.parseColor("#FAA000"),
                Color.parseColor("#FBB900"),
                Color.parseColor("#FDE50E"),
                Color.parseColor("#FFF293"));
        mGraphViewModelPieData = new PieData(set);
    }

}