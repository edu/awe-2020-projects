package awe.sshmonitor.main.home;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class LiveDataHomeViewModel extends ViewModel {
    public final static String TAG = "SM.HomeViewModel";

    // Todo: Testing
    private MutableLiveData<String> mUsername = new MutableLiveData<>();
    private MutableLiveData<String> mHostname = new MutableLiveData<>();
    private MutableLiveData<String> mOsVersion = new MutableLiveData<>();
    private MutableLiveData<String> mKernelVersion = new MutableLiveData<>();
    private MutableLiveData<String> mUpTime = new MutableLiveData<>();

    public LiveDataHomeViewModel() {
    }

    LiveData<String> getUsernameLiveData() {
        if (mUsername == null) {
            mUsername = new MutableLiveData<>();
        }
        return mUsername;
    }

    LiveData<String> getHostnameLiveData() {
        if (mHostname == null) {
            mHostname = new MutableLiveData<>();
        }
        return mHostname;
    }

    LiveData<String> getOsVersionLiveData() {
        if (mOsVersion == null) {
            mOsVersion = new MutableLiveData<>();
        }
        return mOsVersion;
    }

    LiveData<String> getKernelVersionLiveData() {
        if (mKernelVersion == null) {
            mKernelVersion = new MutableLiveData<>();
        }
        return mKernelVersion;
    }

    LiveData<String> getUpTimeLiveData() {
        if (mUpTime == null) {
            mUpTime = new MutableLiveData<>();
        }
        return mUpTime;
    }

    public void setUsername(String s) {
        mUsername.setValue(s);
    }

    public void setHostname(String s) {
        mHostname.setValue(s);
    }

    public void setOsVersion(String s) {
        mOsVersion.setValue(s);
    }

    public void setKernelVersion(String s) {
        mKernelVersion.setValue(s);
    }

    public void setUpTime(String s) {
        mUpTime.setValue(s);
    }
}