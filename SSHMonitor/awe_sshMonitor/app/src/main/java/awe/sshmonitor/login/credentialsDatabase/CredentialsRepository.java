package awe.sshmonitor.login.credentialsDatabase;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class CredentialsRepository {

    private CredentialsDao mCredentialsDao;
    private LiveData<List<Credentials>> mAllCredentials;

    CredentialsRepository(Application application) {
        CredentialsRoomDatabase db = CredentialsRoomDatabase.getDatabase(application);
        mCredentialsDao = db.credentialsDao();
        mAllCredentials = mCredentialsDao.getCredentials();
    }

    LiveData<List<Credentials>> getAllCredentials() {
        return mAllCredentials;
    }

    void insert(final Credentials credentials) {
        CredentialsRoomDatabase.databaseWriteExecutor.execute(() -> {
            mCredentialsDao.insert(credentials);
        });
    }

    void deleteSingleEntry(final Credentials credentials) {
        CredentialsRoomDatabase.databaseWriteExecutor.execute(() -> {
            mCredentialsDao.deleteSingleEntry(credentials);
        });
    }
}
