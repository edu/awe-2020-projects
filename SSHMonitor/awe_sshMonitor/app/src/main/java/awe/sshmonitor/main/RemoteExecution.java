package awe.sshmonitor.main;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class RemoteExecution implements Runnable, Handler.Callback {
    public interface Listener {
        void onDone(String result, String command);
    }

    private final static String TAG = "SM.RemoteExecution";
    private String result;
    private String command;
    private JSONObject credentials;
    final private Listener li;
    final private Handler handler;


    public RemoteExecution(Listener li, JSONObject creds, String cmd) {
        this.li = li;
        handler = new Handler(Looper.getMainLooper(), this);
        credentials = creds;
        command = cmd;
        new Thread(this).start();
    }

    public void run() {
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(credentials.getString("username"),
                    credentials.getString("target"),
                    Integer.parseInt(credentials.getString("port")));
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.setPassword(credentials.getString("password"));
            session.connect();
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(System.err);
            InputStream in = channel.getInputStream();
            channel.connect();
            result = copyToString(in, channel);
            channel.disconnect();
            session.disconnect();
        } catch (Exception e) {
            Log.d(TAG, "Exception: " + e.getMessage());
        }
        handler.sendEmptyMessage(0);
    }

    public boolean handleMessage(Message msg) {
        li.onDone(result, command);
        return true;
    }

    private String copyToString(InputStream in, Channel channel) {
        String tmpres = "";
        try {
            byte[] tmp = new byte[1024];
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0) break;
                    tmpres = new String(tmp, 0, i);
                }
                if (channel.isClosed()) {
                    if (in.available() > 0) continue;
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception ee) {
                    Log.d(TAG, "doInBackground: " + ee.getMessage());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tmpres;
    }
}
