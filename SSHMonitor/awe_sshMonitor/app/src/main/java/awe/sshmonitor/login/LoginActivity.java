package awe.sshmonitor.login;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import awe.sshmonitor.R;
import awe.sshmonitor.login.credentialsDatabase.CredentialsActivity;
import awe.sshmonitor.main.MainActivity;

public class LoginActivity extends AppCompatActivity
        implements LoginCheck.Listener {

    final static String TAG = "SM.LoginActivity: ";
    final static String username = "username";
    final static String target = "target";
    final static String port = "port";
    final static String password = "password";
    public static final int CREDENTIALS_ACTIVITY_SAVE_REQUEST_CODE = 1;
    public static final int CREDENTIALS_ACTIVITY_LOAD_REQUEST_CODE = 2;
    EditText usernameEditText;
    EditText targetEditText;
    EditText portEditText;
    EditText passwordEditText;
    private ProgressBar loadingProgressBar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final SharedPreferences sharedPref;
        sharedPref = LoginActivity.this.getSharedPreferences(
                getResources().getString(R.string.settings), Context.MODE_PRIVATE);
        final SharedPreferences.Editor editorSettings = sharedPref.edit();

        if (sharedPref.getBoolean(getResources().getString(R.string.security_accepted), true)) {
            showSecurityWarning();
        }

        usernameEditText = findViewById(R.id.username);
        targetEditText = findViewById(R.id.target);
        portEditText = findViewById(R.id.portnumber);
        passwordEditText = findViewById(R.id.password);
        final Button saveButton = findViewById(R.id.button_save);
        final Button loadButton = findViewById(R.id.button_load);
        final Button loginButton = findViewById(R.id.button_login);
        loadingProgressBar = findViewById(R.id.loading);

        //       getCredentialsFromIntent(getIntent());

        final JSONObject credentials = new JSONObject();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                closeKeyboard();
                try {
                    credentials.put(username, usernameEditText.getText());
                    credentials.put(target, targetEditText.getText());
                    credentials.put(port, portEditText.getText());
                    credentials.put(password, passwordEditText.getText());
                    // execute loginCheck with the given credentials
                    start(credentials);
                } catch (JSONException e) {
                    Log.d(TAG, "saveButton.onClick: " + e.getMessage());
                }
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard();
                if (TextUtils.isEmpty(targetEditText.getText().toString())) {
                    Toast.makeText(LoginActivity.this, "Please enter Credentials", Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(LoginActivity.this, CredentialsActivity.class);
                    intent.putExtra(username, usernameEditText.getText().toString());
                    intent.putExtra(target, targetEditText.getText().toString());
                    intent.putExtra(port, portEditText.getText().toString());
                    intent.putExtra(password, passwordEditText.getText().toString());
                    startActivityForResult(intent, CREDENTIALS_ACTIVITY_SAVE_REQUEST_CODE);
                }
            }
        });

        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard();
                startActivityForResult(new Intent(LoginActivity.this, CredentialsActivity.class), CREDENTIALS_ACTIVITY_LOAD_REQUEST_CODE);
            }
        });
    }

    public void onDone(boolean result, JSONObject credentials, String answer) {
        loadingProgressBar.setVisibility(View.INVISIBLE);
        if (result) {
            loginUser(credentials);
        } else {
            showLoginFailed(answer);
        }
    }

    private void loginUser(JSONObject credentials) {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra("json", credentials.toString());
        LoginActivity.this.startActivity(intent);
        Toast.makeText(this, "Login successful", Toast.LENGTH_SHORT).show();
    }

    public void start(JSONObject json) {
        new LoginCheck(this, json);
    }

    private void showLoginFailed(String answer) {
        Bundle bundle = new Bundle();
        bundle.putString("answer", answer);
        DialogFragment loginFailedFragment = new LoginFailedDialogFragment();
        loginFailedFragment.setArguments(bundle);
        loginFailedFragment.show(getSupportFragmentManager(), "loginfailed");
    }

    private void showSecurityWarning() {
        DialogFragment securityWarningFragment = new SecurityWarningDialogFragment();
        securityWarningFragment.show(getSupportFragmentManager(), "securityWarning");
    }

    private void closeKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException npe) {
            Log.d(TAG, "onClick: noinputfound" + npe.getMessage());
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CREDENTIALS_ACTIVITY_SAVE_REQUEST_CODE && resultCode == RESULT_OK) {
            Toast.makeText(this, "Credentials saved to Database", Toast.LENGTH_SHORT).show();
        }
        if (requestCode == CREDENTIALS_ACTIVITY_LOAD_REQUEST_CODE && resultCode == RESULT_OK) {
            getCredentialsFromIntent(data);
        } else {
            Toast.makeText(this, "No credentials loaded", Toast.LENGTH_SHORT).show();
        }
    }

    private void getCredentialsFromIntent(Intent intent) {
        try {
            if (intent.hasExtra(username)) {
                usernameEditText.setText(intent.getStringExtra(username));
                targetEditText.setText(intent.getStringExtra(target));
                portEditText.setText(intent.getStringExtra(port));
                passwordEditText.setText(intent.getStringExtra(password));
            } else {
                Log.d(TAG, "onCreate: ERROR - no JSONObject Extra attached to Intent");
            }
        } catch (Exception e) {
            Log.d(TAG, "onCreate: " + e.getMessage());
        }
    }
}
