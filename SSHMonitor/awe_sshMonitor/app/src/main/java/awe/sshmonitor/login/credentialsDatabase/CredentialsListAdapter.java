package awe.sshmonitor.login.credentialsDatabase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import awe.sshmonitor.R;

public class CredentialsListAdapter extends RecyclerView.Adapter<CredentialsListAdapter.CredentialsViewHolder> {

    private static final String TAG = "SM.cla";
    private OnCredentialsListener mOnCredentialsListener;
    private final LayoutInflater mInflater;
    private List<Credentials> mCredentials; // cached list for credentials

    class CredentialsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView credentialsViewUsername;
        private final TextView credentialsViewTarget;
        private final TextView credentialsViewPort;
        OnCredentialsListener onCredentialsListener;

        private CredentialsViewHolder(View itemView, OnCredentialsListener onCredentialsListener) {
            super(itemView);
            credentialsViewUsername = itemView.findViewById(R.id.recyclerview_username_dynamic);
            credentialsViewTarget = itemView.findViewById(R.id.recyclerview_target_dynamic);
            credentialsViewPort = itemView.findViewById(R.id.recyclerview_port_dynamic);
            this.onCredentialsListener = onCredentialsListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onCredentialsListener.onCredentialsClick(getAdapterPosition());
        }
    }

    public interface OnCredentialsListener {
        void onCredentialsClick(int pos);
    }

    CredentialsListAdapter(Context context, OnCredentialsListener onCredentialsListener) {
        mInflater = LayoutInflater.from(context);
        this.mOnCredentialsListener = onCredentialsListener;
    }

    @Override
    public CredentialsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_credentials_item,
                parent, false);
        return new CredentialsViewHolder(itemView, mOnCredentialsListener);
    }

    @Override
    public void onBindViewHolder(CredentialsViewHolder holder, int position) {
        if (mCredentials != null) {
            Credentials current = mCredentials.get(position);
            holder.credentialsViewUsername.setText(current.getUsername());
            holder.credentialsViewTarget.setText(current.getTarget());
            holder.credentialsViewPort.setText(current.getPort());
        } else {
            holder.credentialsViewUsername.setText("No Value");
            holder.credentialsViewTarget.setText("No Value");
            holder.credentialsViewPort.setText("No Value");
        }
    }

    void setCredentials(List<Credentials> credentials) {
        mCredentials = credentials;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mCredentials != null)
            return mCredentials.size();
        else return 0;
    }
}
