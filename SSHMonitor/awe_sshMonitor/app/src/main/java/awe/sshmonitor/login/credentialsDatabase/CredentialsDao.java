package awe.sshmonitor.login.credentialsDatabase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CredentialsDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Credentials credentials);

    @Query("DELETE FROM credentials_table")
    void deleteAll();

    @Delete
    void deleteSingleEntry(Credentials credentials);

    @Query("SELECT * from credentials_table")
    LiveData<List<Credentials>> getCredentials();

}
