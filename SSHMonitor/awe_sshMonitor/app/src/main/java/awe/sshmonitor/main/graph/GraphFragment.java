package awe.sshmonitor.main.graph;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.formatter.PercentFormatter;

import awe.sshmonitor.R;
import awe.sshmonitor.main.MainActivity;

public class GraphFragment extends Fragment {
    public final static String TAG = "SM.GraphFragment";

    private LiveDataGraphViewModel mLiveDataGraphViewModel;
    private LineChart mLineChart;
    private PieChart mPieChart;
    private TextView graphTotal;
    private TextView graphAverage;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        MainActivity activity = (MainActivity) getActivity();
        this.mLiveDataGraphViewModel = activity.mLiveDataGraphViewModel;
        View root = inflater.inflate(R.layout.fragment_graph, container, false);
        mLineChart = root.findViewById(R.id.memory_line_chart);
        mPieChart = root.findViewById(R.id.memory_pie_chart);
        graphTotal = root.findViewById(R.id.graph_total);
        graphAverage = root.findViewById(R.id.graph_average);
        setupLineChart();
        setupPieChart();
        subscribe();
        return root;
    }

    private void subscribe() {
        final Observer<Integer> totalObserver = new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                String s = integer.toString();
                graphTotal.setText(s);
            }
        };
        final Observer<Integer> usedObserver = new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                mLiveDataGraphViewModel.addEntryToUsedArrayList(integer);
                mLineChart.setData(mLiveDataGraphViewModel.getGraphViewModelLineData());
                mLineChart.invalidate();
            }
        };
        final Observer<Integer> freeObserver = new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                mLiveDataGraphViewModel.calculatePercentages();
                PieData data = mLiveDataGraphViewModel.getGraphViewModelPieData();
                data.setValueFormatter(new PercentFormatter(mPieChart));
                mPieChart.setData(data);
                mPieChart.invalidate();
            }
        };
        final Observer<Integer> sharedObserver = new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {

            }
        };
        final Observer<Integer> cacheObserver = new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {

            }
        };
        final Observer<Integer> availableObserver = new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {

            }
        };
        final Observer<Integer> averageObserver = new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                String s = integer.toString();
                graphAverage.setText(s);
            }
        };

        mLiveDataGraphViewModel.getGraphTotal().observe(getViewLifecycleOwner(), totalObserver);
        mLiveDataGraphViewModel.getGraphUsed().observe(getViewLifecycleOwner(), usedObserver);
        mLiveDataGraphViewModel.getGraphFree().observe(getViewLifecycleOwner(), freeObserver);
        mLiveDataGraphViewModel.getGraphShared().observe(getViewLifecycleOwner(), sharedObserver);
        mLiveDataGraphViewModel.getGraphCache().observe(getViewLifecycleOwner(), cacheObserver);
        mLiveDataGraphViewModel.getGraphAvailable().observe(getViewLifecycleOwner(), availableObserver);
        mLiveDataGraphViewModel.getGraphAverage().observe(getViewLifecycleOwner(), averageObserver);
    }

    private void setupLineChart() {
        mLineChart.setDrawGridBackground(false);
        mLineChart.getDescription().setEnabled(false);
        mLineChart.setDrawBorders(true);
        mLineChart.setDragEnabled(false);
        mLineChart.setScaleEnabled(false);
        mLineChart.setPinchZoom(false);
        mLineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mLineChart.getXAxis().setEnabled(true);
    }

    private void setupPieChart() {
        mPieChart.getDescription().setEnabled(false);
        mPieChart.setRotationEnabled(false);
        mPieChart.setUsePercentValues(true);
        mPieChart.setDrawEntryLabels(false);
        mPieChart.setUsePercentValues(true);

        Legend l = mPieChart.getLegend();
        l.setFormSize(10f); // set the size of the legend forms/shapes
        l.setForm(Legend.LegendForm.CIRCLE); // set what type of form/shape should be used
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setTypeface(Typeface.MONOSPACE);
        l.setTextSize(12f);
        l.setTextColor(Color.BLACK);
        l.setXEntrySpace(5f); // space between the legend entries on the x-axis
        l.setYEntrySpace(5f); // space between the legend entries on the y-axis
    }
}
