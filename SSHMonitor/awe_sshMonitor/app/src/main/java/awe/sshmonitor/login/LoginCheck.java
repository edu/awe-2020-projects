package awe.sshmonitor.login;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class LoginCheck implements Runnable, Handler.Callback {
    public final static String TAG = "SM.LoginCheck";

    interface Listener {
        void onDone(boolean result, JSONObject creds, String answer);
    }

    boolean result;
    private String answer;
    final private Listener li;
    final private Handler handler;
    private JSONObject credentials;

    LoginCheck(Listener li, JSONObject args) {
        this.li = li;
        handler = new Handler(Looper.getMainLooper(), this);
        credentials = args;
        new Thread(this).start();
    }

    public void run() {
        try {
            Thread.sleep(1000);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(credentials.getString("username"),
                    credentials.getString("target"),
                    Integer.parseInt(credentials.getString("port")));
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.setPassword(credentials.getString("password"));
            session.connect();
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand("whoami");
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(System.err);
            InputStream in = channel.getInputStream();
            channel.connect();
            answer = copyToString(in, channel);
            channel.disconnect();
            session.disconnect();
        } catch (Exception e) {
            answer = e.getMessage();
            Log.d(TAG, "Exception: " + e.getMessage());
        }
        handler.sendEmptyMessage(0);
    }

    public boolean handleMessage(Message msg) {
        result = checkLogin(answer);
        li.onDone(result, credentials, answer);
        return true;
    }

    private boolean checkLogin(String answer) {
        try {
            if (answer != null && answer.length() > 0) {
                answer = answer.substring(0, answer.length() - 1);
            }
            return answer.equals(credentials.getString("username"));
        } catch (Exception e) {
            Log.d(TAG, "checkLogin: " + e.getMessage());
            return false;
        }
    }

    private String copyToString(InputStream in, Channel channel) {
        String tmpres = "";
        try {
            byte[] tmp = new byte[1024];
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0) break;
                    tmpres = new String(tmp, 0, i);
                }
                if (channel.isClosed()) {
                    if (in.available() > 0) continue;
                    Log.d(TAG, ("exit-status:" + channel.getExitStatus()));
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception ee) {
                    Log.d(TAG, "doInBackground: " + ee.getMessage());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tmpres;
    }
}
