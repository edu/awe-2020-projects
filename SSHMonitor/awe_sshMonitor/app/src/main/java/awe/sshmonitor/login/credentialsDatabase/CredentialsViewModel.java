package awe.sshmonitor.login.credentialsDatabase;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class CredentialsViewModel extends AndroidViewModel {

    private CredentialsRepository mRepository;
    private LiveData<List<Credentials>> mAllCredentials;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String username;
    private String target;
    private String port;
    private String password;

    public CredentialsViewModel(Application application) {
        super(application);
        mRepository = new CredentialsRepository(application);
        mAllCredentials = mRepository.getAllCredentials();
    }

    LiveData<List<Credentials>> getAllCredentials() {
        return mAllCredentials;
    }

    public void insert(Credentials credentials) {
        mRepository.insert(credentials);
    }

    public void deleteSingleEntry(Credentials credentials) {
        mRepository.deleteSingleEntry(credentials);
    }
}
