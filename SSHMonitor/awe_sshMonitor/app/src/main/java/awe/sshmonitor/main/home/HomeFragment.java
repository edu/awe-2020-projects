package awe.sshmonitor.main.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import awe.sshmonitor.R;
import awe.sshmonitor.main.MainActivity;

public class HomeFragment extends Fragment {

    public final static String TAG = "SM.HomeFragment";

    private LiveDataHomeViewModel mLiveDataHomeViewModel;
    private TextView HomeUserName;
    private TextView HomeTargetName;
    private TextView HomeOsVersion;
    private TextView HomeKernelVersion;
    private TextView HomeUptime;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        MainActivity activity = (MainActivity) getActivity();
        this.mLiveDataHomeViewModel = activity.mLiveDataHomeViewModel;
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        HomeUserName = root.findViewById(R.id.homeusername);
        HomeTargetName = root.findViewById(R.id.hometargetname);
        HomeOsVersion = root.findViewById(R.id.homeosversion);
        HomeKernelVersion = root.findViewById(R.id.homekernelversion);
        HomeUptime = root.findViewById(R.id.homeuptime);
        subscribe();
        return root;
    }

    private void subscribe() {
        final Observer<String> usernameObserver = new Observer<String>() {
            @Override
            public void onChanged(String s) {
                HomeUserName.setText(s);
            }
        };
        final Observer<String> hostnameObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String s) {
                HomeTargetName.setText(s);
            }
        };
        final Observer<String> osVersionObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String s) {
                HomeOsVersion.setText(s);
            }
        };
        final Observer<String> kernelVersionObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String s) {
                HomeKernelVersion.setText(s);
            }
        };
        final Observer<String> upTimeObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String s) {
                HomeUptime.setText(s);
            }
        };

        mLiveDataHomeViewModel.getUsernameLiveData().observe(getViewLifecycleOwner(), usernameObserver);
        mLiveDataHomeViewModel.getHostnameLiveData().observe(getViewLifecycleOwner(), hostnameObserver);
        mLiveDataHomeViewModel.getOsVersionLiveData().observe(getViewLifecycleOwner(), osVersionObserver);
        mLiveDataHomeViewModel.getKernelVersionLiveData().observe(getViewLifecycleOwner(), kernelVersionObserver);
        mLiveDataHomeViewModel.getUpTimeLiveData().observe(getViewLifecycleOwner(), upTimeObserver);
    }

}
