package awe.sshmonitor.main.terminal;

import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import awe.sshmonitor.R;
import awe.sshmonitor.main.MainActivity;

public class TerminalFragment extends Fragment {
    public final static String TAG = "SM.SlideshowFragment";

    private TerminalViewModel mTerminalViewModel;
    private EditText mTerminalCommandField;
    private TextView mTerminalOutput;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        final MainActivity activity = (MainActivity) getActivity();
        this.mTerminalViewModel = activity.mTerminalViewModel;
        View root = inflater.inflate(R.layout.fragment_terminal, container, false);
        mTerminalCommandField = root.findViewById(R.id.terminal_command_field);
        mTerminalOutput = root.findViewById(R.id.terminal_output);
        mTerminalOutput.setMovementMethod(new ScrollingMovementMethod());
        final Button mExecuteButton = root.findViewById(R.id.button_execute);
        subscribe();

        mExecuteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String command = mTerminalCommandField.getText().toString();
                activity.executeCommand(command);
                try {
                    InputMethodManager inputManager = (InputMethodManager)
                            activity.getSystemService(Context.INPUT_METHOD_SERVICE);

                    inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                } catch (NullPointerException npe) {
                    Log.d(TAG, "onClick: " + npe.getMessage());
                }
            }
        });
        return root;
    }

    private void subscribe() {
        final Observer<String> outputObserver = new Observer<String>() {
            @Override
            public void onChanged(String s) {
                mTerminalOutput.setText(s);
            }
        };
        mTerminalViewModel.getOutput().observe(getViewLifecycleOwner(), outputObserver);
    }
}
