package awe.sshmonitor.login;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.DialogFragment;

import awe.sshmonitor.R;

public class SecurityWarningDialogFragment extends DialogFragment {
    final static String TAG = "SM.SecurityWarning";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final SharedPreferences sharedPref = getActivity().getSharedPreferences(
                getString(R.string.settings), Context.MODE_PRIVATE);
        final SharedPreferences.Editor editorSettings = sharedPref.edit();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.security_warning);
        builder.setMessage(R.string.lorem_ipsum);
        builder.setPositiveButton(R.string.accept_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editorSettings.putBoolean(getResources().getString(R.string.security_accepted), false);
                editorSettings.apply();
            }
        });
        return builder.create();
    }
}
