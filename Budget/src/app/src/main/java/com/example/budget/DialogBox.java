package com.example.budget;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

public class DialogBox extends AppCompatDialogFragment {
    private EditText editTextItemname;
    private EditText editTextItemvalue;
    private  DialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View myView = inflater.inflate(R.layout.layout_dialog_box, null);

        builder.setView(myView)
                .setTitle("Create Item")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String itemName = editTextItemname.getText().toString();
                        String itemValue = editTextItemvalue.getText().toString();
                        listener.applyTexts(itemName, itemValue);
                    }
                });

        editTextItemname = myView.findViewById(R.id.edit_itemname);
        editTextItemvalue = myView.findViewById(R.id.edit_itemvalue);

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        listener = (DialogListener) context;
    }

    public interface DialogListener{
        void applyTexts(String itemName, String itemValue);
    }
}
