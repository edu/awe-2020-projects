package com.example.budget;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements DialogBox.DialogListener {
    private ArrayList<ExampleItem> myExampleList;

    private BudgetViewModel budgetViewModel;
    private RecyclerView myRecyclerView;
    private ExampleAdapter myAdapter;
    private RecyclerView.LayoutManager myLayoutManager;
    private ImageView myAddImage;
    private TextView sumTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createExampleList();
        buildRecyclerView();
        setButtons();
        setTextViews();
        sumOfList(myExampleList);
        budgetViewModel = ViewModelProviders.of(this).get(BudgetViewModel.class);
        budgetViewModel.getAllEntries().observe(this, new Observer<List<DB_Entry>>() {
            @Override
            public void onChanged(List<DB_Entry> db_entries) {
            }
        });
    }

    public void insertItem(int position, String itemName, String itemValue) {
        if (itemName.equals(null) || itemName.equals("")){ // enters dummy values to prevent crash
            itemName = "Empty";
        }
        if (itemValue.equals(null) || itemValue.equals("")){
            itemValue = "0";
        }

        myExampleList.add(position, new ExampleItem(R.drawable.ic_android, itemName, itemValue));
        myAdapter.notifyItemInserted(position); // Refreshes only Item at given position
        sumOfList(myExampleList);

        DB_Entry db_entry = new DB_Entry(position, itemName, itemValue);
        budgetViewModel.insert(db_entry); budgetViewModel.insert(db_entry);
    }

    public void removeItem(int position) {
        DB_Entry db_entry = new DB_Entry(position , myExampleList.get(position).getText1(), myExampleList.get(position).getText2());
        budgetViewModel.delete(db_entry); // garbage collector will delete this instance
        myExampleList.remove(position);
        myAdapter.notifyItemRemoved(position); // Refreshes only Item at given position
        sumOfList(myExampleList);
    }

    public void changeItem(int position, String text1) {
        DB_Entry db_entry = new DB_Entry(position, text1, myExampleList.get(position).getText2());
        budgetViewModel.update(db_entry); // garbage collector will delete this instance
        myExampleList.get(position).changeText1(text1);
        myAdapter.notifyItemChanged(position);
        sumOfList(myExampleList);
    }

    public void createExampleList() {
        myExampleList = new ArrayList<>();
    }

    public void buildRecyclerView() {
        myRecyclerView = findViewById(R.id.recyclerView);
        myRecyclerView.setHasFixedSize(true);
        myLayoutManager = new LinearLayoutManager(this);
        myAdapter = new ExampleAdapter(myExampleList);

        myRecyclerView.setLayoutManager(myLayoutManager);
        myRecyclerView.setAdapter(myAdapter);

        myAdapter.setOnItemClickListener(new ExampleAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                // Not used but could come in handy for future features
                //changeItem(position, "Clicked");
            }

            @Override
            public void onDeletClick(int position) {
                removeItem(position);
            }
        });
    }

    public void setTextViews(){
        sumTextView = findViewById(R.id.totalValue);
    }

    public void setButtons() {
        myAddImage = findViewById(R.id.image_add);

        myAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
    }

    public void openDialog() {
        DialogBox itemCreateDialog = new DialogBox();
        itemCreateDialog.show(getSupportFragmentManager(), "item creation dialog");
    }

    @Override
    public void applyTexts(String itemName, String itemValue) {
        // always set new item on the last position in list
        int position = myExampleList.size();
        insertItem(position, itemName, itemValue);
    }

    // Creates sum of items in list
    public void sumOfList(ArrayList<ExampleItem> list){
        int sum = 0;
        for (int iter = 0; iter < list.size(); iter ++){
            sum = sum + Integer.parseInt(list.get(iter).getText2().replaceAll("[\\D]", ""));
        }

        sumTextView.setText(String.format(Locale.ENGLISH, "Total value: %d", sum));
    }
}
