package com.example.budget;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {DB_Entry.class}, version = 2)
public abstract class BudgetDatabase extends RoomDatabase {

    // Singleton class
    private static BudgetDatabase instance;

    public abstract DB_Entry_DAO db_entry_dao();

    // Only one thread at a time has access, to prevent accidentally creating multiple insances
    public static synchronized BudgetDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    BudgetDatabase.class, "budget_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance; // return existing instance if already created
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private DB_Entry_DAO db_entry_dao;

        private PopulateDbAsyncTask(BudgetDatabase db){
            db_entry_dao = db.db_entry_dao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }
}
