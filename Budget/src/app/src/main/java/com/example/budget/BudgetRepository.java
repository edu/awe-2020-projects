package com.example.budget;

import android.app.Application;
import android.os.AsyncTask;
import androidx.lifecycle.LiveData;
import java.util.List;

public class BudgetRepository {
    private DB_Entry_DAO db_entry_dao;
    private LiveData<List<DB_Entry>> allEntries;

    public BudgetRepository(Application application){
        BudgetDatabase database = BudgetDatabase.getInstance(application);
        db_entry_dao = database.db_entry_dao();
        allEntries = db_entry_dao.getAllNotes();
    }

    public void insert(DB_Entry db_entry){
        new InsertEntryAsyncTask(db_entry_dao).execute(db_entry);
    }

    public void update(DB_Entry db_entry){
        new UpdateEntryAsyncTask(db_entry_dao).execute(db_entry);
    }

    public void delete(DB_Entry db_entry){
        new DeleteEntryAsyncTask(db_entry_dao).execute(db_entry);
    }

    public void deleteAllEntries(){
        new DeleteAllEntriesAsyncTask(db_entry_dao).execute();
    }

    public LiveData<List<DB_Entry>> getAllEntries() {
        return allEntries;
    }

    private static class InsertEntryAsyncTask extends AsyncTask<DB_Entry, Void, Void>{
        private DB_Entry_DAO db_entry_dao;

        private InsertEntryAsyncTask(DB_Entry_DAO db_entry_dao){
            this.db_entry_dao = db_entry_dao;
        }

        @Override
        protected Void doInBackground(DB_Entry... db_entries) {
            db_entry_dao.insert(db_entries[0]);

            return null;
        }
    }

    private static class UpdateEntryAsyncTask extends AsyncTask<DB_Entry, Void, Void>{
        private DB_Entry_DAO db_entry_dao;

        private UpdateEntryAsyncTask(DB_Entry_DAO db_entry_dao){
            this.db_entry_dao = db_entry_dao;
        }

        @Override
        protected Void doInBackground(DB_Entry... db_entries) {
            db_entry_dao.update(db_entries[0]);
            return null;
        }
    }

    private static class DeleteEntryAsyncTask extends AsyncTask<DB_Entry, Void, Void>{
        private DB_Entry_DAO db_entry_dao;

        private DeleteEntryAsyncTask(DB_Entry_DAO db_entry_dao){
            this.db_entry_dao = db_entry_dao;
        }

        @Override
        protected Void doInBackground(DB_Entry... db_entries) {
            db_entry_dao.delete(db_entries[0]);
            return null;
        }
    }

    private static class DeleteAllEntriesAsyncTask extends AsyncTask<Void, Void, Void>{
        private DB_Entry_DAO db_entry_dao;

        private DeleteAllEntriesAsyncTask(DB_Entry_DAO db_entry_dao){
            this.db_entry_dao = db_entry_dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            db_entry_dao.deleteAllNotes();
            return null;
        }
    }
}
