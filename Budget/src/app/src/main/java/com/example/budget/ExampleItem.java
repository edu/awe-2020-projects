package com.example.budget;

public class ExampleItem {
    private int myImageResource;
    private String name;
    private String value;

    public ExampleItem(int imageResource, String text1, String text2){
        myImageResource = imageResource;
        name = text1;
        value = text2;
    }

    public void changeText1(String text){
        name = text;
    }

    public int getImageResource(){
        return myImageResource;
    }

    public String getText1(){
        return name;
    }

    public String getText2(){
        return value;
    }
}
