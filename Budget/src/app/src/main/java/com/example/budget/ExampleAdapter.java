package com.example.budget;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ExampleAdapter extends RecyclerView.Adapter<ExampleAdapter.ExampleViewHolder> {
    private ArrayList<ExampleItem> myExampleList;

    private OnItemClickListener myListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
        void onDeletClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        myListener = listener;
    }

    public static class ExampleViewHolder extends RecyclerView.ViewHolder{
        public ImageView myImageView;
        public ImageView myDeleteImage;
        public TextView myTextView1;
        public TextView myTextView2;


        public ExampleViewHolder(@NonNull View itemView,final OnItemClickListener listener) {
            super(itemView);
            myImageView = itemView.findViewById(R.id.imageView);
            myTextView1 = itemView.findViewById(R.id.textView1);
            myTextView2 = itemView.findViewById(R.id.textView2);
            myDeleteImage = itemView.findViewById(R.id.image_delete);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Check for valid positions before using position
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            myDeleteImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Check for valid positions before using position
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onDeletClick(position);
                        }
                    }
                }
            });
        }
    }

    public ExampleAdapter(ArrayList<ExampleItem> myList){
        myExampleList = myList;
    }

    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View myView = LayoutInflater.from(parent.getContext()).inflate(R.layout.example_item, parent, false);
        ExampleViewHolder myExampleViewHolder = new ExampleViewHolder(myView, myListener);
        return  myExampleViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {
        ExampleItem currentItem = myExampleList.get(position);

        holder.myImageView.setImageResource(currentItem.getImageResource());
        holder.myTextView1.setText(currentItem.getText1());
        holder.myTextView2.setText(currentItem.getText2());
    }

    @Override
    public int getItemCount() {
        return myExampleList.size();
    }
}
