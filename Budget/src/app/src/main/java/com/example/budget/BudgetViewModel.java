package com.example.budget;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class BudgetViewModel extends AndroidViewModel {
    private BudgetRepository repository;
    private LiveData<List<DB_Entry>> allEntries;

    public BudgetViewModel(@NonNull Application application) {
        super(application);
        repository = new BudgetRepository(application);
        allEntries = repository.getAllEntries();
    }

    public void insert(DB_Entry db_entry){
        repository.insert(db_entry);
    }

    public void update(DB_Entry db_entry){
        repository.update(db_entry);
    }

    public void delete(DB_Entry db_entry){
        repository.delete(db_entry);
    }

    public void deleteAll(){
        repository.deleteAllEntries();
    }

    public LiveData<List<DB_Entry>> getAllEntries(){
        return allEntries;
    }
}
