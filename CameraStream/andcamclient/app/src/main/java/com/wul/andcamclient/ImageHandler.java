package com.wul.andcamclient;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;

public class ImageHandler extends Handler {
    private final WeakReference<MainActivity> mainActivity;

    public ImageHandler(MainActivity activity) {
        mainActivity = new WeakReference<MainActivity>(activity);
    }
    @Override
    public void handleMessage(Message msg) {
        System.out.println("---Recived msg");
        MainActivity activity = mainActivity.get();
        if(activity != null) {
            try {
                activity.lastFrame = ((Bitmap) msg.obj);
            }
            catch (Exception ex) {
                System.err.println("Set failed");
            }
        }
        super.handleMessage(msg);
    }
}
