package com.wul.andcamclient;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Handler;
import android.os.Message;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ClientSocketThread implements Runnable {
    Socket s = null;
    Handler imgHandler;
    OutputStream os = null;

    public ClientSocketThread(Handler handler, Socket s) throws IOException {
        imgHandler = handler;
        this.s = s;
    }

    @Override
    public void run() {
        try {
            InputStream inStream = null;
            try {
                inStream = s.getInputStream();
            } catch (Exception e) {
                e.printStackTrace();
            }
            DataInputStream is = new DataInputStream(inStream);
            while (true) {
                try {
                    int token = is.readInt();
                    if (token == 4) {
                        if(is.readUTF().equals("-start-")) {
                            int imgLength = is.readInt();
                            System.out.println("getLength:" + imgLength);
                            System.out.println("endtoken:" + is.readUTF());
                            byte[] buffer = new byte[imgLength];
                            int len = 0;
                            while (len < imgLength) {
                                len += is.read(buffer, len, imgLength - len);
                            }
                            Bitmap bitmap = BitmapFactory.decodeByteArray(buffer, 0, buffer.length);
                            Message message = imgHandler.obtainMessage();
                            message.obj = bitmap;
                            if(message.obj != null)  {
                                imgHandler.dispatchMessage(message);
                            }
                            else
                            {
                                System.err.println("Decode failed");
                            }
                            System.out.println("Done");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
