package com.wul.andcamclient;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {
    ImageView imgView;
    Bitmap lastFrame;
    Handler imageHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imgView = findViewById(R.id.imageView);
        imageHandler = new ImageHandler(this);

    }

    private Runnable imageDisplayer = new Runnable() {
        @Override
        public void run() {
            try {
                imageHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (lastFrame!=null){
                            imgView.setImageBitmap(lastFrame);
                        }

                    }
                });
            } finally {
                imageHandler.postDelayed(imageDisplayer, 1000/20); //Display at 20fps
            }
        }
    };

    public void start(View v){
        //Aquire image
        (new Thread(new Runnable() {
            public void run() {
                Socket s = null;
                try {
                    s = new Socket("192.168.178.138", 1234);
                    new Thread(new ClientSocketThread(imageHandler, s)).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        })).start();

        //display aquired images
        imageDisplayer.run();
    }
}
