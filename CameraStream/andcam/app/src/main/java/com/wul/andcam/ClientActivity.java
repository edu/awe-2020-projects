package com.wul.andcam;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.wul.andcam.client.*;
import java.io.IOException;
import java.net.Socket;

public class ClientActivity extends AppCompatActivity {
    ImageView imgView;
    static Handler imageHandler;
    static Handler showHideReconnectHandler;
    String ipAddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
        ipAddress = (String)getIntent().getExtras().get("ip");
        imgView = findViewById(R.id.imageView);

        //TODO: Merge both handlers into one
        imageHandler = new Handler() {
            public void handleMessage(Message msg) {
                    try {
                        setImage(((Bitmap) msg.obj));
                    }
                    catch (Exception ex) {
                        System.err.println("Set failed");
                    }
            }
        };
        showHideReconnectHandler = new Handler()  {
            public void handleMessage(Message msg) {
                showReconnectButton();
        }
    };
        connectToServer(ipAddress);
    }

    private void connectToServer(String ipAddress){
        final String ip = ipAddress;
        //Aquire image
        //TODO: onPause / onDestroy. Terminate threads
        (new Thread(new Runnable() { //need own thread here (no networking on main thread allowed)
            public void run() {
                Socket s = null;
                try {
                    s = new Socket(ip, 1234);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if(s == null || !s.isConnected()) {
                    System.err.println("Conection failed");
                    showHideReconnectHandler.sendEmptyMessage( 0);
                }
                else {
                    new ClientSocketThread(imageHandler, s).run();
                    if (s.isClosed() || !s.isConnected()) {
                        //Connection lost
                        showHideReconnectHandler.sendEmptyMessage( 0);
                        System.err.println("Connection lost");
                    }
                }
            }
        })).start();
    }

    private void showReconnectButton() {
        TextView clText = findViewById(R.id.conLostText);
        clText.setVisibility(View.VISIBLE);
        TextView stateTxt = findViewById(R.id.stateText);
        stateTxt.setText("Not connected");
        Button btn = findViewById(R.id.reconnect);
        btn.setEnabled(true);
        btn.setVisibility(View.VISIBLE);
    }
    public void reconnectButtonClicked(View view) {
        TextView clText = findViewById(R.id.conLostText);
        clText.setVisibility(View.INVISIBLE);
        Button btn = findViewById(R.id.reconnect);
        btn.setEnabled(true);
        btn.setVisibility(View.INVISIBLE);
        TextView stateTxt = findViewById(R.id.stateText);
        stateTxt.setText("Connected to: " + ipAddress);
        connectToServer(ipAddress);
    }

    private void setImage(Bitmap img) {
        if(img != null) {
            System.err.println(img.getByteCount());
            this.imgView.setImageBitmap(img);
        }
    }
}
