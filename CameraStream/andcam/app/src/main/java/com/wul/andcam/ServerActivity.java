package com.wul.andcam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.wul.andcam.model.Tags;
import com.wul.andcam.server.StreamCameraManager;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

public class ServerActivity extends AppCompatActivity {

    private static final String TAG = "AndCam";
    private TextureView textureView;
    private  boolean qrCodeVisible;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    StreamCameraManager streamManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server);
        Bitmap qrCode = null;
        try {
            qrCode = encodeAsBitmap(getLocalIpAddress());
            ImageView imgview = findViewById(R.id.qrcodeview);
            imgview.setImageBitmap(qrCode);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        textureView = (TextureView) findViewById(R.id.textureView);
        assert textureView != null;
        textureView.setSurfaceTextureListener(textureListener);
    }

    public void toggleStreaming(View v) {
        if(streamManager.isStreaming)
            streamManager.stopStreaming();
        else
            streamManager.startStreaming();
    }

    public void toggleQrCode(View v) {
        ImageView imgview = findViewById(R.id.qrcodeview);
        if(qrCodeVisible)
            imgview.setVisibility(View.INVISIBLE);
        else
            imgview.setVisibility(View.VISIBLE);
        qrCodeVisible = !qrCodeVisible;
    }

    private String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()&& inetAddress instanceof Inet4Address) { return inetAddress.getHostAddress().toString(); }
                }
            }
        } catch (SocketException ex) {
            Log.e("ServerActivity", ex.toString());
        }
        return null;
    }

    Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, 300, 300, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 300, 0, 0, w, h);
        return bitmap;
    }

    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            System.err.println("Surface avaible");
            CameraManager manager = (CameraManager)getSystemService(Context.CAMERA_SERVICE);
            Size desiredPreviewSize = new Size(width,height);
            //set up stream
            streamManager = new StreamCameraManager(
                    ServerActivity.this,
                    manager,
                    Tags.CAMERA_FACING_FRONT,
                    Tags.STREAM_QUALITY_LOW,
                    surface,
                    desiredPreviewSize);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            // Update desiredPreviewSize
            System.err.println("Surface sizechanged");
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            System.err.println("Surface destroyed");
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
           // System.err.println("Surface updated");
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // close the app
                Toast.makeText(ServerActivity.this, "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(streamManager != null) {
            streamManager.startCameraThreads();
            streamManager.resumeCamera();
            ToggleButton streamingButton = findViewById(R.id.streamingButton);
            streamingButton.setChecked(streamManager.isStreaming);
        }
        Log.e(TAG, "onResume");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
    }

    @Override
    protected void onPause() {
        Log.e(TAG, "onPause");
        if(streamManager != null)
            streamManager.pauseCamera();
            streamManager.releaseCameraThreads();
        super.onPause();
    }
}
