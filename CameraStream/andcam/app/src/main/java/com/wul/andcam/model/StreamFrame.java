package com.wul.andcam.model;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

public class StreamFrame implements Serializable{
    byte[] image;
    public StreamFrame(ByteArrayOutputStream image){
        this.image = image.toByteArray();
    }
    public byte[] getImage() {
        return image;
    }

}
