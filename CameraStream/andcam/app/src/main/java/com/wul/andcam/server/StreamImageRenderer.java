package com.wul.andcam.server;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.Image;
import android.media.ImageReader;

import com.wul.andcam.model.StreamFrame;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

public class StreamImageRenderer implements ImageReader.OnImageAvailableListener {
    StreamFrame latestImage;

    @Override
    public void onImageAvailable(ImageReader reader) {
        Image image = null;
        try {
            image = reader.acquireLatestImage();
            ByteArrayOutputStream compressedImage = null;
            if(image != null)
                compressedImage = compressImage(image);
            if(compressedImage != null)
                latestImage = new StreamFrame(compressedImage);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (image != null) {
                image.close();
            }
        }
    }

    private ByteArrayOutputStream compressImage(Image image){
        final Image.Plane[] planes = image.getPlanes();
        final ByteBuffer buffer = planes[0].getBuffer();
        buffer.rewind();
        final byte[] data = new byte[buffer.capacity()];
        buffer.get(data);

        long startTime = System.nanoTime();
        //convert data to bitmap (implement on client side) test:
        BitmapFactory.Options options = new BitmapFactory.Options();
        //options.inSampleSize = 10; //returns a bitmap with 1/10 of original size
        final Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);

        Matrix matrix = new Matrix();
        matrix.preRotate(90);

        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        ByteArrayOutputStream compressedImage = new ByteArrayOutputStream();

        //test compress
        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 10, compressedImage);

        //cPreview.write(data);

        long endTime = System.nanoTime();

        // get difference of two nanoTime values
        long timeElapsed = endTime - startTime;

       // System.out.println("Execution time in milliseconds : " +
       //         timeElapsed / 1000000);




        return compressedImage;

    }

    public StreamFrame getLatestImage() {
        return latestImage;
    }
}
