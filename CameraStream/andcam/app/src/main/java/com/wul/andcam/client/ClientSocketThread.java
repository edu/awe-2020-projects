package com.wul.andcam.client;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;

import com.wul.andcam.model.StreamFrame;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ClientSocketThread implements Runnable {
    Socket s = null;
    Handler imgHandler;
    OutputStream os = null;

    public ClientSocketThread(Handler handler, Socket s){
        imgHandler = handler;
        this.s = s;
    }

    @Override
    public void run() {
        try {
            InputStream inStream = null;
            try {
                inStream = s.getInputStream();
            } catch (Exception e) {
                e.printStackTrace();
            }
            while(s.isConnected()) {
                ObjectInputStream ois = new ObjectInputStream(inStream);
                Object input = ois.readObject();
                if(input instanceof StreamFrame){
                    StreamFrame frame = (StreamFrame)input;
                    byte[] barray = frame.getImage();
                    Bitmap bitmap = BitmapFactory.decodeByteArray(barray, 0, barray.length);
                    Message message = imgHandler.obtainMessage();
                    message.obj = bitmap;
                    if(message.obj != null)  {
                        imgHandler.dispatchMessage(message);
                    }
                    else
                    {
                        System.err.println("Decode failed");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                s.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
