package com.wul.andcam.server;

import com.wul.andcam.model.StreamFrame;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ServerSocketThread implements Runnable {
    Socket socket;
    StreamImageRenderer imageRenderer;
    OutputStream os = null;

    public ServerSocketThread(StreamImageRenderer imageRenderer, Socket s) throws IOException {
        this.imageRenderer = imageRenderer;
        this.socket = s;
    }

    @Override
    public void run() {
        if (socket != null) {
            String clientIp = socket.getInetAddress().toString().replace("/", "");
            int clientPort = socket.getPort();
            System.out.println("====client ip=====" + clientIp);
            System.out.println("====client port=====" + clientPort);
            try {
                socket.setKeepAlive(true);
                os = socket.getOutputStream();
                while (socket.isConnected()) {

                    //Quit save
                    if (Thread.interrupted()) {
                        System.out.println("Streaming stopped");
                        if (socket != null && !socket.isClosed())
                            socket.close();
                        if (os != null)
                            os.close();
                        break;
                    }

                    StreamFrame image = imageRenderer.getLatestImage();
                    if (image != null) {
                        System.err.println("Writing img");
                        ObjectOutputStream oos = new ObjectOutputStream(os);
                        oos.writeObject(image);
                        oos.flush();
                    }
                    Thread.sleep(1000 / 30);
                }
                System.out.println("====client disconected=====" + clientIp);
            } catch (Exception e) {
                System.out.println("====exception=====" + clientIp);
                e.printStackTrace();
                try {
                    if (os != null)
                        os.close();

                } catch (Exception e2) {
                    e.printStackTrace();
                }

            }

        } else {
            System.out.println("socket is null");

        }
    }

}