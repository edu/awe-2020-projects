package com.wul.andcam.server;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Size;
import android.view.Surface;

import androidx.core.app.ActivityCompat;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StreamCameraManager {
    String TAG = "CamaraManager";
    Activity mainActivity;
    String CameraId;
    List<Surface> targets;
    HandlerThread cameraThread;
    Handler cameraHandler;
    StreamImageRenderer imageRenderer;
    ImageReader imageReader;
    Thread StreamingThread;
    ServerSocket ServerSocket;
    List<Thread> ServerThreads;
    CameraDevice cameraDevice;
    CameraCaptureSession captureSession;
    CameraManager cameraManager;
    int cameraFacing;
    String streamQuality;
    SurfaceTexture previewTexture;
    Size streamingImageSize;
    public boolean isStreaming = false;

    public StreamCameraManager(Activity context, CameraManager manager, int cameraFacing,
                               String streamQuality, SurfaceTexture previewTexture, Size desiredPreviewSize) {
        mainActivity = context;
        cameraManager = manager;
        this.cameraFacing = cameraFacing;
        this.previewTexture = previewTexture;
        //setup preview size
        Size prevSize = getNearestSupportedCameraResolution(manager, cameraFacing, desiredPreviewSize);
        previewTexture.setDefaultBufferSize(prevSize.getWidth(), prevSize.getHeight());

        //setup stream size
        // TODO: set according to streamquality
        Size desitedStreamImageSize = new Size(200,200);
        streamingImageSize = getNearestSupportedCameraResolution(manager, cameraFacing, desitedStreamImageSize);
        startCameraThreads();
        //setup image renderer
        this.imageRenderer = new StreamImageRenderer();
        this.imageReader = createImageReader(imageRenderer, streamingImageSize);

        this.targets = Arrays.asList(new Surface(previewTexture), imageReader.getSurface());

        initCamera(manager, cameraFacing, streamQuality);
    }

    public void startStreaming() {
        ServerThreads = new ArrayList<Thread>();
        this.StreamingThread = new  Thread(new Runnable() {
            public void run() {
                try {
                    StreamCameraManager.this.ServerSocket = new ServerSocket(1234);
                    while (true) {
                        Socket s = StreamCameraManager.this.ServerSocket.accept(); //Will throw exception on ServerSocket.close() and stop thread
                        Thread serverThread = new Thread(new ServerSocketThread(imageRenderer, s));
                        serverThread.start();
                        StreamCameraManager.this.ServerThreads.add(serverThread);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.err.println("ServerThread: run: error");
                }
            }
        });

        this.StreamingThread.start();
        this.isStreaming = true;

    }

    public void stopStreaming() {

        //Close server
        try {
            if (ServerSocket != null && !ServerSocket.isClosed())
                ServerSocket.close();

            //Terminate all connections
            if (ServerThreads != null) {
                for (Thread t : ServerThreads) {
                    t.interrupt();
                }
            }
            this.isStreaming = false;
        } catch (IOException ex){
            System.err.println("Stop streaming failed.");
            ex.printStackTrace();
        }

    }

    private ImageReader createImageReader(StreamImageRenderer imageRenderer, Size streamingImageSize) {
        ImageReader imageReader = ImageReader.newInstance(streamingImageSize.getWidth(), streamingImageSize.getHeight(), ImageFormat.JPEG, 2);
        imageReader.setOnImageAvailableListener(imageRenderer, cameraHandler);
        return imageReader;
    }

    public void startCameraThreads() {
        cameraThread = new HandlerThread("Camera Background");
        cameraThread.start();
        cameraHandler = new Handler(cameraThread.getLooper());
    }

    public void releaseCameraThreads() {
        if(this.cameraHandler != null && this.cameraThread != null) {
            this.cameraHandler.removeCallbacksAndMessages(null);
            this.cameraThread.quitSafely();
        }
    }

    //call on Activity.onPause
    public void pauseCamera() {
        if(this.isStreaming)
           stopStreaming();
        if(cameraDevice != null)
            cameraDevice.close();
        if(captureSession != null)
            captureSession.close();
        if(imageReader != null)
            imageReader.close();
    }

    //call on Activity.onResume
    public void resumeCamera() {
        this.imageReader = createImageReader(imageRenderer, streamingImageSize); //Reopen imageReader
        this.targets = Arrays.asList(new Surface(previewTexture), imageReader.getSurface());
        initCamera(this.cameraManager, this.cameraFacing, this.streamQuality);
        if(this.isStreaming)
            startStreaming();
    }

    private void initCamera(CameraManager manager, int cameraFacing, String streamQuality) {
        //TODO: Implement camera facing, Add imageDimension selection
        Log.e(TAG, "is camera open");
        try {
            CameraId = manager.getCameraIdList()[0];
            //Move to mainactivity?
            if (ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(mainActivity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                return;
            }
            manager.openCamera(CameraId, cameraStateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "openCamera");
    }

    private final CameraDevice.StateCallback cameraStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            //Camera now open
            Log.e(TAG, "onOpened");
            try {
                //Configure capture request
                final CaptureRequest.Builder captureRequestBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                StreamCameraManager.this.cameraDevice = camera;
                captureRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, 0);
                for (Surface target : targets) {
                    captureRequestBuilder.addTarget(target);
                }
                camera.createCaptureSession(targets, new CameraCaptureSession.StateCallback() {
                    @Override
                    public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                        StreamCameraManager.this.captureSession = cameraCaptureSession;
                        // Capture request ready
                        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
                        try {
                            //start capture session in background handler
                            cameraCaptureSession.setRepeatingRequest(captureRequestBuilder.build(), null, cameraHandler);
                        } catch (CameraAccessException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
                        Log.e(TAG, "session configuration failed");
                    }
                }, null);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            Log.e(TAG, "Camera onDissconected");
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            Log.e(TAG, "Camera onError");
        }
    };

    private Size getNearestSupportedCameraResolution(CameraManager manager, int cameraFacing, Size desiredSize) {
        try {
            String cameraId = manager.getCameraIdList()[cameraFacing];
            CameraCharacteristics   characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            //TODO implement desiredsize filter
            return map.getOutputSizes(MediaRecorder.class)[5];
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
