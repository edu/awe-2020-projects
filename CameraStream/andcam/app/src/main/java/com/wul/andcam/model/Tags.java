package com.wul.andcam.model;

public final class Tags {
    public static final int CAMERA_FACING_FRONT = 0;
    public static final int CAMERA_FACING_BACK = 1;

    public static final String STREAM_QUALITY_LOW = "lo";
    public static final String STREAM_QUALITY_MEDIUM = "me";
    public static final String STREAM_QUALITY_HIGH= "hi";
    private Tags() {}

}
