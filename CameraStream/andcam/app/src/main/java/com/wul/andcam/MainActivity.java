package com.wul.andcam;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void serverView(View v) {
        startActivity(new Intent(MainActivity.this, ServerActivity.class));
    }

    public void clientView(View v) {
        Intent scannerIntent = new Intent(this, QrCodeScanner.class);
        startActivity(scannerIntent);
    }
}